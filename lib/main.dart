import 'package:mad_sal/core/constants/app_packages.dart';

void main() async {
  await initialServices();
  runApp(const MyApp());
}
