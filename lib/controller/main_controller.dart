import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:mad_sal/view/roots/categories/screens/categories_screen.dart';
import 'package:mad_sal/view/roots/home/screens/home_screen.dart';
import 'package:mad_sal/view/roots/setting/screens/setting_screen.dart';

abstract class RootController extends GetxController {
  void selectScreen(int index);
}

class RootControllerImp extends RootController {
  int currentIndex = 0;

  final List<Widget> rootsScreensList = const [
    HomeScreen(),
    SettingScreen(),
    CategoriesScreen(),
    HomeScreen(),
    HomeScreen(),
  ];

  @override
  void selectScreen(int index) {
    currentIndex = index;
    update();
  }
}
