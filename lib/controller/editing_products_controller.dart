
import 'package:get/get.dart';
import 'package:mad_sal/view/roots/categories/editing_services/model/edit_service.dart';

class EditingProductsController extends GetxController {
  int currentIndex = 1;

  @override
  void removeItem(index) {
    editService.removeAt(index);
    update();
  }

  @override
  change(int value) {
    currentIndex = value;
    update();
  }



}