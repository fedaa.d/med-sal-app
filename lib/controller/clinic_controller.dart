import 'package:get/get_state_manager/get_state_manager.dart';

abstract class ClinicController extends GetxController {
  change(int value);
}

class ClinicControllerImp extends ClinicController {
  int currentIndex = 0;
  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
