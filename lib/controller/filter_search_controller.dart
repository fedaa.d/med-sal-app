import 'package:get/get.dart';

abstract class FilterSearchController extends GetxController {
  selectedCategoriesFilter(int value);
  selectedDoctorsFilter(int value);
}

class FilterSearchControllerImp extends FilterSearchController {
  int currentIndex = 0;
  int selectedIndex = 0;

  @override
  selectedCategoriesFilter(int value) {
    currentIndex = value;
    update();
  }

  @override
  selectedDoctorsFilter(value) {
    selectedIndex = value;
    update();
  }
}
