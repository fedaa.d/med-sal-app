import 'package:get/get_state_manager/get_state_manager.dart';

abstract class HospitalsController extends GetxController {
  change(int value);
}

class HospitalsControllerImp extends HospitalsController {
  int currentIndex = 0;
  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
