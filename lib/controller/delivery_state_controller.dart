import 'package:get/get.dart';

abstract class DeliveryStateController extends GetxController {
  change(int value);
}

class DeliveryStateControllerImp extends DeliveryStateController {
  int currentIndex = 0;

  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
