import 'package:get/get_state_manager/get_state_manager.dart';

abstract class DentistryController extends GetxController {
  change(int value);
}

class DentistryControllerImp extends DentistryController {
  int currentIndex = 0;
  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
