import 'package:get/get.dart';

abstract class SPRegistrationRequestController extends GetxController {
  change(int value);
}

class SPRegistrationRequestControllerImp
    extends SPRegistrationRequestController {
  int currentIndex = 0;

  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
