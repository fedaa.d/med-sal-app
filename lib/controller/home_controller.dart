import 'package:get/get.dart';

abstract class HomeController extends GetxController {
  onChoose(val);
}

class HomeControllerImp extends HomeController {
  lang selectedItem = lang.EN;

  @override
  onChoose(val) {
    selectedItem = val;
    update();
  }
}

enum lang {
  EN,
  AR,
}
