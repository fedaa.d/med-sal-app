import 'package:get/get.dart';

abstract class ServicesAdminController extends GetxController {
  change(int value);
}

class ServicesAdminControllerImp extends ServicesAdminController {
  int currentIndex = 0;
  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
