import 'package:get/get_state_manager/get_state_manager.dart';

abstract class PharmaciesController extends GetxController {
  change(int value);
}

class PharmaciesControllerImp extends PharmaciesController {
  int currentIndex = 0;
  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
