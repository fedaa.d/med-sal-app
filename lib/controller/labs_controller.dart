import 'package:get/get_state_manager/get_state_manager.dart';

abstract class LabsController extends GetxController {
  change(int value);
}

class LabsControllerImp extends LabsController {
  int currentIndex = 0;
  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
