import 'package:get/get_state_manager/get_state_manager.dart';

abstract class ProductBookingController extends GetxController {
  change(int value);
}

class ProductBookingControllerImp extends ProductBookingController {
  int currentIndex = 0;
  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
