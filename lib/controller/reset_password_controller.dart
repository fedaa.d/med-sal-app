import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

abstract class ResetPasswordController extends GetxController {
  visibilityPassword();
  visibilityConfirmPassword();
}

class ResetPasswordControllerImp extends ResetPasswordController {
  TextEditingController passwordController = TextEditingController();

  TextEditingController confirmPasswordController = TextEditingController();
  GlobalKey formKey = GlobalKey<FormState>();
  static const passwordChange = SnackBar(
      content: Text('Password Changed',),
    padding: EdgeInsets.symmetric(horizontal: 140, vertical: 10),backgroundColor: Colors.green,);
  static const somthingWrong = SnackBar(
    content: Text('Somthing is wrong \n \n please try again'),
      padding: EdgeInsets.symmetric(horizontal: 140, vertical: 10),backgroundColor: AppColors.redColor,);

  bool isPassword = true;
  bool isConfirmPassword = true;

  @override
  visibilityPassword() {
    isPassword = !isPassword;
    update();
  }

  @override
  visibilityConfirmPassword() {
    isConfirmPassword = !isConfirmPassword;
    update();
  }
}
