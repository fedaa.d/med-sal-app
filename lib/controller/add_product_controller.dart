import 'package:get/get_state_manager/src/simple/get_controllers.dart';

abstract class AddProductController extends GetxController {
  setValue(int val);
}

class AddProductControllerImp extends AddProductController {
  int value = 0;
  int value1 = 0;
  @override

  void setValue (int val){
    value = val;
    update();
  }

  void setValue1 (int val){
    value1 = val;
    update();
  }


}