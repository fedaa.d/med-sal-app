import 'package:get/get.dart';

abstract class AdminsListController extends GetxController {
  change(int value);
}

class AdminsListControllerImp extends AdminsListController {
  int currentIndex = 0;

  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
