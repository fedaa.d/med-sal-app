import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class AddServicesController extends GetxController {}

class AddServicesControllerImp extends AddServicesController {
  TextEditingController nameServicesController = TextEditingController();
  TextEditingController nameCategoryController = TextEditingController();
  TextEditingController costController = TextEditingController();
  TextEditingController discountController = TextEditingController();
  TextEditingController totalCostController = TextEditingController();

  GlobalKey formKey = GlobalKey<FormState>();
}
