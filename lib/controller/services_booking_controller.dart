import 'package:get/get.dart';

abstract class ServicesBookingController extends GetxController {
  change(int value);
}

class ServicesBookingControllerImp extends ServicesBookingController {
  int currentIndex = 0;

  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
