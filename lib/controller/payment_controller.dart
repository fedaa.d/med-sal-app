
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

class PaymentController extends GetxController {
  List<String> pay = ['Cash','Card'];
  var selectedPay = ''.obs;
  String select = 'Cash';
  static const orderConfirmed = SnackBar(
    content: Text('the Order has confirmed',),
    padding: EdgeInsets.symmetric(horizontal: 120, vertical: 10),backgroundColor: Colors.green,);
  static const orderCanceled = SnackBar(
    content: Text('The Order has canceled'),
    padding: EdgeInsets.symmetric(horizontal: 120, vertical: 10),backgroundColor: AppColors.redColor,);


  int value = 0;

  void onClickedRadioButton(var value){
    selectedPay.value = value;
  }


  void setValue (int val){
    value = val;
    update();
  }

}








