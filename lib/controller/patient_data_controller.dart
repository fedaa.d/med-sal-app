import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PatientDataController extends GetxController {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController dateController = TextEditingController();
  DateTime selectDate = DateTime.now();
  var formKey = GlobalKey<FormState>();
  bool isPassword1 = true;
  bool isPassword2 = true;

  @override
  visibilityPassword() {
    isPassword1 = !isPassword1;
    update();
  }

  @override
  visibilityConfirmPassword() {
    isPassword2 = !isPassword2;
    update();
  }

  @override
  chooseDate() async {
    DateTime? pickDate = await showDatePicker(
      context: Get.context!,
      initialDate: selectDate,
      firstDate: DateTime(2023),
      lastDate: DateTime(2025),
      initialEntryMode: DatePickerEntryMode.input,
    );
    if (pickDate != null && pickDate != selectDate) {
      selectDate = pickDate;
    }
    update();
  }
}
