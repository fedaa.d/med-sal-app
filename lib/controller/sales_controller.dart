import 'package:get/get.dart';

abstract class SalesController extends GetxController {
  change(int value);
  onSelected(int value);
}

class SalesControllerImp extends SalesController {
  int currentIndex = 0;
  int selectedIndex = 0;

  @override
  change(int value) {
    currentIndex = value;
    update();
  }

  @override
  onSelected(int value) {
    selectedIndex = value;
    update();
  }
}
