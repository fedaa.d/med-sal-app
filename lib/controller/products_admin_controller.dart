import 'package:get/get.dart';

abstract class ProductsAdminController extends GetxController {
  change(int value);
}

class ProductsAdminControllerImp extends ProductsAdminController {
  int currentIndex = 0;
  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
