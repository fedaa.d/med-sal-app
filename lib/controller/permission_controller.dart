import 'package:get/get_state_manager/get_state_manager.dart';

abstract class PermissionController extends GetxController {
  change(int value);
}

class PermissionControllerImp extends PermissionController {
  int currentIndex = 0;
  @override
  change(int value) {
    currentIndex = value;
    update();
  }
}
