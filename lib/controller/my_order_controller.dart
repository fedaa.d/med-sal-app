import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

abstract class MyOrderController extends GetxController {}

class MyOrderControllerImp extends MyOrderController {
  TextEditingController cardNumberController = TextEditingController();
  TextEditingController nameCardController = TextEditingController();
  TextEditingController expDateController = TextEditingController();
  TextEditingController cvvController = TextEditingController();
  TextEditingController descreptionController = TextEditingController();
  static const orderConfirmed = SnackBar(
    content: Text(
      'the Order has confirmed',
    ),
    padding: EdgeInsets.symmetric(horizontal: 120, vertical: 10),
    backgroundColor: Colors.green,
  );
  static const orderCanceled = SnackBar(
    content: Text('The Order has canceled'),
    padding: EdgeInsets.symmetric(horizontal: 120, vertical: 10),
    backgroundColor: AppColors.redColor,
  );

  GlobalKey formKey = GlobalKey<FormState>();
}
