import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class RegistrationDetailsController extends GetxController {
  visibilityPassword();
  visibilityConfirmPassword();
  pickFile();
}

class RegistrationDetailsControllerImp extends RegistrationDetailsController {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController numberController = TextEditingController();
  TextEditingController businesController = TextEditingController();
  TextEditingController servicesController = TextEditingController();
  bool isPassword = true;
  bool isConfirmPassword = true;
  GlobalKey formKey = GlobalKey<FormState>();
  @override
  visibilityPassword() {
    isPassword = !isPassword;
    update();
  }

  @override
  visibilityConfirmPassword() {
    isConfirmPassword = !isConfirmPassword;
    update();
  }

  void pickFile() async {
    String? filePath = await FilePicker.platform
        .pickFiles(
          type: FileType.any,
        )
        .then((result) => result?.files.single.path);
    update();
  }
}
