import 'dart:io';
import 'package:mad_sal/core/constants/app_packages.dart';

abstract class RegisterProviderController extends GetxController {
  // Show or hide the password in the password entry field
  void visibilityPassword();

  // Show or hide the password in the password confirmation field
  void visibilityConfirmPassword();

  // Choose file
  void pickFile();

  // Confirm the account creation process
  void submitRegister();
}

class RegisterProviderControllerImp extends RegisterProviderController {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();
  final TextEditingController numberController = TextEditingController();
  final TextEditingController bussinessNameController = TextEditingController();
  final TextEditingController serviceController = TextEditingController();
  final TextEditingController bankNameController = TextEditingController();
  final TextEditingController swiftCodeController = TextEditingController();
  final TextEditingController ilbanController = TextEditingController();

//----------------------------------------------------------------

  bool isPassword = true;

  @override
  visibilityPassword() {
    isPassword = !isPassword;
    update();
  }

//----------------------------------------------------------------

  bool isConfirmPassword = true;

  @override
  visibilityConfirmPassword() {
    isConfirmPassword = !isConfirmPassword;
    update();
  }

//----------------------------------------------------------------

  File? file1;

  @override
  void pickFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles(
      type: FileType.custom,
      allowedExtensions: ['pdf'],
    );

    if (result != null) {
      PlatformFile file = result.files.first;
      file1 = File(file.path!);
    }
  }

//----------------------------------------------------------------

  final RegisterProviderData registerProviderData =
      RegisterProviderData(Get.find());
  final MyServices myServices = Get.find<MyServices>();
  StatusRequest statusRequest = StatusRequest.none;

  UserProviderModel? userProviderModel;
  UserModel? userModel;

  @override
  void submitRegister() async {
    if (!formKey.currentState!.validate()) return;
    if (file1 == null) {
      customSnackBar(title: 'فشل', message: 'تأكد من تعبة حقل الملف');
      return;
    }

    statusRequest = StatusRequest.loading;
    update();

    var response = await registerProviderData.postRegisterProviderData(
      emailController.text.trim(),
      passwordController.text.trim(),
      confirmPasswordController.text.trim(),
      numberController.text.trim(),
      bussinessNameController.text.trim(),
      serviceController.text.trim(),
      bankNameController.text.trim(),
      ilbanController.text.trim(),
      swiftCodeController.text.trim(),
      file1!,
    );

    statusRequest = handlingData(response);

    if (statusRequest == StatusRequest.success) {
      userModel = UserModel.fromJson(response['user']);
      userProviderModel = UserProviderModel.fromJson(response['provider']);

      myServices.getBox.write('email', userModel!.email);
      myServices.getBox.write('role', userModel!.role);
      myServices.getBox.write('id', userModel!.id);

      myServices.getBox.write('userId', userProviderModel!.userId);
      myServices.getBox
          .write('serviceTypeId', userProviderModel!.serviceTypeId);
      myServices.getBox.write('businessName', userProviderModel!.businessName);
      myServices.getBox
          .write('contactNumber', userProviderModel!.contactNumber);
      myServices.getBox.write('bankName', userProviderModel!.bankName);

      myServices.getBox.write('token', response['token']);

      Get.offNamed(AppNameRoutes.checkEmailScreen, arguments: {
        'email': myServices.getBox.read('email'),
      });
    } else {
      print('فشلت العملية');
    }
    update();
  }
}
