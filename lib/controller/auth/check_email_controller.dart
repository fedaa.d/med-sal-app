import 'package:mad_sal/core/constants/app_packages.dart';

class CheckEmailController extends GetxController {
  final String email = Get.arguments['email'];
  String splitEmail = '';

  void splitTheEmail() {
    List<String> emailList = email.split('@');
    String numMask = '*' * (emailList[0].length - 2);
    splitEmail =
        '$numMask${emailList[0].substring(emailList[0].length - 2)}@${emailList[1].split('.')[0]}';
  }

  @override
  void onInit() {
    splitTheEmail();
    super.onInit();
  }
}
