import 'package:mad_sal/core/constants/app_packages.dart';
import 'package:mad_sal/core/data/datasourse/remote/auth/login_data.dart';

abstract class LoginController extends GetxController {
  void visibilityPassword();
  void checked(bool check);
  void submitLogin();
}

class LoginControllerImp extends LoginController {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  GlobalKey<FormState> formKey = GlobalKey<FormState>();
  bool isPassword = true;
  bool isChecked = false;

  @override
  visibilityPassword() {
    isPassword = !isPassword;
    update();
  }

  @override
  checked(bool check) {
    isChecked = check;
    update();
  }

//----------------------------------------------------------------

  final LoginData loginData = LoginData(Get.find());
  final MyServices myServices = Get.find<MyServices>();
  StatusRequest statusRequest = StatusRequest.none;

  UserModel? userModel;

  void submitLogin() async {
    if (!formKey.currentState!.validate()) return;

    statusRequest = StatusRequest.loading;
    update();

    var response = await loginData.postLoginData(
      emailController.text.trim(),
      passwordController.text.trim(),
    );

    statusRequest = handlingData(response);

    if (statusRequest == StatusRequest.success) {
      userModel = UserModel.fromJson(response['user']);

      myServices.getBox.write('email', userModel!.email);
      myServices.getBox.write('role', userModel!.role);

      myServices.getBox.write('token', response['token']);

      customDialog(title: 'Success', subTitle: 'You are Welcome to Med-sal');

      Get.offNamed(AppNameRoutes.rootScreen, arguments: {
        'email': myServices.getBox.read('email'),
      });
    } else {
      print(statusRequest);
    }
    statusRequest = StatusRequest.none;
    update();
  }
}
