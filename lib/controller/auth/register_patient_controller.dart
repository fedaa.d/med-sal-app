import 'package:mad_sal/core/constants/app_packages.dart';

abstract class RegisterPatientController extends GetxController {
  // Show or hide the password in the password entry field
  void visibilityPassword();

  // Show or hide the password in the password confirmation field
  void visibilityConfirmPassword();
}

class RegisterPatientControllerImp extends RegisterPatientController {
  final formKey = GlobalKey<FormState>();

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();

//----------------------------------------------------------------

  bool isPassword = true;

  @override
  void visibilityPassword() {
    isPassword = !isPassword;
    update();
  }

//----------------------------------------------------------------

  bool isPasswordConfirm = true;

  @override
  void visibilityConfirmPassword() {
    isPasswordConfirm = !isPasswordConfirm;
    update();
  }

//----------------------------------------------------------------

  final RegisterPatientData registerPatientData =
      RegisterPatientData(Get.find());
  final MyServices myServices = Get.find<MyServices>();
  StatusRequest statusRequest = StatusRequest.none;

  UserModel? userModel;

  void submitRegister() async {
    if (!formKey.currentState!.validate()) return;

    statusRequest = StatusRequest.loading;
    update();

    var response = await registerPatientData.postRegisterPatientData(
      emailController.text.trim(),
      passwordController.text.trim(),
      confirmPasswordController.text.trim(),
    );

    statusRequest = handlingData(response);

    if (statusRequest == StatusRequest.success) {
      userModel = UserModel.fromJson(response['user']);

      myServices.getBox.write('email', userModel!.email);
      myServices.getBox.write('role', userModel!.role);
      myServices.getBox.write('id', userModel!.id);
      myServices.getBox.write('token', response['token']);

      Get.offNamed(AppNameRoutes.checkEmailScreen, arguments: {
        'email': myServices.getBox.read('email'),
      });
    } else {
      print(statusRequest);
    }
    statusRequest = StatusRequest.none;
    update();
  }
}
