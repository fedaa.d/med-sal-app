import 'package:mad_sal/core/constants/app_packages.dart';
import 'package:mad_sal/core/data/datasourse/remote/auth/verification_code_data.dart';

abstract class VerificationCodeController extends GetxController {
  submitVerificationCode();
}

class VerificationCodeControllerImp extends VerificationCodeController {
  String code = '';

  void setCode(final String value) {
    code = value;
  }

//----------------------------------------------------------------

  final VerificationCodeData verificationCodeData =
      VerificationCodeData(Get.find());
  StatusRequest statusRequest = StatusRequest.none;

  @override
  void submitVerificationCode() async {
    if (code.length < 6) {
      customSnackBar(title: 'فشل', message: 'تأكد من تعبة جميع الحقول');
      return;
    }
    
    statusRequest = StatusRequest.loading;
    update();

    var response = await verificationCodeData.postVerificationCodeData(
      code.trim(),
    );

    statusRequest = handlingData(response);

    if (statusRequest == StatusRequest.success) {
      Get.offNamed(AppNameRoutes.login);
    } else {
      print(statusRequest);
    }
    statusRequest = StatusRequest.none;
    update();
  }
}
