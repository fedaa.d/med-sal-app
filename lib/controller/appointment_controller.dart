import 'package:get/get.dart';
import 'package:table_calendar/table_calendar.dart';

abstract class AppointmentController extends GetxController {
  onDaySelected(selectedDay, focusedDay);
  onFormatChanged(format);
}

class AppointmentControllerImp extends AppointmentController {
  DateTime? selected;
  DateTime focus = DateTime.now();
  CalendarFormat calendarFormat = CalendarFormat.month;
  @override
  onDaySelected(selectedDay, focusedDay) {
    selected = selectedDay;
    focus = focusedDay;
    update();
  }

  @override
  onFormatChanged(format) {
    calendarFormat = format;
    update();
  }

  @override
  void onInit() {
    selected = focus;
    super.onInit();
  }
}
