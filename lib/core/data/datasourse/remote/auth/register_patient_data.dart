import 'package:mad_sal/core/class/crud.dart';
import 'package:mad_sal/core/constants/app_api_links.dart';

class RegisterPatientData {
  Crud crud;
  RegisterPatientData(this.crud);

  postRegisterPatientData(
    final String email,
    final String password,
    final String passwordConfirmation,
  ) async {
    var response = await crud.postData(AppApiLinks.registerPatientApi, {
      'email': email,
      'password': password,
      'password_confirmation': passwordConfirmation,
    });

    return response.fold((l) => l, (r) => r);
  }
}
