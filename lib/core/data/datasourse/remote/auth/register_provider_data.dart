import 'dart:convert';
import 'dart:io';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/class/crud.dart';
import 'package:mad_sal/core/class/status_request.dart';
import 'package:mad_sal/core/constants/app_api_links.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/functions/check_internet.dart';
import 'package:mad_sal/core/shared/custom_snackbar.dart';
import 'package:http/http.dart' as http;

class RegisterProviderData {
  Crud crud;
  RegisterProviderData(this.crud);
  postRegisterProviderData(
    final String email,
    final String password,
    final String passwordConfirmation,
    final String contactNumber,
    final String businessName,
    final String serviceId,
    final String bankName,
    final String iban,
    final String swiftCode,
    final File document,
  ) async {
    Future<Either<StatusRequest, Map>> postData() async {
      try {
        //Test the presence of an Internet connection
        if (await checkInternet()) {
          var request = http.MultipartRequest(
              'POST', Uri.parse(AppApiLinks.registerProviderApi))
            ..headers['Accept'] = 'application/json'
            ..headers['app_key'] =
                'base64:6+VIIByNpQQSJ3wbs0YQUCVJn68kxH1qkK48kz1KLDM=';

          request.fields['email'] = email;
          request.fields['password'] = password;
          request.fields['password_confirmation'] = passwordConfirmation;
          request.fields['contact_number'] = contactNumber;
          request.fields['business_name'] = businessName;
          request.fields['service_type_id'] = serviceId;
          request.fields['bank_name'] = bankName;
          request.fields['iban'] = iban;
          request.fields['swift_code'] = swiftCode;
          request.files
              .add(await http.MultipartFile.fromPath('document', document.path));
          final response = await http.Response.fromStream(await request.send());
          Get.closeAllSnackbars();

          //Test the status of the API connection
          if (response.statusCode == 200) {
            final Map responseBody = jsonDecode(response.body);
            print(response.body);
            //Test if the operation was completed successfully
            if (responseBody.containsKey('message')) {
              final String message = responseBody['message'];
              customSnackBar(title: 'اشعار', message: message);
            }
            int status = 1;
            if (responseBody.containsKey('status')) {
              status = responseBody['status'];
              if (status != 1) {
                return const Left(StatusRequest.serverfailure);
              }
            }
            return Right(responseBody);
          } else {
            if (response.statusCode == 422) {
              final Map responseBody = jsonDecode(response.body);

              //Test if the operation was completed successfully
              if (responseBody.containsKey('message')) {
                final String message = responseBody['message'];
                customSnackBar(title: 'فشل', message: message);
              } else {
                customSnackBar(title: 'فشل', message: 'حدث خطأ ما');
              }
            } else if (response.statusCode == 401) {
              customSnackBar(
                title: 'فشل',
                message: 'انتهت صلاحية الجلسة قم بتسجيل الدخول من جديد',
              );
              await Get.offAndToNamed(AppNameRoutes.login);
              Get.deleteAll();
              Get.put(Crud());
              return const Left(StatusRequest.none);
            } else {
              customSnackBar(title: 'فشل', message: 'حدث خطأ ما');
            }
            return const Left(StatusRequest.serverfailure);
          }
        } else {
          customSnackBar(title: 'فشل', message: 'لا يوجد اتصال بالانترنت');
          return const Left(StatusRequest.offlinefailure);
        }
      } catch (e) {
        customSnackBar(title: 'فشل', message: 'قم بإعادة المحاولة مرة اخرى');
        return const Left(StatusRequest.offlinefailure);
      }
    }

    var v = await postData();
    return v.fold((l) => l, (r) => r);
  }
}
