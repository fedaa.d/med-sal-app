import 'package:mad_sal/core/class/crud.dart';
import 'package:mad_sal/core/constants/app_api_links.dart';

class LoginData {
  Crud crud;
  LoginData(this.crud);

  postLoginData(
    final String email,
    final String password,
  ) async {
    var response = await crud.postData(AppApiLinks.loginApi, {
      'email': email,
      'password': password,
    });

    return response.fold((l) => l, (r) => r);
  }
}
