import 'package:mad_sal/core/class/crud.dart';
import 'package:mad_sal/core/constants/app_api_links.dart';
import 'package:mad_sal/core/constants/app_packages.dart';

class VerificationCodeData {
  Crud crud;
  VerificationCodeData(this.crud);
  final MyServices myServices = Get.find<MyServices>();

  postVerificationCodeData(
    final String code,
  ) async {
    var response = await crud.postData(
      AppApiLinks.verificationCodeApi,
      {'code': code,},
      myServices.getBox.read('token'),
    );

    return response.fold((l) => l, (r) => r);
  }
}
