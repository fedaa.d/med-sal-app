import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
    final String email;
    final String role;
    final DateTime updatedAt;
    final DateTime createdAt;
    final int id;

    UserModel({
        required this.email,
        required this.role,
        required this.updatedAt,
        required this.createdAt,
        required this.id,
    });

    factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        email: json["email"],
        role: json["role"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "email": email,
        "role": role,
        "updated_at": updatedAt.toIso8601String(),
        "created_at": createdAt.toIso8601String(),
        "id": id,
    };
}
