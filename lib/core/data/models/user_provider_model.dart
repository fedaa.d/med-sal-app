// To parse this JSON data, do
//
//     final userProviderModel = userProviderModelFromJson(jsonString);

import 'dart:convert';

UserProviderModel userProviderModelFromJson(String str) =>
    UserProviderModel.fromJson(json.decode(str));

String userProviderModelToJson(UserProviderModel data) =>
    json.encode(data.toJson());

class UserProviderModel {
  final int? userId;
  final String serviceTypeId;
  final String businessName;
  final String contactNumber;
  final String bankName;
  final String iban;
  final String swiftCode;
  final String document;
  final String status;
  final DateTime updatedAt;
  final DateTime createdAt;
  final int id;

  UserProviderModel({
    required this.userId,
    required this.serviceTypeId,
    required this.businessName,
    required this.contactNumber,
    required this.bankName,
    required this.iban,
    required this.swiftCode,
    required this.document,
    required this.status,
    required this.updatedAt,
    required this.createdAt,
    required this.id,
  });

  factory UserProviderModel.fromJson(Map<String, dynamic> json) =>
      UserProviderModel(
        userId: json["user_id"],
        serviceTypeId: json["service_type_id"],
        businessName: json["bussiness_name"],
        contactNumber: json["contact_number"],
        bankName: json["bank_name"],
        iban: json["iban"],
        swiftCode: json["swift_code"],
        document: json["document"],
        status: json["status"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "user_id": userId,
        "service_type_id": serviceTypeId,
        "bussiness_name": businessName,
        "contact_number": contactNumber,
        "bank_name": bankName,
        "iban": iban,
        "swift_code": swiftCode,
        "document": document,
        "status": status,
        "updated_at": updatedAt.toIso8601String(),
        "created_at": createdAt.toIso8601String(),
        "id": id,
      };
}
