class AppImages {
  static const root = 'assets/images/';

//--------------------------Home Images-----------------------------
  static const logoImage = '${root}logo.png';
  static const doctorImage = '${root}doctor.png';
  static const doctorsImage = '${root}doctors.png';
  static const clientImage = '${root}client.png';

//--------------------------Auth Images-----------------------------
  static const checkEmailImage = '${root}check_email.png';
  static const loginImage = '${root}login_image.png';
  static const uploadImage = '${root}Rectangle 65.png';

//--------------------------Categories Images-----------------------------
  static const profileImage = '${root}profile.png';
  static const stethoscopeImage = '${root}Rectangle 100.png';
  static const pharmacyImage = '${root}Rectangle 101.png';
  static const hospitalImage = '${root}Rectangle 102.png';
  static const labImage = '${root}Rectangle 103.png';
  static const clinicImage = '${root}Rectangle 104.png';
  static const stateBookingImage = '${root}state_booking.png';
  static const pay1Image = '${root}pay1.png';
  static const pay2Image = '${root}pay2.png';
  static const pay3Image = '${root}pay3.png';
  static const groupSuccessImage = '${root}Group 290.png';
  static const mapImage = '${root}map.png';
  static const mapingImage = '${root}maping.png';
  static const pdfImage = '${root}pdf.png';
  static const errorImage = '${root}image1.png';
  static const groupFailedImage = '${root}group_order_faild.png';

  static const successImage = '${root}Group 149.png';
  static const paymentImage = '${root}Rectangle 188.png';
  static const addressImage = '${root}Rectangle 185.png';


//--------------------------Categories Sections Images-----------------------------
  static const hospitalRoomImage = '${root}hospital_room.png';
  static const buildingImage = '${root}Rectangle 124.png';
  static const patientImage = '${root}patient.png';

//--------------------------Categories Doctor Images-----------------------------
  static const dentistryImage = '${root}dentistry.png';
  static const eyeglassesImage = '${root}eyeglasses.png';
  static const nutritionImage = '${root}nutrition.png';
  static const laboratoryImage = '${root}laboratory.png';
  static const careImage = '${root}care.png';
  static const selfCareImage = '${root}self_care.png';
  static const dentistImage = '${root}dentist.png';
  static const teethImage = '${root}teeth.png';
  static const whiteningTeethImage = '${root}whitening_teeth.png';
  static const personImage = '${root}person.png';
  static const dentistryGrey = '${root}dentistryGrey.png';

//--------------------------Products Booking Images-----------------------------
  static const bookingImage = '${root}booking.png';






}
