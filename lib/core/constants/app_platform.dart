import 'package:flutter/material.dart';

class AppPlatform {
  static Orientation getOrientation(context) =>
      MediaQuery.of(context).orientation;
}
