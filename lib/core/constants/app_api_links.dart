class AppApiLinks {
//-----------------------------baseUrl-----------------------------------

  static const baseUrl = 'https://med-sal.000webhostapp.com/api/';

  static const registerPatientApi = '${baseUrl}register/patient';


 static const registerProviderApi = '${baseUrl}register/provider';
 static const verificationCodeApi = '${baseUrl}confirm-verification-code';
 static const loginApi = '${baseUrl}login';

}
