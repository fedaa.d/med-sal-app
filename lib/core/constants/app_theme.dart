import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class AppTheme {
//----------------------Arabic Text Theme------------------------------
  static ThemeData themeArabic = ThemeData(
    fontFamily: 'Read Pro',
    splashColor: null,
    primarySwatch: null,
    useMaterial3: true,
    textTheme: TextTheme(
      titleLarge: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: AppSize.size30,
        color: AppColors.secondaryColor,
      ),
      titleMedium: const TextStyle(
          fontSize: AppSize.size20, fontWeight: FontWeight.w600),
      titleSmall: const TextStyle(
        fontSize: AppSize.size14,
      ),
      bodyLarge: const TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: AppSize.size16,
        decoration: TextDecoration.none,
      ),
      bodySmall: const TextStyle(
          fontSize: AppSize.size16,
          color: AppColors.whiteColor,
          fontWeight: FontWeight.normal),
      labelSmall: const TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size14,
          fontWeight: FontWeight.w400),
      labelMedium: const TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size14,
          fontWeight: FontWeight.w500),
      displaySmall: TextStyle(
          color: AppColors.secondaryColor,
          fontSize: AppSize.size16,
          fontWeight: FontWeight.normal),
      displayMedium: const TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size20,
          fontWeight: FontWeight.normal),
      headlineSmall: TextStyle(
          color: AppColors.secondaryGryColor,
          fontSize: AppSize.size14,
          fontWeight: FontWeight.normal),
      headlineMedium: const TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size18,
          fontWeight: FontWeight.w600),
      headlineLarge: const TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size20,
          fontWeight: FontWeight.w500),
    ),
  );
    static ThemeData themeEnglish = ThemeData(
    fontFamily: 'Read Pro',
    splashColor: null,
    primarySwatch: null,
    useMaterial3: true,
    textTheme: TextTheme(
      titleLarge: TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: AppSize.size30,
        color: AppColors.secondaryColor,
      ),
      titleMedium: const TextStyle(
          fontSize: AppSize.size20, fontWeight: FontWeight.w600),
      titleSmall: const TextStyle(
        fontSize: AppSize.size14,
      ),
      bodyLarge: const TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: AppSize.size16,
        decoration: TextDecoration.none,
      ),
      bodySmall: const TextStyle(
          fontSize: AppSize.size16,
          color: AppColors.whiteColor,
          fontWeight: FontWeight.normal),
      labelSmall: const TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size14,
          fontWeight: FontWeight.w400),
      labelMedium: const TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size14,
          fontWeight: FontWeight.w500),
      displaySmall: TextStyle(
          color: AppColors.secondaryColor,
          fontSize: AppSize.size16,
          fontWeight: FontWeight.normal),
      displayMedium: const TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size20,
          fontWeight: FontWeight.normal),
      headlineSmall: TextStyle(
          color: AppColors.secondaryGryColor,
          fontSize: AppSize.size14,
          fontWeight: FontWeight.normal),
      headlineMedium: const TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size18,
          fontWeight: FontWeight.w600),
      headlineLarge: const TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size20,
          fontWeight: FontWeight.w500),
    ),
  );
}
