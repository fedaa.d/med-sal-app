import 'package:flutter/material.dart';
import 'package:mad_sal/core/functions/hex_color.dart';

class AppColors {
  static Color mainColor = HexColor('#15AB98');
  static Color secondaryColor = HexColor('#227B70');
  static Color lightMainColor = HexColor('#E1EBE9');
  static Color lightWhiteColor = HexColor('#C6C6C6');
  static Color lightbackGroundColor = HexColor('#F5F2F2');
  static Color lightgreyColor = HexColor('#F5F5F5');
  static Color lightpinkColor = HexColor('#FAF3F3');
  static Color lightColor = HexColor('#D9D9D9');
  static Color borderColor = HexColor('#CDC5C5');
  static Color darkColor = HexColor('#1C1B1F');
  static Color cardColor = HexColor('#FAFAFA');
  static Color lightBlackColor = HexColor('#938D8D');
  static Color secondaryGryColor = HexColor('#A9A5A5');
  static Color reqColor = HexColor('#44C6B5');
  static Color torquoise = HexColor('#047D6E');
  static Color subColor = HexColor('#313033');
  static Color darkGryColor = HexColor('#807A7A');
  static Color lightTealColor = HexColor('#19CCB6');
  static Color lighGreenColor = HexColor('#098575');
  static Color lightSecondaryColor = HexColor('#12B09C');
  static Color lightTorquoiseColor = HexColor('#0A7265');
  static Color lightHerbalColor = HexColor('#CCEBE7');
  static Color gryTextColor = HexColor('#9B9595');
  static Color lightGryColor = HexColor('#F1EDED');
  static Color redHotColor = HexColor('#BE1313');
  static Color glowingGreenColor = HexColor('#38EED7');
  static Color darkBorderColor = HexColor('#938D8D');
  static Color orangeColor = HexColor('#F69842');
  static Color lightRedColor = HexColor('#EE0606');

  static const Color gryColor = Colors.grey;
  static const Color blackColor = Colors.black;
  static const Color whiteColor = Colors.white;
  static const Color tealColor = Colors.teal;
  static const Color redColor = Colors.red;
  static const Color transparentColor = Colors.transparent;
}
