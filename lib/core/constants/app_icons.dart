class AppIcons {
  static const root = 'assets/icons/';

//--------------------------Root Icons--------------------------
  static const homeIcon = '${root}vector.svg';
  static const profileIcon = '${root}profile.svg';
  static const bagIcon = '${root}bag.svg';
  static const mailIcon = '${root}mail.svg';
  static const helpIcon = '${root}help.svg';

//--------------------------Auth Icons----------------------------
  static const businesIcon = '${root}busines.svg';
  static const editIcon = '${root}edit.svg';
  static const arrowDropDownIcon = '${root}arrow_drop_down.svg';
  static const uploadIcon = '${root}upload.svg';
  static const visibilityLockIcon = '${root}visibility_lock.svg';
  static const visibilityOffIcon = '${root}visibility_off.svg';

//--------------------------Home Icons-----------------------------
  static const notificationIcon = '${root}notification.svg';
  static const callIcon = '${root}call.svg';
  static const alternateEmailIcon = '${root}alternate_email.svg';
  static const formatListIcon = '${root}list.svg';
  static const arrowIcon = '${root}arrow_forward.svg';
  static const filterIcon = '${root}filter_alt.svg';

//--------------------------Categories Icons-----------------------
  static const syncLockIcon = '${root}sync_lock.svg';
  static const notlistedLocationIcon = '${root}not_listed_location.svg';
  static const clearNightIcon = '${root}clear_night.svg';
  static const loghOutIcon = '${root}move_item.svg';
  static const calendarIcon = '${root}calendar_month.svg';
  static const listIcon = '${root}list.svg';
  static const blackCircle = '${root}Ellipse 53.svg';
  static const settingsIcon = '${root}settings.svg';

  //--------------------------Categories Doctor Icons-----------------------------
  static const moreVertIcon = '${root}more_vert.svg';
  static const arrowDownIcon = '${root}arrow_down_ios.svg';
  static const searchIcon = '${root}search.svg';

  //--------------------------Admin Option Icons-----------------------------
  static const logoutIcon = '${root}move_item(2).svg';
}
