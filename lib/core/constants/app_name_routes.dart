class AppNameRoutes {
  static const splashScreen = '/';
  static const rootScreen = '/RootScreen';
  static const registerPatientScreen = '/RegisterPatientScreen';
  static const registerProviderScreen = '/RegisterProviderScreen';
  static const checkEmailScreen = '/CheckEmailScreen';
  static const verifyCodeScreen = '/VerifyCodeScreen';
  static const login = '/LoginScreen';
  static const forgotPasswordScreen = '/ForgotPasswordScreen';
  static const resetPasswordScreen = '/ResetPasswordScreen';
  static const settingScreen = '/SettingScreen';
  static const profileScreen = '/ProfileScreen';

  static const dentistryScreen = '/DentistryScreen';
  static const availableDoctorsScreen = '/AvailableDoctorsScreen';
  static const doctorBookScreen = '/DoctorBookScreen';
  static const doctorAppointmentScreen = '/DoctorAppointmentScreen';
  static const hospitalScreen = '/HospitalScreen';
  static const clinicScreen = '/ClinicScreen';
  static const labsScreen = '/LabsScreen';
  static const pharmaciesScreen = '/PharmaciesScreen';
  static const categoriesDoctorScreen = '/CategoriesDoctorScreen';

  static const productBookingScreen = '/ProductBookingScreen';
  static const servicesBookingScreen = '/ServicesBookingScreen';
  static const detailsServicesScreen = '/DetailsServicesScreen';
  static const serviceAppointmentScreen = '/ServiceAppointmentScreen';
  static const addProductScreen = '/AddProductScreen';
  static const schedulScreen = '/SchedulScreen';
  static const orderSccessScreen = '/OrderSccessScreen';
  static const orderFiledScreen = '/OrderFiledScreen';
  static const orderScreen = '/OrderScreen';
  static const orderDetailsScreen = '/OrderDetailsScreen';

  static const deliveryReportScreen = '/DeliveryReportScreen';
  static const salesScreen = '/SalesScreen';
  static const servicesAdminScreen = '/ServicesAdminScreen';
  static const productsAdminScreen = '/ProductsAdminScreen';

  static const spRequestRigistScreen = '/SpRequestRigistScreen';
  static const histogramScreen = '/HistogramScreen';
  static const filterSearchScreen = '/FilterSearchScreen';
  static const optionScreen = '/OptionScreen';
  static const permissionListScreen = '/PermissionListScreen';
  static const datailsRegestrationScreen = '/DatailsRegestrationScreen';
  static const stateBookingScreen = '/StateBookingScreen';
  static const categoriesScreen = '/CategoriesScreen';
  static const editingServicesScreen = '/EditingServicesScreen';
  static const editingProductsScreen = '/EditingProductsScreen';
}
