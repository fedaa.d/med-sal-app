
// //-------------------------------------------------------------Custom Alert for exit app

// import 'package:med_sal/core/constant/app_packages.dart';

// Future<bool> exitApp() {
//   Get.generalDialog(
//     pageBuilder: (
//       BuildContext context,
//       Animation<double> animation,
//       Animation<double> secondaryAnimation,
//     ) {
//       return const CustomExitAppWidget().animate().fade().slideY(
//             duration: 250.ms,
//             begin: -1,
//             curve: Curves.linearToEaseOut,
//           );
//     },
//   );
//   return Future.value(true);
// }
