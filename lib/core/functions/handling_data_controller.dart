import 'package:mad_sal/core/class/status_request.dart';

/*  function for check status request 
    and handling all of states*/

handlingData(response) {
  if (response is StatusRequest) {
    return response;
  } else {
    return StatusRequest.success;
  }
}
