import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_regular_expression.dart';

//Class for validate all of input fields
class FieldsValidators {
  //validate user name
  static String? userNameValidator(final String? value) {
    if (value == null || value.isEmpty) {
      return '9'.tr;
    }

    if (value.length < 3) {
      return '10'.tr;
    }

    return null;
  }

  //validate full name
  static String? fullNameValidator(final String? value) {
    if (value == null || value.isEmpty) {
      return '11'.tr;
    }

    if (value.length < 3) {
      return '12'.tr;
    }

    return null;
  }

  //validate phone number
  static String? phoneNumberValidator(final String? value) {
    if (value == null || value.isEmpty) {
      return '12'.tr;
    }

    if (value.length < 13) {
      return '13'.tr;
    }

    // if (!RegExp(ApRegularExpression.phoneValidator).hasMatch(value)) {
    //   return '15'.tr;
    // }
    return null;
  }

  //validate password
  static String password = '';
  static String? passwordValidator(final String? value) {
    password = value!;
    if (value.isEmpty) {
      return '16'.tr;
    }

    if (value.length < 8) {
      return '17'.tr;
    }

    return null;
  }

  //validate confirm password
  static String? confirmPasswordValidator(final String? value) {
    if (value == null || value.isEmpty) {
      return '18'.tr;
    }
    if (password != value) {
      return '19'.tr;
    }
    return null;
  }

  //validate empty input fields
  static String? emptyValidator(final String? value) {
    if (value == null || value.isEmpty) {
      return '20'.tr;
    }

    return null;
  }
}
