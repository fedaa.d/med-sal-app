import 'dart:convert';
import 'package:dartz/dartz.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:mad_sal/core/class/status_request.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/functions/check_internet.dart';
import 'package:mad_sal/core/shared/custom_snackbar.dart';

class Crud {
  //A function through which data can be post and get
  Future<Either<StatusRequest, Map>> postData(
    final String linkUrl,
    final Map body, [
    final String? token,
  ]) async {
    try {
      //Test the presence of an Internet connection
      if (await checkInternet()) {
        var response = await http.post(
          Uri.parse(linkUrl),
          headers: {
            'Accept': 'application/json',
            'app_key': 'base64:6+VIIByNpQQSJ3wbs0YQUCVJn68kxH1qkK48kz1KLDM=',
            if (token != null) 'Authorization': 'Bearer $token',
          },
          body: body,
        );
        print(response.body);

        Get.closeAllSnackbars();
        //Test the status of the API connection
        if (response.statusCode == 200) {
          final Map responseBody = jsonDecode(response.body);

          //Test if the operation was completed successfully
          if (responseBody.containsKey('message')) {
            final String message = responseBody['message'];
            customSnackBar(title: 'اشعار', message: message);
          }
          int status = 1;
          if (responseBody.containsKey('status')) {
            status = responseBody['status'];
            if (status != 1) {
              return const Left(StatusRequest.serverfailure);
            }
          }
          return Right(responseBody);
        } else {
          if (response.statusCode == 422) {
            final Map responseBody = jsonDecode(response.body);

            //Test if the operation was completed successfully
            if (responseBody.containsKey('message')) {
              final String message = responseBody['message'];
              customSnackBar(title: 'فشل', message: message);
            } else {
              customSnackBar(title: 'فشل', message: 'حدث خطأ ما');
            }
          } else if (response.statusCode == 401) {
            customSnackBar(
              title: 'فشل',
              message: 'انتهت صلاحية الجلسة قم بتسجيل الدخول من جديد',
            );
            await Get.offAndToNamed(AppNameRoutes.login);
            Get.deleteAll();
            Get.put(Crud());
            return const Left(StatusRequest.none);
          } else {
            customSnackBar(title: 'فشل', message: 'حدث خطأ ما');
          }
          return const Left(StatusRequest.serverfailure);
        }
      } else {
        customSnackBar(title: 'فشل', message: 'لا يوجد اتصال بالانترنت');
        return const Left(StatusRequest.offlinefailure);
      }
    } catch (e) {
      customSnackBar(title: 'فشل', message: 'قم بإعادة المحاولة مرة اخرى');
      return const Left(StatusRequest.offlinefailure);
    }
  }

//----------------------------------------------------------------

  //A function through which data can be get
  Future<Either<StatusRequest, Map>> getData(
    final String linkUrl, [
    final String? token,
  ]) async {
    try {
      //Test the presence of an Internet connection
      if (await checkInternet()) {
        var response = await http.get(Uri.parse(linkUrl), headers: {
          'Accept': 'application/json',
          'app_key': 'base64:6+VIIByNpQQSJ3wbs0YQUCVJn68kxH1qkK48kz1KLDM=',
          if (token != null) 'Authorization': 'Bearer $token',
        });
        print(response.body);
        Get.closeAllSnackbars();
        //Test the status of the API connection
        if (response.statusCode == 200) {
          final Map responseBody = jsonDecode(response.body);

          //Test if the operation was completed successfully
          if (responseBody.containsKey('message')) {
            final String message = responseBody['message'];
            customSnackBar(title: 'اشعار', message: message);
          }
          int status = 1;
          if (responseBody.containsKey('status')) {
            status = responseBody['status'];
          }
          if (status != 1) {
            return const Left(StatusRequest.serverfailure);
          }
          return Right(responseBody);
        } else {
          if (response.statusCode == 422) {
            final Map responseBody = jsonDecode(response.body);

            //Test if the operation was completed successfully
            if (responseBody.containsKey('message')) {
              final String message = responseBody['message'];
              customSnackBar(title: 'فشل', message: message);
            }
          } else if (response.statusCode == 401) {
            customSnackBar(
              title: 'فشل',
              message: 'انتهت صلاحية الجلسة قم بتسجيل الدخول من جديد',
            );
            await Get.offAndToNamed(AppNameRoutes.login);
            Get.deleteAll();
            Get.put(Crud());
            return const Left(StatusRequest.none);
          } else {
            customSnackBar(title: 'فشل', message: 'حدث خطأ ما');
          }
          return const Left(StatusRequest.serverfailure);
        }
      } else {
        customSnackBar(title: 'فشل', message: 'لا يوجد اتصال بالانترنت');
        return const Left(StatusRequest.offlinefailure);
      }
    } catch (e) {
      customSnackBar(title: 'فشل', message: 'قم بإعادة المحاولة مرة اخرى');
      return const Left(StatusRequest.offlinefailure);
    }
  }

//----------------------------------------------------------------

  //A function through which data can be deleted
  Future<Either<StatusRequest, Map>> deletedData(
    final String linkUrl,
    final Map body, [
    final String? token,
  ]) async {
    try {
      //Test the presence of an Internet connection
      if (await checkInternet()) {
        var response = await http.delete(
          Uri.parse(linkUrl),
          headers: {
            'Accept': 'application/json',
            'app_key': 'base64:6+VIIByNpQQSJ3wbs0YQUCVJn68kxH1qkK48kz1KLDM=',
            if (token != null) 'Authorization': 'Bearer $token',
          },
          body: body,
        );
        Get.closeAllSnackbars();

        //Test the status of the API connection
        if (response.statusCode == 200) {
          final Map responseBody = jsonDecode(response.body);

          //Test if the operation was completed successfully
          if (responseBody.containsKey('message')) {
            final String message = responseBody['message'];
            customSnackBar(title: 'اشعار', message: message);
          }
          int status = 1;
          if (responseBody.containsKey('status')) {
            status = responseBody['status'];
          }
          if (status != 1) {
            return const Left(StatusRequest.serverfailure);
          }
          return Right(responseBody);
        } else {
          if (response.statusCode == 422) {
            final Map responseBody = jsonDecode(response.body);

            //Test if the operation was completed successfully
            if (responseBody.containsKey('message')) {
              final String message = responseBody['message'];
              customSnackBar(title: 'فشل', message: message);
            }
          } else if (response.statusCode == 401) {
            customSnackBar(
              title: 'فشل',
              message: 'انتهت صلاحية الجلسة قم بتسجيل الدخول من جديد',
            );
            await Get.offAndToNamed(AppNameRoutes.login);
            Get.deleteAll();
            Get.put(Crud());
            return const Left(StatusRequest.none);
          } else {
            customSnackBar(title: 'فشل', message: 'حدث خطأ ما');
          }

          return const Left(StatusRequest.serverfailure);
        }
      } else {
        Get.closeAllSnackbars();
        customSnackBar(title: 'فشل', message: 'لا يوجد اتصال بالانترنت');
        return const Left(StatusRequest.offlinefailure);
      }
    } catch (e) {
      Get.closeAllSnackbars();
      customSnackBar(title: 'فشل', message: 'قم بإعادة المحاولة مرة اخرى');
      return const Left(StatusRequest.offlinefailure);
    }
  }

//----------------------------------------------------------------

  //A function through which data can be updated
  Future<Either<StatusRequest, Map>> updateData(
    final String linkUrl,
    final Map body, [
    final String? token,
  ]) async {
    try {
      //Test the presence of an Internet connection
      if (await checkInternet()) {
        var response = await http.patch(
          Uri.parse(linkUrl),
          headers: {
            'Accept': 'application/json',
            'app_key': 'base64:6+VIIByNpQQSJ3wbs0YQUCVJn68kxH1qkK48kz1KLDM=',
            if (token != null) 'Authorization': 'Bearer $token',
          },
          body: body,
        );

        //Test the status of the API connection
        if (response.statusCode == 200) {
          final Map responseBody = jsonDecode(response.body);

          //Test if the operation was completed successfully
          if (responseBody.containsKey('message')) {
            final String message = responseBody['message'];
            customSnackBar(title: 'اشعار', message: message);
          }
          int status = 1;
          if (responseBody.containsKey('status')) {
            status = responseBody['status'];
          }
          if (status != 1) {
            return const Left(StatusRequest.serverfailure);
          }
          return Right(responseBody);
        } else {
          if (response.statusCode == 422) {
            final Map responseBody = jsonDecode(response.body);

            //Test if the operation was completed successfully
            if (responseBody.containsKey('message')) {
              final String message = responseBody['message'];
              customSnackBar(title: 'فشل', message: message);
            }
          } else if (response.statusCode == 401) {
            customSnackBar(
              title: 'فشل',
              message: 'انتهت صلاحية الجلسة قم بتسجيل الدخول من جديد',
            );
            await Get.offAndToNamed(AppNameRoutes.login);
            Get.deleteAll();
            Get.put(Crud());
            return const Left(StatusRequest.none);
          } else {
            customSnackBar(title: 'فشل', message: 'حدث خطأ ما');
          }

          return const Left(StatusRequest.serverfailure);
        }
      } else {
        Get.closeAllSnackbars();
        customSnackBar(title: 'فشل', message: 'لا يوجد اتصال بالانترنت');
        return const Left(StatusRequest.offlinefailure);
      }
    } catch (e) {
      Get.closeAllSnackbars();
      customSnackBar(title: 'فشل', message: 'قم بإعادة المحاولة مرة اخرى');
      return const Left(StatusRequest.offlinefailure);
    }
  }

//----------------------------------------------------------------

  //A function through which data can be modified or put
  Future<Either<StatusRequest, Map>> putData(
    final String linkUrl,
    final Map body, [
    final String? token,
  ]) async {
    try {
      //Test the presence of an Internet connection
      if (await checkInternet()) {
        var response = await http.put(
          Uri.parse(linkUrl),
          headers: {
            'Accept': 'application/json',
            'app_key': 'base64:6+VIIByNpQQSJ3wbs0YQUCVJn68kxH1qkK48kz1KLDM=',
            if (token != null) 'Authorization': 'Bearer $token',
          },
          body: body,
        );

        Get.closeAllSnackbars();

        //Test the status of the API connection
        if (response.statusCode == 200) {
          final Map responseBody = jsonDecode(response.body);

          //Test if the operation was completed successfully
          if (responseBody.containsKey('message')) {
            final String message = responseBody['message'];
            customSnackBar(title: 'اشعار', message: message);
          }
          int status = 1;
          if (responseBody.containsKey('status')) {
            status = responseBody['status'];
          }
          if (status != 1) {
            return const Left(StatusRequest.serverfailure);
          }

          return Right(responseBody);
        } else {
          if (response.statusCode == 422) {
            final Map responseBody = jsonDecode(response.body);

            //Test if the operation was completed successfully
            if (responseBody.containsKey('message')) {
              final String message = responseBody['message'];
              customSnackBar(title: 'فشل', message: message);
            }
          } else if (response.statusCode == 401) {
            customSnackBar(
              title: 'فشل',
              message: 'انتهت صلاحية الجلسة قم بتسجيل الدخول من جديد',
            );
            await Get.offAndToNamed(AppNameRoutes.login);
            Get.deleteAll();
            Get.put(Crud());
            return const Left(StatusRequest.none);
          } else {
            customSnackBar(title: 'فشل', message: 'حدث خطأ ما');
          }

          return const Left(StatusRequest.serverfailure);
        }
      } else {
        customSnackBar(title: 'فشل', message: 'لا يوجد اتصال بالانترنت');
        return const Left(StatusRequest.offlinefailure);
      }
    } catch (e) {
      customSnackBar(title: 'فشل', message: 'قم بإعادة المحاولة مرة اخرى');
      return const Left(StatusRequest.offlinefailure);
    }
  }
}
