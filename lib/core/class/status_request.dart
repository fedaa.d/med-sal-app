//-------------------------------------define all of status request

enum StatusRequest {
  none,
  loading,
  success,
  failure,
  serverfailure,
  serverException,
  offlinefailure
}
