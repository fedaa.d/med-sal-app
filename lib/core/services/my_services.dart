import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

class MyServices extends GetxService {
  late GetStorage getBox;

  Future<MyServices> init() async {
//----------------------------------------------------------------

    WidgetsFlutterBinding.ensureInitialized();

//----------------------------------------------------------------

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

//----------------------------------------------------------------

    await GetStorage.init();
    getBox = GetStorage();

//----------------------------------------------------------------

    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(
        systemNavigationBarColor: AppColors.lightgreyColor,
        statusBarColor: AppColors.lightgreyColor,
      ),
    );

//----------------------------------------------------------------

    return this;
  }
}

initialServices() async {
  await Get.putAsync(() => MyServices().init());
}
