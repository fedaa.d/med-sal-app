import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_theme.dart';
import 'package:mad_sal/core/services/my_services.dart';

class LocaleController extends GetxController {
  Locale? language;

  MyServices myServices = Get.find();

  ThemeData appTheme = AppTheme.themeArabic;

  changeLang(String languageCode) {
    Locale locale = Locale(languageCode);
    myServices.getBox.write("lang", languageCode);
    appTheme = languageCode == "ar" ? AppTheme.themeArabic : AppTheme.themeEnglish;
    Get.changeTheme(appTheme);
    Get.updateLocale(locale);
  }

  @override
  void onInit() {
    String? sharedPrefLang = myServices.getBox.read("lang");
    if (sharedPrefLang == "ar") {
      language = const Locale("ar");
      appTheme = AppTheme.themeArabic;
    } else if (sharedPrefLang == "en") {
      language = const Locale("en");
      appTheme = AppTheme.themeEnglish;
    } else {
      language = Locale(Get.deviceLocale!.languageCode);
      appTheme = AppTheme.themeEnglish;
    }
    super.onInit();
  }
}
