import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

void customSnackBar({
  required String title,
  required String message,
}) =>
    Get.showSnackbar(
      GetSnackBar(
        backgroundColor: AppColors.mainColor,
        duration: const Duration(seconds: 3),
        borderRadius: 20,
        titleText: Text(
          title,
          style: Get.context!.textTheme.titleLarge!.copyWith(
            color: AppColors.blackColor,
          ),
        ),
        messageText: Text(
          message,
          style: Get.context!.textTheme.bodyMedium!.copyWith(
            color: AppColors.blackColor,
          ),
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
