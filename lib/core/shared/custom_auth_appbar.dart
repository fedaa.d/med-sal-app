import 'package:mad_sal/core/constants/app_packages.dart';

AppBar customAuthAppBar({required final String title}) {
  return AppBar(
    systemOverlayStyle: SystemUiOverlayStyle(
      statusBarColor: AppColors.lightgreyColor,
      statusBarIconBrightness: Brightness.dark,
      statusBarBrightness: Brightness.light,
    ),
    centerTitle: true,
    toolbarHeight: 80.h,
    backgroundColor: AppColors.lightgreyColor,
    elevation: 0,
    title: Text(
      title,
      style: const TextStyle(
        fontSize: AppSize.size25,
        fontWeight: FontWeight.w600,
        color: AppColors.blackColor,
      ),
    ),
  );
}
