import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class CustomButton extends StatelessWidget {
  final String text;
  final Function() onPressed;
  final double width;
  final double height;
  final bool? isLoading;

  const CustomButton({
    super.key,
    required this.text,
    required this.onPressed,
    required this.width,
    required this.height,
    this.isLoading = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: isLoading! ? null : width,
      height: height,
      decoration: BoxDecoration(
        color: AppColors.mainColor,
        borderRadius: isLoading! ? null : BorderRadius.circular(10),
        shape: isLoading! ? BoxShape.circle : BoxShape.rectangle,
      ),
      child: MaterialButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        splashColor: !isLoading! ? null : Colors.transparent,
        highlightColor: !isLoading! ? null : Colors.transparent,
        onPressed: onPressed,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Visibility(
              visible: !isLoading!,
              child: Text(
                text,
                style: const TextStyle(
                  color: AppColors.whiteColor,
                  fontSize: AppSize.size20,
                  fontWeight: FontWeight.w500,
                ),
              ),
            ),
            Visibility(
              visible: isLoading!,
              child: const CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(AppColors.whiteColor),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
