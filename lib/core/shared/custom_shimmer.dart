// import 'package:flutter/material.dart';
// import 'package:mad_sal/core/constants/app_packages.dart';
// import 'package:shimmer/shimmer.dart';

// class CustomShimmerWidget extends StatelessWidget {
//   final double? width;
//   final double? height;
//   final bool isCircle;

//   const CustomShimmerWidget({
//     super.key,
//     this.width,
//     this.height,
//     this.isCircle = false,
//   });

//   @override
//   Widget build(BuildContext context) {
//     return Shimmer.fromColors(
//       direction: ShimmerDirection.rtl,
//       baseColor: Colors.grey[300]!,
//       highlightColor: Colors.grey[100]!,
//       child: Container(
//         width: width?.w,
//         height: height?.h,
//         decoration: BoxDecoration(
//           color: Colors.black,
//           shape: isCircle ? BoxShape.circle : BoxShape.rectangle,
//           borderRadius: isCircle ? null : BorderRadius.circular(8),
//         ),
//       ),
//     );
//   }
// }
