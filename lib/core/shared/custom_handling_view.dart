import 'package:flutter/material.dart';
import 'package:mad_sal/core/class/status_request.dart';

class CustomHandlingView extends StatelessWidget {
  final StatusRequest statusRequest;
  final Widget basicWidget;
  final Widget loadingWidget;
  final Widget offlineWidget;
  final Widget errorWidget;

  const CustomHandlingView({
    super.key,
    required this.statusRequest,
    required this.loadingWidget,
    required this.basicWidget,
    required this.offlineWidget,
    required this.errorWidget,
  });

  @override
  Widget build(BuildContext context) {
    return statusRequest == StatusRequest.loading
        ? loadingWidget
        : statusRequest == StatusRequest.offlinefailure
            ? offlineWidget
            : statusRequest == StatusRequest.serverfailure
                ? errorWidget
                : statusRequest == StatusRequest.failure
                    ? errorWidget
                    : basicWidget;
  }
}
