import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class CustomTextFelid extends StatelessWidget {
  final TextEditingController controller;
  final String? label;
  final TextInputType? keyboardType;
  final String hint;
  final Widget? icon;
  final bool? obscure;
  final Function? validator;

  const CustomTextFelid({
    super.key,
    required this.controller,
    this.keyboardType,
    this.label,
    required this.hint,
    this.icon,
    this.obscure,
    this.validator,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: AppColors.darkGryColor,
      style: const TextStyle(
        fontWeight: FontWeight.w400,
        color: AppColors.blackColor,
        fontSize: AppSize.size16,
      ),
      autovalidateMode: AutovalidateMode.onUserInteraction,
      controller: controller,
      keyboardType: keyboardType,
      validator: validator != null ? (value) => validator!(value) : null,
      obscureText: obscure ?? false,
      decoration: InputDecoration(
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15.0),
          borderSide: BorderSide(
            width: 1.0,
            color: AppColors.lightBlackColor,
          ),
        ),
        labelText: label,
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.lightWhiteColor),
          borderRadius: BorderRadius.circular(15),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.lightWhiteColor),
          borderRadius: BorderRadius.circular(15),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.lightWhiteColor),
          borderRadius: BorderRadius.circular(15),
        ),
        labelStyle: const TextStyle(
          fontSize: AppSize.size18,
          fontWeight: FontWeight.w400,
          color: AppColors.blackColor,
        ),
        hintText: hint,
        hintStyle: TextStyle(
          fontSize: AppSize.size16,
          fontWeight: FontWeight.w400,
          color: AppColors.lightWhiteColor,
        ),
        suffixIcon: icon,
        suffixIconColor: AppColors.lightWhiteColor,
      ),
    );
  }
}
