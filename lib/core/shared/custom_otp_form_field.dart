import 'package:flutter_pin_code_fields/flutter_pin_code_fields.dart';
import 'package:mad_sal/core/constants/app_packages.dart';

class CustomOtpForm extends StatelessWidget {
  final Function(String pin) onCompleted;
  const CustomOtpForm({super.key, required this.onCompleted});

  @override
  Widget build(BuildContext context) {
    return PinCodeFields(
      length: 6,
      fieldBorderStyle: FieldBorderStyle.bottom,
      responsive: true,
      autofocus: true,
      activeBorderColor: AppColors.mainColor,
      keyboardType: TextInputType.number,
      autoHideKeyboard: true,
      textStyle: context.textTheme.titleMedium!.copyWith(
        color: AppColors.blackColor,
      ),
      animation: Animations.slideInUp,
      onComplete: (String value) => onCompleted(value),
    );
  }
}
