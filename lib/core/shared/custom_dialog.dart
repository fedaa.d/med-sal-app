import 'package:mad_sal/core/constants/app_packages.dart';

void customDialog({required String title, required String subTitle}) {
  Get.defaultDialog(
      content: SingleChildScrollView(
    child: AlertDialog(
      title: Container(
        padding: EdgeInsets.symmetric(vertical: 40.h),
        color: AppColors.lightgreyColor,
        child: Text(
          title,
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontSize: AppSize.size25,
            fontWeight: FontWeight.w400,
            color: AppColors.blackColor,
          ),
        ),
      ),
      content: Column(
        children: [
          Image.asset(AppImages.groupSuccessImage),
          const CustomVerticalSizedBox(30),
          Text(
            subTitle,
            style: const TextStyle(
              fontSize: AppSize.size20,
              fontWeight: FontWeight.w400,
              color: AppColors.blackColor,
            ),
          ),
        ],
      ),
      insetPadding: const EdgeInsets.symmetric(horizontal: 30, vertical: 150),
    ),
  ));
}
