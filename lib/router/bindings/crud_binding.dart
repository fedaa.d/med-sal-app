import 'package:get/get.dart';
import 'package:mad_sal/core/class/crud.dart';

class CrudBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(Crud());
  }
}
