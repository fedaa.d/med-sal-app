import 'package:get/get.dart';
import 'package:mad_sal/controller/auth/register_patient_controller.dart';

class RegisterPatientBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(RegisterPatientControllerImp());
  }
}
