import 'package:mad_sal/core/constants/app_packages.dart';

class CheckEmailBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(CheckEmailController());
  }
}
