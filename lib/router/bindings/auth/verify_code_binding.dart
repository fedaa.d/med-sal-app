import 'package:mad_sal/core/constants/app_packages.dart';

class VerifyCodeBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(VerificationCodeControllerImp());
  }
}