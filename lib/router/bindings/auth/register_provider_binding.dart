import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterProviderBinding extends Bindings{
  @override
  void dependencies() {
    Get.put(RegisterProviderControllerImp());
  }
}