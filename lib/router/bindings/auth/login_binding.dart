import 'package:mad_sal/core/constants/app_packages.dart';

class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(LoginControllerImp());
  }
}
