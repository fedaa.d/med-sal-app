import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/router/bindings/auth/check_email_binding.dart';
import 'package:mad_sal/router/bindings/auth/login_binding.dart';
import 'package:mad_sal/router/bindings/auth/register_patient_binding.dart';
import 'package:mad_sal/router/bindings/auth/register_provider_binding.dart';
import 'package:mad_sal/router/bindings/auth/verify_code_binding.dart';
import 'package:mad_sal/view/auth/check_email/screens/check_email.dart';
import 'package:mad_sal/view/auth/screens/forgot_password.dart';
import 'package:mad_sal/view/auth/login/screens/login_screen.dart';
import 'package:mad_sal/view/auth/register_patient/screens/register_patient.dart';
import 'package:mad_sal/view/auth/register_provider/screens/register_provider.dart';
import 'package:mad_sal/view/auth/screens/reset_password.dart';
import 'package:mad_sal/view/auth/verify_code/screens/verify_code.dart';
import 'package:mad_sal/view/roots/categories/add_products/add_product1/screens/add_product1.dart';
import 'package:mad_sal/view/roots/categories/admin_option/screens/admin_option_screen.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/appointment/screens/appointment_screen.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/screens/available_doctors_screen.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/screens/dentistry_screen.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/schedule/screens/my_schedule_screen.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/screens/categories_doctor.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/state_booking/screens/state_booking_screen.dart';
import 'package:mad_sal/view/roots/categories/clinic/screens/clinic_screen.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/screens/delivery_state_screen.dart';
import 'package:mad_sal/view/roots/categories/doctor_book/screens/doctor_book_screen.dart';
import 'package:mad_sal/view/roots/categories/editing_products/screens/editing_products.dart';
import 'package:mad_sal/view/roots/categories/editing_services/screens/editing_services.dart';
import 'package:mad_sal/view/roots/categories/filter_search/screens/filter_search_screen.dart';
import 'package:mad_sal/view/roots/categories/hospitals/screens/hospital_screen.dart';
import 'package:mad_sal/view/roots/categories/lab/screens/labs_screen.dart';
import 'package:mad_sal/view/roots/categories/order/my_order/screens/order_screen.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/screens/order_details.dart';
import 'package:mad_sal/view/roots/categories/order/order_faild/screens/order_failed.dart';
import 'package:mad_sal/view/roots/categories/order/success/screens/order_success_screen.dart';
import 'package:mad_sal/view/roots/categories/permissions_list/screens/permissions_list_admins.dart';
import 'package:mad_sal/view/roots/categories/pharmacies/pharmacies_screen.dart';
import 'package:mad_sal/view/roots/categories/products_admin/screens/products_admin_screen.dart';
import 'package:mad_sal/view/roots/categories/products_booking/details/screens/details_screen.dart';
import 'package:mad_sal/view/roots/categories/products_booking/screens/products_booking_screen.dart';
import 'package:mad_sal/view/roots/categories/registration_details/screens/registrations_details_screen.dart';
import 'package:mad_sal/view/roots/categories/sales/histogram/screens/histogram_screen.dart';
import 'package:mad_sal/view/roots/categories/sales/screens/sales_screen.dart';
import 'package:mad_sal/view/roots/categories/screens/categories_screen.dart';
import 'package:mad_sal/view/roots/categories/services_admin/screens/services_admin_screen.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/screens/service_appointment.dart';
import 'package:mad_sal/view/roots/categories/sp_request_regist/screens/sp_request_regist.dart';
import 'package:mad_sal/view/roots/profile/screens/profile.dart';
import 'package:mad_sal/view/roots/root/screens/root_screen.dart';
import 'package:mad_sal/view/roots/services_booking/screens/services_booking_screen.dart';
import 'package:mad_sal/view/roots/setting/screens/setting_screen.dart';

class AppRoutes {
  static final routes = [
    GetPage(
      name: AppNameRoutes.registerPatientScreen,
      page: () => const RegisterPatientScreen(),
      binding: RegisterPatientBinding(),
    ),
    GetPage(
      name: AppNameRoutes.registerProviderScreen,
      page: () => const RegisterProviderScreen(),
      binding: RegisterProviderBinding(),
    ),
    GetPage(
      name: AppNameRoutes.checkEmailScreen,
      page: () => const CheckEmailScreen(),
      binding: CheckEmailBinding(),
    ),
    GetPage(
      name: AppNameRoutes.verifyCodeScreen,
      page: () => const VerifyCodeScreen(),
      binding: VerifyCodeBinding(),
    ),
    GetPage(
      name: AppNameRoutes.login,
      page: () => const LoginScreen(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: AppNameRoutes.forgotPasswordScreen,
      page: () => const ForgotPassword(),
    ),
    GetPage(
      name: AppNameRoutes.resetPasswordScreen,
      page: () => const ResetPassword(),
    ),
    GetPage(
      name: AppNameRoutes.settingScreen,
      page: () => const SettingScreen(),
    ),
    GetPage(
      name: AppNameRoutes.productsAdminScreen,
      page: () => const ProductsAdminScreen(),
    ),
    GetPage(
      name: AppNameRoutes.rootScreen,
      page: () => const RootScreen(),
    ),
    GetPage(
      name: AppNameRoutes.profileScreen,
      page: () => const ProfileScreen(),
    ),
    GetPage(
      name: AppNameRoutes.dentistryScreen,
      page: () => const DentistryScreen(),
    ),
    GetPage(
      name: AppNameRoutes.productBookingScreen,
      page: () => const ProducstBookingScreen(),
    ),
    GetPage(
      name: AppNameRoutes.servicesBookingScreen,
      page: () => const ServicesBookingScreen(),
    ),
    GetPage(
      name: AppNameRoutes.detailsServicesScreen,
      page: () => const DetailsServicesScreen(),
    ),
    GetPage(
      name: AppNameRoutes.availableDoctorsScreen,
      page: () => const AvailableDoctorsScreen(),
    ),
    GetPage(
      name: AppNameRoutes.doctorBookScreen,
      page: () => const DoctorBookScreen(),
    ),
    GetPage(
      name: AppNameRoutes.doctorAppointmentScreen,
      page: () => const DoctorAppointmentScreen(),
    ),
    GetPage(
      name: AppNameRoutes.serviceAppointmentScreen,
      page: () => ServiceAppointment(),
    ),
    GetPage(
      name: AppNameRoutes.addProductScreen,
      page: () => const AddProduct1(),
    ),
    GetPage(
      name: AppNameRoutes.schedulScreen,
      page: () => const MyScheduleScreen(),
    ),
    GetPage(
      name: AppNameRoutes.orderSccessScreen,
      page: () => const OrderSuccessScreen(),
    ),
    GetPage(
      name: AppNameRoutes.orderFiledScreen,
      page: () => const OrderFailedScreen(),
    ),
    GetPage(
      name: AppNameRoutes.orderScreen,
      page: () => const MyOrderScreen(),
    ),
    GetPage(
      name: AppNameRoutes.orderDetailsScreen,
      page: () => const OrderDetails(),
    ),
    GetPage(
      name: AppNameRoutes.deliveryReportScreen,
      page: () => const DeliveryStateScreen(),
    ),
    GetPage(
      name: AppNameRoutes.salesScreen,
      page: () => const SalesScreen(),
    ),
    GetPage(
      name: AppNameRoutes.servicesAdminScreen,
      page: () => const ServicesAdminScreen(),
    ),
    GetPage(
      name: AppNameRoutes.spRequestRigistScreen,
      page: () => const SPRequestRigist(),
    ),
    GetPage(
      name: AppNameRoutes.histogramScreen,
      page: () => const HistogramScreen(),
    ),
    GetPage(
      name: AppNameRoutes.filterSearchScreen,
      page: () => const FilterSearchScreen(),
    ),
    GetPage(
      name: AppNameRoutes.optionScreen,
      page: () => const AdminOptionScreen(),
    ),
    GetPage(
      name: AppNameRoutes.permissionListScreen,
      page: () => const PermissionsListAdmins(),
    ),
    GetPage(
      name: AppNameRoutes.datailsRegestrationScreen,
      page: () => const RegistrationDetailsScreen(),
    ),
    GetPage(
      name: AppNameRoutes.stateBookingScreen,
      page: () => const StateBookingScreen(),
    ),
    GetPage(
      name: AppNameRoutes.categoriesScreen,
      page: () => const CategoriesScreen(),
    ),
    GetPage(
      name: AppNameRoutes.editingServicesScreen,
      page: () => const EditingServicesScreen(),
    ),
    GetPage(
      name: AppNameRoutes.editingProductsScreen,
      page: () => const EditingProductsScreen(),
    ),
    GetPage(
      name: AppNameRoutes.hospitalScreen,
      page: () => const HospitalScreen(),
    ),
    GetPage(
      name: AppNameRoutes.clinicScreen,
      page: () => const ClinicScreen(),
    ),
    GetPage(
      name: AppNameRoutes.labsScreen,
      page: () => const LabsScreen(),
    ),
    GetPage(
      name: AppNameRoutes.pharmaciesScreen,
      page: () => const PharmaciesScreen(),
    ),
    GetPage(
      name: AppNameRoutes.categoriesDoctorScreen,
      page: () => const CategoriesDoctorScreen(),
    ),
  ];
}
