import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterPatientScreen extends GetView<RegisterPatientControllerImp> {
  const RegisterPatientScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: context.theme.scaffoldBackgroundColor,
        appBar: customAuthAppBar(title: '1'.tr),
        body: const RegisterPatientBasicWidget(),
        bottomNavigationBar: const RegisterJoinWidget(),
      ),
    );
  }
}
