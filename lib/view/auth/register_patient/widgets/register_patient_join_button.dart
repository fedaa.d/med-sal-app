import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterPatientJoinButton extends StatelessWidget {
  const RegisterPatientJoinButton({super.key});

  @override
  Widget build(BuildContext context) {
    return CustomButton(
      width: 232.w,
      height: 68.h,
      text: 'Join Now',
      onPressed: () => Get.toNamed(AppNameRoutes.registerProviderScreen),
    );
  }
}
