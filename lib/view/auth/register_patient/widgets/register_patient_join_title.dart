import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterPatientJoinTitle extends StatelessWidget {
  const RegisterPatientJoinTitle({super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      '7'.tr,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: AppColors.mainColor,
        fontSize: AppSize.size20,
      ),
    );
  }
}
