import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterJoinWidget extends StatelessWidget {
  const RegisterJoinWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 180.h,
      color: AppColors.lightgreyColor,
      child: const Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          RegisterPatientJoinTitle(),
          RegisterPatientJoinButton(),
        ],
      ),
    );
  }
}
