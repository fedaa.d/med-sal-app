import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterPatientButtonWidget extends GetView<RegisterPatientControllerImp> {
  const RegisterPatientButtonWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterPatientControllerImp>(
      builder: (_) => CustomButton(
        width: double.infinity,
        height: 53.h,
        text: 'Register',
        onPressed: controller.submitRegister,
        isLoading: controller.statusRequest == StatusRequest.loading,
      ),
    );
  }
}
