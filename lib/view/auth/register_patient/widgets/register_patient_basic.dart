import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterPatientBasicWidget extends StatelessWidget {
  const RegisterPatientBasicWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 12.w),
        child: const Column(
          children: [
            CustomVerticalSizedBox(30),
            RegisterPatientFormsWidget(),
            CustomVerticalSizedBox(50),
            RegisterPatientButtonWidget(),
            CustomVerticalSizedBox(20),
          ],
        ),
      ),
    );
  }
}
