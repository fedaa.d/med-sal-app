import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterPatientFormsWidget extends GetView<RegisterPatientControllerImp> {
  const RegisterPatientFormsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterPatientControllerImp>(
      builder: (_) => Form(
        key: controller.formKey,
        child: Column(
          children: [
            CustomTextFelid(
              validator: (value) => FieldsValidators.emptyValidator(value),
              label: '2'.tr,
              controller: controller.emailController,
              hint: '3'.tr,
              keyboardType: TextInputType.emailAddress,
              icon: Padding(
                padding: const EdgeInsets.all(12),
                child: SvgPicture.asset(
                  AppIcons.mailIcon,
                  colorFilter: ColorFilter.mode(
                    AppColors.borderColor,
                    BlendMode.srcIn,
                  ),
                ),
              ),
            ),
            const CustomVerticalSizedBox(30),
            CustomTextFelid(
              validator: (value) => FieldsValidators.passwordValidator(value),
              label: '4'.tr,
              controller: controller.passwordController,
              hint: '5'.tr,
              keyboardType: TextInputType.visiblePassword,
              icon: IconButton(
                onPressed: () => controller.visibilityPassword(),
                icon: SvgPicture.asset(
                  controller.isPassword
                      ? AppIcons.visibilityLockIcon
                      : AppIcons.visibilityOffIcon,
                ),
              ),
              obscure: controller.isPassword,
            ),
            const CustomVerticalSizedBox(30),
            CustomTextFelid(
              validator: (value) =>
                  FieldsValidators.confirmPasswordValidator(value),
              label: '6'.tr,
              controller: controller.confirmPasswordController,
              hint: '********',
              keyboardType: TextInputType.visiblePassword,
              icon: IconButton(
                onPressed: () => controller.visibilityConfirmPassword(),
                icon: SvgPicture.asset(
                  controller.isPasswordConfirm
                      ? AppIcons.visibilityLockIcon
                      : AppIcons.visibilityOffIcon,
                ),
              ),
              obscure: controller.isPasswordConfirm,
            ),
          ],
        ),
      ),
    );
  }
}
