import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class ResendButton extends StatelessWidget {
  const ResendButton({super.key});

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: () {},
      child: Container(
        padding: const EdgeInsets.only(bottom: 4),
        decoration: BoxDecoration(
            border: Border(
          bottom: BorderSide(
            color: AppColors.mainColor,
            width: 2,
          ),
        )),
        child: Text(
          'Resend it',
          style: TextStyle(
            fontSize: AppSize.size16,
            fontWeight: FontWeight.w400,
            color: AppColors.secondaryColor,
          ),
        ),
      ),
    );
  }
}
