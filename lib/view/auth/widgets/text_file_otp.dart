// import 'package:flutter/material.dart';
// import 'package:mad_sal/core/constants/app_colors.dart';
// import 'package:mad_sal/core/constants/app_sizes.dart';

// class TextFieldOTP extends StatelessWidget {
//   final TextEditingController controller;
//   const TextFieldOTP({
//     required this.first,
//     required this.last,
//     super.key,
//     required this.controller,
//   });
//   final bool first;
//   final bool last;

//   @override
//   Widget build(BuildContext context) {
//     return AspectRatio(
//       aspectRatio: 1.0,
//       child: TextField(
//         controller: controller,
//         textAlignVertical: TextAlignVertical.center,
//         textAlign: TextAlign.center,
//         autofocus: true,
//         onChanged: (value) {
//           if (value.length == 1 && last == false) {
//             FocusScope.of(context).nextFocus();
//           }
//           if (value.isEmpty && first == false) {
//             FocusScope.of(context).previousFocus();
//           }
//         },
//         showCursor: false,
//         readOnly: false,
//         style: const TextStyle(
//           fontSize: AppSize.size18,
//           height: .8,
//           fontWeight: FontWeight.normal,
//         ),
//         keyboardType: TextInputType.number,
//         maxLength: 1,
//         decoration: InputDecoration(
//           counter: const Offstage(),
//           enabledBorder: OutlineInputBorder(
//             borderSide:
//                 BorderSide(width: 1, color: AppColors.lightWhiteColor),
//             borderRadius: BorderRadius.circular(20),
//           ),
//           focusedBorder: OutlineInputBorder(
//             borderSide: BorderSide(width: 1, color: AppColors.mainColor),
//             borderRadius: BorderRadius.circular(20),
//           ),
//         ),
//       ),
//     );
//   }
// }
