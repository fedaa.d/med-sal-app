import 'package:mad_sal/core/constants/app_packages.dart';
import 'package:mad_sal/view/auth/verify_code/widgets/verify_code_basic.dart';

class VerifyCodeScreen extends StatelessWidget {
  const VerifyCodeScreen({super.key});

  @override
  Widget build(BuildContext context) {    
    return SafeArea(
      child: Scaffold(
        backgroundColor: context.theme.scaffoldBackgroundColor,
        body: const VerifyCodeBasicWidget(),
      ),
    );
  }
}
