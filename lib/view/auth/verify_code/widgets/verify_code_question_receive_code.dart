import 'package:mad_sal/core/constants/app_packages.dart';

class VerifyCodeReceiveCodeWidget extends StatelessWidget {
  const VerifyCodeReceiveCodeWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          '41'.tr,
          style: const TextStyle(
              fontSize: AppSize.size16, color: AppColors.blackColor),
        ),
        const ResendButton(),
      ],
    );
  }
}
