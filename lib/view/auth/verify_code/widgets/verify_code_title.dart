import 'package:mad_sal/core/constants/app_packages.dart';

class VerifyCodeTitleWidget extends StatelessWidget {
  const VerifyCodeTitleWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      '39'.tr,
      style: const TextStyle(
        fontSize: AppSize.size20,
        color: AppColors.blackColor,
      ),
    );
  }
}
