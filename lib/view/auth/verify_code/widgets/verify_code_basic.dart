import 'package:mad_sal/core/constants/app_packages.dart';
import 'package:mad_sal/core/shared/custom_otp_form_field.dart';

class VerifyCodeBasicWidget extends GetView<VerificationCodeControllerImp> {
  const VerifyCodeBasicWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 12.w),
      child:  Column(
        children: [
          const CustomVerticalSizedBox(50),
          const VerifyCodeTitleWidget(),
          const CustomVerticalSizedBox(80),
          CustomOtpForm(
            onCompleted: (value) => controller.setCode(value),
          ),
          const CustomVerticalSizedBox(40),
          const VerifyCodeCustomBottomWidget(),
          const CustomVerticalSizedBox(20),
          const VerifyCodeReceiveCodeWidget(),
        ],
      ),
    );
  }
}
