import 'package:mad_sal/core/constants/app_packages.dart';

class VerifyCodeCustomBottomWidget
    extends GetView<VerificationCodeControllerImp> {
  const VerifyCodeCustomBottomWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<VerificationCodeControllerImp>(
      builder: (_) => CustomButton(
        width: double.infinity,
        height: 53.h,
        text: '40'.tr,
        onPressed: () {
          controller.submitVerificationCode();
        },
        isLoading: controller.statusRequest == StatusRequest.loading,
      ),
    );
  }
}
