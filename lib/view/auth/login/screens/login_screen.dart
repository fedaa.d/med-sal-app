import 'package:mad_sal/core/constants/app_packages.dart';

class LoginScreen extends GetView<LoginControllerImp> {
  const LoginScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: context.theme.scaffoldBackgroundColor,
        body: const LoginBasicWidget(),
      ),
    );
  }
}
