import 'package:mad_sal/core/constants/app_packages.dart';

class LoginForgotPasswordWidget extends GetView<LoginControllerImp> {
  const LoginForgotPasswordWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Checkbox(
            side: BorderSide(color: AppColors.mainColor),
            activeColor: AppColors.mainColor,
            value: controller.isChecked,
            onChanged: (value) => controller.checked(value!)),
        Text(
         '43'.tr,
          style:
              TextStyle(color: AppColors.mainColor, fontSize: AppSize.size18),
        ),
        const Spacer(),
        GestureDetector(
          onTap: () {
            Get.offNamed(
              AppNameRoutes.forgotPasswordScreen,
            );
          },
          child: Text(
           '44'.tr,
            style:
                TextStyle(color: AppColors.mainColor, fontSize: AppSize.size18),
          ),
        )
      ],
    );
  }
}
