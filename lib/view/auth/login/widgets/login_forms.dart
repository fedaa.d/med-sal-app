import 'package:mad_sal/core/constants/app_packages.dart';

class LoginFormsWidget extends GetView<LoginControllerImp> {
  const LoginFormsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginControllerImp>(
      builder: (_) => Form(
          key: controller.formKey,
          child: Column(
            children: [
              CustomTextFelid(
                label: '2'.tr,
                controller: controller.emailController,
                hint: '3'.tr,
                keyboardType: TextInputType.emailAddress,
                icon: IconButton(
                  icon: SvgPicture.asset(
                    AppIcons.mailIcon,
                    colorFilter: ColorFilter.mode(
                        AppColors.borderColor, BlendMode.srcIn),
                  ),
                  onPressed: () {},
                ),
                obscure: false,
              ),
              const CustomVerticalSizedBox(30),
              CustomTextFelid(
                label: '4'.tr,
                controller: controller.passwordController,
                hint: '*********',
                keyboardType: TextInputType.visiblePassword,
                icon: IconButton(
                  onPressed: () {
                    controller.visibilityPassword();
                  },
                  icon: SvgPicture.asset(
                    controller.isPassword
                        ? AppIcons.visibilityLockIcon
                        : AppIcons.visibilityOffIcon,
                  ),
                ),
                obscure: controller.isPassword,
              ),
            ],
          )),
    );
  }
}
