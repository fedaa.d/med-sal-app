import 'package:mad_sal/core/constants/app_packages.dart';

class LoginCreateAccountWidget extends StatelessWidget {
  const LoginCreateAccountWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          '45'.tr,
          style: const TextStyle(
            fontSize: AppSize.size16,
            color: AppColors.blackColor,
          ),
        ),
        TextButton(
          onPressed: () {},
          child: Container(
            padding: const EdgeInsets.only(bottom: 4),
            decoration: BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: AppColors.mainColor,
                  width: 2,
                ),
              ),
            ),
            child: Text(
              '46'.tr,
              style: TextStyle(
                fontSize: AppSize.size16,
                color: AppColors.secondaryColor,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
