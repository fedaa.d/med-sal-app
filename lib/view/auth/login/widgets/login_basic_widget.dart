import 'package:mad_sal/core/constants/app_packages.dart';
import 'package:mad_sal/view/auth/login/widgets/login_forms.dart';

class LoginBasicWidget extends StatelessWidget {
  const LoginBasicWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Image.asset(
            AppImages.loginImage,
            width: double.infinity,
            fit: BoxFit.fill,
          ),
          const CustomVerticalSizedBox(40),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 12.w),
            child: const Column(
              children: [
                LoginFormsWidget(),
                CustomVerticalSizedBox(10),
                LoginForgotPasswordWidget(),
                CustomVerticalSizedBox(30),
                LoginBottomWidget(),
                CustomVerticalSizedBox(20),
                LoginCreateAccountWidget(),
                CustomVerticalSizedBox(20),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
