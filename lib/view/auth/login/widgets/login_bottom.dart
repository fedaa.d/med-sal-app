import 'package:mad_sal/core/constants/app_packages.dart';

class LoginBottomWidget extends GetView<LoginControllerImp> {
  const LoginBottomWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return CustomButton(
      width: double.infinity,
      height: 53.h,
      text: '42'.tr,
      onPressed: () => controller.submitLogin(),
    );
  }
}
