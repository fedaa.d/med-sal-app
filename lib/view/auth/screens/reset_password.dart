import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/reset_password_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/core/shared/custom_button.dart';
import 'package:mad_sal/core/shared/custom_text_field.dart';
import 'package:mad_sal/view/auth/screens/forgot_password.dart';

class ResetPassword extends GetView<ResetPasswordControllerImp> {
  const ResetPassword({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(ResetPasswordControllerImp());
    return Form(
      key: formKey,
      child: Scaffold(
          backgroundColor: AppColors.whiteColor,
          appBar: AppBar(
            toolbarHeight: 121,
            elevation: 0,
            backgroundColor: AppColors.lightgreyColor,
            centerTitle: true,
            title: const Text(
              'Reset Password',
              style: TextStyle(
                  fontWeight: FontWeight.w500,
                  fontSize: AppSize.size30,
                  color: AppColors.blackColor),
            ),
          ),
          body: GetBuilder<ResetPasswordControllerImp>(
            builder: (_) => SafeArea(
                child: Column(children: [
              const SizedBox(
                height: 100,
              ),
              CustomTextFelid(
                label: 'Password',
                controller: controller.passwordController,
                hint: 'At least 8 characters',
                keyboardType: TextInputType.visiblePassword,
                icon: IconButton(
                  onPressed: () {
                    controller.visibilityPassword();
                  },
                  icon: SvgPicture.asset(
                    controller.isPassword
                        ? AppIcons.visibilityLockIcon
                        : AppIcons.visibilityOffIcon,
                  ),
                ),
                obscure: controller.isPassword,
              ),
              const SizedBox(
                height: 50,
              ),
              CustomTextFelid(
                label: 'Confirm Password',
                controller: controller.confirmPasswordController,
                hint: 'Confirm Password',
                keyboardType: TextInputType.visiblePassword,
                icon: IconButton(
                  onPressed: () {
                    controller.visibilityConfirmPassword();
                  },
                  icon: SvgPicture.asset(
                    controller.isConfirmPassword
                        ? AppIcons.visibilityLockIcon
                        : AppIcons.visibilityOffIcon,
                  ),
                ),
                obscure: controller.isConfirmPassword,
              ),
              const SizedBox(
                height: 50,
              ),
              Padding(
                padding: const EdgeInsets.all(18.0),
                child: CustomButton(
                  width: double.infinity,
                  height: 53,
                  text: 'Change Password',
                  onPressed: () {
                    Get.offNamed(AppNameRoutes.settingScreen);
                    if (controller.passwordController.text.isNotEmpty) {
                      ScaffoldMessenger.of(context).showSnackBar(
                          ResetPasswordControllerImp.passwordChange);
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(
                          ResetPasswordControllerImp.somthingWrong);
                    }
                  },
                ),
              ),
            ])),
          )),
    );
  }
}
