import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';

import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/core/shared/custom_button.dart';
import 'package:mad_sal/core/shared/custom_text_field.dart';

var emailController = TextEditingController();
var formKey = GlobalKey<FormState>();

class ForgotPassword extends StatelessWidget {
  const ForgotPassword({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.whiteColor,
        appBar: AppBar(
          toolbarHeight: 121,
          elevation: 0,
          leading: const Icon(
            Icons.arrow_circle_left_outlined,
            color: AppColors.blackColor,
          ),
          backgroundColor: AppColors.lightgreyColor,
          centerTitle: true,
          title: const Text(
            'Forgot Password',
            style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: AppSize.size30,
                color: AppColors.blackColor),
          ),
        ),
        body: SafeArea(
            child: Column(children: [
          const SizedBox(
            height: 25,
          ),
          const Text(
            'Pleas enter email',
            style: TextStyle(
                fontSize: AppSize.size20, color: AppColors.blackColor),
          ),
          const SizedBox(
            height: 30,
          ),
          Form(
              key: formKey,
              child: CustomTextFelid(
                label: 'Email',
                controller: emailController,
                hint: 'yourname@mail.com',
                keyboardType: TextInputType.emailAddress,
                icon: IconButton(
                  icon: SvgPicture.asset(
                    AppIcons.mailIcon,
                    colorFilter: ColorFilter.mode(
                        AppColors.borderColor, BlendMode.srcIn),
                  ),
                  onPressed: () {},
                ),
                obscure: false,
              )),
          const SizedBox(
            height: 15,
          ),
          Padding(
            padding: const EdgeInsets.all(18.0),
            child: CustomButton(
              width: double.infinity,
              height: 53,
              text: 'Send verification code',
              onPressed: () {
                Get.offNamed(AppNameRoutes.resetPasswordScreen);
              },
            ),
          ),
        ])));
  }
}
