import 'package:mad_sal/core/constants/app_packages.dart';

class CheckEmailButton extends StatelessWidget {
  const CheckEmailButton({super.key});

  @override
  Widget build(BuildContext context) {
    return CustomButton(
      width: double.infinity,
      height: 53.h,
      text: 'Enter Code',
      onPressed: () => Get.offNamed(AppNameRoutes.verifyCodeScreen),
    );
  }
}
