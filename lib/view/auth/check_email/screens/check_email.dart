import 'package:mad_sal/core/constants/app_packages.dart';

class CheckEmailScreen extends GetView<CheckEmailController> {
  const CheckEmailScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.whiteColor,
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 12.w),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const Image(
                image: AssetImage(
                  AppImages.checkEmailImage,
                ),
                fit: BoxFit.fitWidth,
              ),
              const CustomVerticalSizedBox(30),
              Text(
                'Check your email',
                style: TextStyle(
                  fontSize: AppSize.size35,
                  fontWeight: FontWeight.w400,
                  color: AppColors.mainColor,
                ),
              ),
              const CustomVerticalSizedBox(90),
              const Text(
                'We just sent a verification code over to',
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: AppSize.size25,
                  fontWeight: FontWeight.w400,
                  color: AppColors.blackColor,
                ),
              ),
              Text(
                controller.splitEmail,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  fontSize: AppSize.size25,
                  fontWeight: FontWeight.w400,
                  color: AppColors.blackColor,
                ),
              ),
              const Spacer(),
              const CheckEmailButton(),
              const CustomVerticalSizedBox(20),
            ],
          ),
        ),
      ),
    );
  }
}
