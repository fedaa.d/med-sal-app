import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterProviderScreen extends StatelessWidget {
  const RegisterProviderScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: customAuthAppBar(title: '33'.tr),
        backgroundColor: context.theme.scaffoldBackgroundColor,
        body: const RegisterProviderBasicWidget(),
      ),
    );
  }
}
