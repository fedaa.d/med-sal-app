import 'package:mad_sal/core/constants/app_packages.dart';
import 'package:mad_sal/view/auth/register_provider/widgets/register_provider_bank_sheet_forms_style.dart';

class RegisterProviderBankSheetFormsWidget
    extends GetView<RegisterProviderControllerImp> {
  const RegisterProviderBankSheetFormsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        RegisterProviderBankSheetFormsStyleWidget(
          validator: (value) => FieldsValidators.emptyValidator(value),
          controller: controller.bankNameController,
          hintText: '30'.tr,
        ),
        const CustomVerticalSizedBox(20),
        RegisterProviderBankSheetFormsStyleWidget(
          validator: (value) => FieldsValidators.emptyValidator(value),
          controller: controller.swiftCodeController,
          hintText: '31'.tr,
        ),
        const CustomVerticalSizedBox(20),
        RegisterProviderBankSheetFormsStyleWidget(
          validator: (value) => FieldsValidators.emptyValidator(value),
          controller: controller.ilbanController,
          hintText: 'Ilban'.tr,
        ),
      ],
    );
  }
}
