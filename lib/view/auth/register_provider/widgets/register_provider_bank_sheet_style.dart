import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterProviderBankSheetStyleWidget
    extends GetView<RegisterProviderControllerImp> {
  const RegisterProviderBankSheetStyleWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 280.h,
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: 20.w,
          vertical: 15.h,
        ),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(bottom: 15.h),
              height: 4,
              width: 30,
              decoration: BoxDecoration(
                color: AppColors.darkGryColor,
                borderRadius: BorderRadius.circular(20),
              ),
            ),
            const RegisterProviderBankSheetFormsWidget(),
          ],
        ),
      ),
    );
  }
}
