import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterProviderButtonWidget
    extends GetView<RegisterProviderControllerImp> {
  const RegisterProviderButtonWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterProviderControllerImp>(
      builder: (_) => CustomButton(
        width: double.infinity,
        height: 53.h,
        text: 'Sign up',
        onPressed: () => controller.submitRegister(),
        isLoading: controller.statusRequest == StatusRequest.loading,
      ),
    );
  }
}
