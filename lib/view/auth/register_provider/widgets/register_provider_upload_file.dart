import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterProviderUploadFileWidget
    extends GetView<RegisterProviderControllerImp> {
  const RegisterProviderUploadFileWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterProviderControllerImp>(
      builder: (_) => Container(
        height: 65,
        decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
            side: const BorderSide(
              width: 1.0,
              color: AppColors.gryColor,
            ),
          ),
        ),
        child: MaterialButton(
          onPressed: controller.pickFile,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                '34'.tr,
                style: const TextStyle(
                  color: AppColors.gryColor,
                  fontSize: AppSize.size14,
                  fontWeight: FontWeight.w400,
                ),
              ),
              SvgPicture.asset(
                AppIcons.uploadIcon,
                colorFilter:
                    ColorFilter.mode(AppColors.borderColor, BlendMode.srcIn),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
