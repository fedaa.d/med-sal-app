import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterProviderFormsWidget
    extends GetView<RegisterProviderControllerImp> {
  const RegisterProviderFormsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegisterProviderControllerImp>(
      builder: (_) => Form(
        key: controller.formKey,
        child: Column(
          children: [
            CustomTextFelid(
              validator: (value) => FieldsValidators.emptyValidator(value),
              label: '2'.tr,
              controller: controller.emailController,
              hint: '3'.tr,
              keyboardType: TextInputType.emailAddress,
              icon: IconButton(
                icon: SvgPicture.asset(
                  AppIcons.mailIcon,
                  colorFilter: ColorFilter.mode(
                    AppColors.borderColor,
                    BlendMode.srcIn,
                  ),
                ),
                onPressed: () {},
              ),
              obscure: false,
            ),
            const CustomVerticalSizedBox(30),
            CustomTextFelid(
              validator: (value) => FieldsValidators.passwordValidator(value),
              label: '4'.tr,
              controller: controller.passwordController,
              hint: '5'.tr,
              keyboardType: TextInputType.visiblePassword,
              icon: IconButton(
                onPressed: () {
                  controller.visibilityPassword();
                },
                icon: SvgPicture.asset(
                  controller.isPassword
                      ? AppIcons.visibilityLockIcon
                      : AppIcons.visibilityOffIcon,
                ),
              ),
              obscure: controller.isPassword,
            ),
            const CustomVerticalSizedBox(30),
            CustomTextFelid(
              validator: (value) =>
                  FieldsValidators.confirmPasswordValidator(value),
              label: '6'.tr,
              controller: controller.confirmPasswordController,
              hint: '********',
              keyboardType: TextInputType.visiblePassword,
              icon: IconButton(
                onPressed: () {
                  controller.visibilityConfirmPassword();
                },
                icon: SvgPicture.asset(
                  controller.isConfirmPassword
                      ? AppIcons.visibilityLockIcon
                      : AppIcons.visibilityOffIcon,
                ),
              ),
              obscure: controller.isConfirmPassword,
            ),
            const CustomVerticalSizedBox(30),
            CustomTextFelid(
              validator: (value) =>
                  FieldsValidators.phoneNumberValidator(value),
              label: '24'.tr,
              controller: controller.numberController,
              hint: '+963 999999999',
              keyboardType: TextInputType.number,
              icon: IconButton(
                icon: SvgPicture.asset(
                  AppIcons.callIcon,
                  colorFilter:
                      ColorFilter.mode(AppColors.borderColor, BlendMode.srcIn),
                ),
                onPressed: () {},
              ),
              obscure: false,
            ),
            const CustomVerticalSizedBox(30),
            CustomTextFelid(
              validator: (value) => FieldsValidators.emptyValidator(value),
              label: '25'.tr,
              controller: controller.bussinessNameController,
              hint: '3'.tr,
              keyboardType: TextInputType.emailAddress,
              icon: IconButton(
                icon: SvgPicture.asset(
                  AppIcons.businesIcon,
                  colorFilter:
                      ColorFilter.mode(AppColors.borderColor, BlendMode.srcIn),
                ),
                onPressed: () {},
              ),
              obscure: false,
            ),
            const CustomVerticalSizedBox(30),
            CustomTextFelid(
              validator: (value) => FieldsValidators.emptyValidator(value),
              label: '26'.tr,
              controller: controller.serviceController,
              hint: '27'.tr,
              keyboardType: TextInputType.text,
              icon: IconButton(
                icon: SvgPicture.asset(
                  AppIcons.editIcon,
                  colorFilter:
                      ColorFilter.mode(AppColors.borderColor, BlendMode.srcIn),
                ),
                onPressed: () {},
              ),
              obscure: false,
            ),
          ],
        ),
      ),
    );
  }
}
