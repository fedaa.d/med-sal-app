import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterProviderBankSheetFormsStyleWidget extends StatelessWidget {
  final TextEditingController controller;
  final String hintText;
  final Function(String value) validator;
  const RegisterProviderBankSheetFormsStyleWidget({
    super.key,
    required this.validator,
    required this.controller,
    required this.hintText,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      cursorColor: AppColors.darkGryColor,
      validator: (value) => validator(value!),
      controller: controller,
      keyboardType: TextInputType.text,
      decoration: InputDecoration(
        hintText: hintText,
        hintStyle: TextStyle(
          fontSize: AppSize.size16,
          color: AppColors.lightWhiteColor,
          fontWeight: FontWeight.w400,
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.lightWhiteColor),
          borderRadius: BorderRadius.circular(15),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.lightBlackColor),
          borderRadius: BorderRadius.circular(15),
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.lightWhiteColor),
          borderRadius: BorderRadius.circular(15),
        ),
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.lightWhiteColor),
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }
}
