import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterProviderTitleFileWidget extends StatelessWidget {
  const RegisterProviderTitleFileWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      '32'.tr,
      style: const TextStyle(
        fontSize: AppSize.size16,
        color: AppColors.blackColor,
      ),
    );
  }
}
