import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterProviderBasicWidget extends StatelessWidget {
  const RegisterProviderBasicWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 12.w),
        child: const Column(
          children: [
            CustomVerticalSizedBox(30),
            RegisterProviderFormsWidget(),
            CustomVerticalSizedBox(30),
            RegisterProviderBankSheetWidget(),
            CustomVerticalSizedBox(30),
            RegisterProviderTitleFileWidget(),
            CustomVerticalSizedBox(10),
            RegisterProviderUploadFileWidget(),
            CustomVerticalSizedBox(30),
            RegisterProviderButtonWidget(),
            CustomVerticalSizedBox(20),
          ],
        ),
      ),
    );
  }
}
