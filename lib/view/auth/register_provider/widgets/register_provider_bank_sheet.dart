import 'package:mad_sal/core/constants/app_packages.dart';

class RegisterProviderBankSheetWidget
    extends GetView<RegisterProviderControllerImp> {
  const RegisterProviderBankSheetWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return InputDecorator(
      decoration: InputDecoration(
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.lightWhiteColor),
          borderRadius: BorderRadius.circular(15),
        ),
        labelText: '28'.tr,
        labelStyle: const TextStyle(
          fontSize: AppSize.size18,
          fontWeight: FontWeight.w400,
          color: AppColors.blackColor,
        ),
        suffixIcon: IconButton(
          icon: SvgPicture.asset(
            AppIcons.arrowDropDownIcon,
            colorFilter:
                ColorFilter.mode(AppColors.borderColor, BlendMode.srcIn),
          ),
          onPressed: () => Get.bottomSheet(
            BottomSheet(
              enableDrag: false,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.vertical(
                  top: Radius.circular(30),
                ),
              ),
              clipBehavior: Clip.antiAliasWithSaveLayer,
              onClosing: () {},
              builder: (context) {
                //body of bottom sheet
                return const RegisterProviderBankSheetStyleWidget();
              },
            ),
          ),
        ),
      ),
      child: Text(
        '29'.tr,
        style: TextStyle(
          fontSize: AppSize.size16,
          color: AppColors.lightWhiteColor,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }
}
