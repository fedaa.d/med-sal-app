import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/setting/model/settings_model.dart';

class ListTileSetting extends StatelessWidget {
  const ListTileSetting({required this.onTap, super.key});
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        itemBuilder: (context, index) => Container(
              alignment: Alignment.center,
              height: 80,
              decoration: BoxDecoration(
                  color: AppColors.lightpinkColor,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: AppColors.lightWhiteColor)),
              child: ListTile(
                onTap: onTap,
                leading: Text(
                  settingsModelList[index].title,
                  style: const TextStyle(
                      fontSize: AppSize.size20, color: AppColors.blackColor),
                ),
                trailing: settingsModelList[index].icon,
              ),
            ),
        separatorBuilder: ((context, index) => const SizedBox(
              height: 15,
            )),
        itemCount: 4);
  }
}
