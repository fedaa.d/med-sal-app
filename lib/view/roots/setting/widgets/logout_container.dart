import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class LogOutContainer extends StatelessWidget {
  const LogOutContainer({super.key});

  @override
  Widget build(BuildContext context) {
    double heightSize = MediaQuery.of(context).size.height;

    return Column(
      children: [
        const SizedBox(
          height: 15,
        ),
        Container(
          width: double.infinity,
          height: heightSize * 0.07,
          decoration: ShapeDecoration(
            color: AppColors.mainColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          child: MaterialButton(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SvgPicture.asset(AppIcons.loghOutIcon),
                const SizedBox(
                  width: 30,
                ),
                const Text(
                  'Logout',
                  style: TextStyle(
                      fontSize: AppSize.size22, color: AppColors.whiteColor),
                ),
              ],
            ),
            onPressed: () {},
          ),
        ),
      ],
    );
  }
}
