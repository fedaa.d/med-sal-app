import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';

class SettingsModel {
  final SvgPicture icon;
  final String title;
  SettingsModel({
    required this.icon,
    required this.title,
  });
}

List<SettingsModel> settingsModelList = [
  SettingsModel(
      icon: SvgPicture.asset(AppIcons.editIcon,
          colorFilter: ColorFilter.mode(AppColors.darkColor, BlendMode.srcIn)),
      title: 'Edit My Profile'),
  SettingsModel(
      icon: SvgPicture.asset(AppIcons.syncLockIcon,
          colorFilter: ColorFilter.mode(AppColors.darkColor, BlendMode.srcIn)),
      title: 'Change Password'),
  SettingsModel(
      icon: SvgPicture.asset(AppIcons.notlistedLocationIcon,
          colorFilter: ColorFilter.mode(AppColors.darkColor, BlendMode.srcIn)),
      title: 'Help'),
  SettingsModel(
      icon: SvgPicture.asset(AppIcons.clearNightIcon,
          colorFilter: ColorFilter.mode(AppColors.darkColor, BlendMode.srcIn)),
      title: 'Dark mode'),
];
