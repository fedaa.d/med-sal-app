import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/profile/screens/profile.dart';
import 'package:mad_sal/view/roots/setting/widgets/listtile_setting.dart';
import 'package:mad_sal/view/roots/setting/widgets/logout_container.dart';

class SettingScreen extends StatelessWidget {
  const SettingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: const Icon(
          Icons.settings_outlined,
          color: AppColors.blackColor,
        ),
        toolbarHeight: 121,
        elevation: 0,
        backgroundColor: AppColors.lightgreyColor,
        centerTitle: true,
        title: const Text(
          'Setting',
          style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: AppSize.size30,
              color: AppColors.blackColor),
        ),
      ),
      body: SafeArea(
          child: SingleChildScrollView(
            child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 10,
              ),
              Text(
                'Account',
                style: TextStyle(
                    color: AppColors.mainColor,
                    fontSize: AppSize.size24,
                    fontWeight: FontWeight.w600),
              ),
              const SizedBox(
                height: 15,
              ),
              ListTileSetting(
                  onTap: () => Get.to(
                        ProfileScreen(),
                      )),
              const LogOutContainer()
            ],
                  ),
                ),
          )),
    );
  }
}
