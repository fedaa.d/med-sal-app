import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/sales/histogram/model/histogram_data_model.dart';

List<BarChartGroupData> chartGroups() {
  List<BarChartGroupData> barChartGroupList =
      List<BarChartGroupData>.empty(growable: true);
  for (int i = 0; i < histogramDataModelList.length; i++) {
    barChartGroupList.add(BarChartGroupData(x: i, barRods: [
      BarChartRodData(
          toY: double.parse(histogramDataModelList[i].value!),
          color: AppColors.secondaryColor,
          width: 35,
          borderRadius: BorderRadius.circular(2))
    ]));
  }
  return barChartGroupList;
}
