import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class HistorgamGainText extends StatelessWidget {
  const HistorgamGainText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'Gain Chart',
          style: TextStyle(
              color: AppColors.mainColor,
              fontSize: AppSize.size25,
              fontWeight: FontWeight.w700),
        ),
        const SizedBox(
          height: 80,
        ),
      ],
    );
  }
}
