import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/sales/histogram/model/histogram_bottom_titles.dart';
import 'package:mad_sal/view/roots/categories/sales/histogram/widgets/histogram_barchart.dart';

class HistorgamBarChartData extends StatelessWidget {
  const HistorgamBarChartData({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 1,
        itemBuilder: (context, index) {
          return SizedBox(
            width: MediaQuery.of(context).size.width * 2.2,
            child: Padding(
              padding: const EdgeInsets.only(top: 22.0),
              child: BarChart(
                BarChartData(
                    maxY: 200,
                    minY: 0,
                    titlesData: FlTitlesData(
                      topTitles: const AxisTitles(
                          sideTitles: SideTitles(showTitles: false)),
                      bottomTitles: AxisTitles(sideTitles: bottomTitles),
                      rightTitles: const AxisTitles(
                          sideTitles: SideTitles(showTitles: false)),
                    ),
                    borderData: FlBorderData(
                      show: false,
                    ),
                    gridData: const FlGridData(
                      show: false,
                      drawHorizontalLine: false,
                      drawVerticalLine: false,
                    ),
                    barGroups: chartGroups()),
              ),
            ),
          );
        },
      ),
    );
  }
}
