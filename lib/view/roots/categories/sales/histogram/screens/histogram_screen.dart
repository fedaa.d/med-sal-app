import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/sales/histogram/widgets/histogram_barachrt_data.dart';
import 'package:mad_sal/view/roots/categories/sales/histogram/widgets/histogram_text.dart';
import 'package:mad_sal/view/roots/categories/widgets/custom_appbar.dart';

class HistogramScreen extends StatelessWidget {
  const HistogramScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: customAppBar(title: 'Histogram'),
      body: const SafeArea(
          child: Padding(
        padding: EdgeInsets.all(18.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [HistorgamGainText(), HistorgamBarChartData()],
        ),
      )),
    );
  }
}
