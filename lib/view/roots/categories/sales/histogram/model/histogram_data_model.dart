class HistogramDataModel {
  String? value;

  HistogramDataModel({this.value});

  HistogramDataModel.fromJson(Map<String, dynamic> json) {
    value = json['value'];
  }
}

List<HistogramDataModel> histogramDataModelList = [
  HistogramDataModel(value: "100"),
  HistogramDataModel(value: "180"),
  HistogramDataModel(value: "130"),
  HistogramDataModel(value: "70"),
  HistogramDataModel(value: "200"),
  HistogramDataModel(value: "140"),
  HistogramDataModel(value: "170"),
  HistogramDataModel(value: "60"),
  HistogramDataModel(value: "30"),
  HistogramDataModel(value: "150"),
  HistogramDataModel(value: "179"),
  HistogramDataModel(value: "110"),
];
