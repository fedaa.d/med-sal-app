import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

SideTitles get bottomTitles => SideTitles(
      showTitles: true,
      getTitlesWidget: (double value, TitleMeta meta) {
        String text = '';
        switch (value.toInt()) {
          case 0:
            text = 'january';
            break;
          case 1:
            text = 'February';
            break;
          case 2:
            text = 'March';
            break;
          case 3:
            text = 'April';
            break;
          case 4:
            text = 'May';
            break;
          case 5:
            text = 'June';
            break;
          case 6:
            text = 'July';
            break;
          case 7:
            text = 'August';
            break;
          case 8:
            text = 'September';
            break;
          case 9:
            text = 'October';
            break;
          case 10:
            text = 'November';
            break;
          case 11:
            text = 'December';
            break;
        }

        return Text(
          text,
          style: const TextStyle(
              fontSize: AppSize.size12, color: AppColors.blackColor),
        );
      },
    );
