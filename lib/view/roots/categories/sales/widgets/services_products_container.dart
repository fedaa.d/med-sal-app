import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/sales_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/sales/model/services_products_model.dart';

class ServicesOrProductsContainer extends GetView<SalesControllerImp> {
  const ServicesOrProductsContainer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(SalesControllerImp());
    return Column(
      children: [
        const SizedBox(
          height: 15,
        ),
        Container(
          height: 40,
          child: ListView.separated(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) =>
                  GetBuilder<SalesControllerImp>(
                    builder: (_) => GestureDetector(
                      onTap: () => controller.onSelected(index),
                      child: Container(
                        height: 39,
                        width: 105,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: controller.selectedIndex == index
                                ? AppColors.secondaryColor
                                : AppColors.lightWhiteColor),
                        child: Text(
                          servicesOrProductsList[index].text,
                          style: TextStyle(
                            color: controller.selectedIndex == index
                                ? AppColors.whiteColor
                                : AppColors.blackColor,
                            fontSize: AppSize.size14,
                          ),
                        ),
                      ),
                    ),
                  ),
              separatorBuilder: (BuildContext context, int index) =>
                  const SizedBox(
                    width: 30,
                  ),
              itemCount: servicesOrProductsList.length),
        ),
      ],
    );
  }
}
