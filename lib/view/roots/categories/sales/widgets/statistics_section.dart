import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/sales/functions/pie_phart_data.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/lines_statistics.dart';

class StatisticsSection extends StatelessWidget {
  const StatisticsSection({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Container(
        height: 517,
        width: double.infinity,
        decoration: BoxDecoration(
            color: AppColors.lightgreyColor,
            border: Border.all(color: AppColors.darkBorderColor),
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(45), topRight: Radius.circular(45))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Expanded(
              child: Padding(
                padding: const EdgeInsets.only(right: 210.0),
                child: PieChart(pieChartData(centerRadius: 35, radius: 15)),
              ),
            ),
            const LinesStatistics(),
            Expanded(
                child: Padding(
              padding: const EdgeInsets.only(left: 230.0),
              child: PieChart(pieChartData(centerRadius: 20, radius: 10)),
            ))
          ],
        ),
      ),
    );
  }
}
