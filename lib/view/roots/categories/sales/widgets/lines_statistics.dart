import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/sales/functions/bar_chart_group.dart';

class LinesStatistics extends StatelessWidget {
  const LinesStatistics({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: BarChart(
      BarChartData(
        barGroups: [
          barChartGroup(x: 1, topY: 7, color: AppColors.secondaryColor),
          barChartGroup(x: 2, topY: 5, color: AppColors.mainColor),
          barChartGroup(x: 3, topY: 8, color: AppColors.mainColor),
          barChartGroup(x: 4, topY: 4, color: AppColors.secondaryColor),
          barChartGroup(x: 5, topY: 7, color: AppColors.mainColor),
          barChartGroup(x: 6, topY: 9, color: AppColors.secondaryColor),
          barChartGroup(x: 7, topY: 5, color: AppColors.mainColor),
          barChartGroup(x: 8, topY: 8, color: AppColors.mainColor),
          barChartGroup(x: 9, topY: 10, color: AppColors.secondaryColor),
        ],
        borderData: FlBorderData(
            show: false,
            border: const Border(
                top: BorderSide.none,
                right: BorderSide.none,
                left: BorderSide(
                  width: 1,
                ),
                bottom: BorderSide(width: 1))),
        titlesData: const FlTitlesData(show: false),
        gridData: const FlGridData(
          show: false,
          drawHorizontalLine: false,
          drawVerticalLine: false,
        ),
      ),
    ));
  }
}
