import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class DateTextBottomSheet extends StatelessWidget {
  const DateTextBottomSheet({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Text(
      'DD /MM /YY',
      style: TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size12,
          fontWeight: FontWeight.w400),
    );
  }
}
