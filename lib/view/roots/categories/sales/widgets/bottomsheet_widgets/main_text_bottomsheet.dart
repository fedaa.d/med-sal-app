import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class MainTextBottomSheet extends StatelessWidget {
  const MainTextBottomSheet({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        SizedBox(
          height: 7,
        ),
        Text(
          'Service Provider Name',
          style:
              TextStyle(fontSize: AppSize.size16, fontWeight: FontWeight.w500),
        ),
        SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
