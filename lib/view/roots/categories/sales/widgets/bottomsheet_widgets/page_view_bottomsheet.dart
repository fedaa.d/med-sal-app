import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/sales_controller.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/table_widgets/table_container.dart';

class PageViewBottomSheet extends GetView<SalesControllerImp> {
  const PageViewBottomSheet({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(SalesControllerImp());
    return GetBuilder<SalesControllerImp>(
      builder: (_) => SizedBox(
        height: 400,
        child: PageView(
          controller: PageController(initialPage: 0),
          onPageChanged: (int val) => controller.change(val),
          children: [
            ...List.generate(
              9,
              (index) => const TableContainer(),
            )
          ],
        ),
      ),
    );
  }
}
