import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/sales/functions/numbers_indicator.dart';
import 'package:mad_sal/view/roots/categories/sales/model/indicator_model.dart';

class Indicator extends StatelessWidget {
  const Indicator({
    Key? key,
    required this.index,
  }) : super(key: key);
  final int? index;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 215,
      height: 23,
      decoration: BoxDecoration(border: Border.all()),
      child: Padding(
        padding: const EdgeInsets.only(left: 4.0, right: 4.0),
        child: ListView.separated(
            scrollDirection: Axis.horizontal,
            shrinkWrap: true,
            itemBuilder: (BuildContext context, int index) => numbersIndicator(
                indicatorModelList[index].numer,
                this.index == index
                    ? AppColors.mainColor
                    : AppColors.blackColor),
            separatorBuilder: (BuildContext context, int index) =>
                const SizedBox(
                  width: 5,
                ),
            itemCount: 9),
      ),
    );
  }
}
