import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class TableTotalText extends StatelessWidget {
  const TableTotalText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.bottomRight,
          child: Text(
            r'Total=48$',
            style: TextStyle(
                color: AppColors.secondaryColor,
                fontSize: AppSize.size12,
                fontWeight: FontWeight.w500),
          ),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
