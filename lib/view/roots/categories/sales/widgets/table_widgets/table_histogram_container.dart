import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class TableHistogramContainer extends StatelessWidget {
  const TableHistogramContainer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 53,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: AppColors.secondaryColor,
      ),
      child: MaterialButton(
        onPressed: () => Get.toNamed(AppNameRoutes.histogramScreen),
        child: const Text(
          'Histogram',
          style: TextStyle(
              color: AppColors.whiteColor,
              fontSize: AppSize.size16,
              fontWeight: FontWeight.w600),
        ),
      ),
    );
  }
}
