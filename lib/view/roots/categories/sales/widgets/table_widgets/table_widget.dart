import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/table_widgets/info_table_cell.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/table_widgets/table_cell.dart';

class TableWidget extends StatelessWidget {
  const TableWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Table(
      children: [
        const TableRow(children: [
          TableCellWidget(
            title: 'Id',
          ),
          TableCellWidget(
            title: 'Service Name',
          ),
          TableCellWidget(
            title: 'Date',
          ),
          TableCellWidget(
            title: 'Gain',
          ),
        ]),
        ...List.generate(
          4,
          (index) => const TableRow(children: [
            Padding(
              padding: EdgeInsets.only(left: 5.0),
              child: InfoTableCell(
                text: '1457',
              ),
            ),
            InfoTableCell(text: 'Cleaning Teeth'),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: InfoTableCell(
                text: '15 / 6 \n 14-14:30 PM ',
              ),
            ),
            InfoTableCell(
              text: r'12$',
            ),
          ]),
        )
      ],
    );
  }
}
