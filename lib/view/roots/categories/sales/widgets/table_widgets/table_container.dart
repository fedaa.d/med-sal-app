import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/table_widgets/table_histogram_container.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/table_widgets/table_total_text.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/table_widgets/table_widget.dart';

class TableContainer extends StatelessWidget {
  const TableContainer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [TableWidget(), TableTotalText(), TableHistogramContainer()],
    );
  }
}
