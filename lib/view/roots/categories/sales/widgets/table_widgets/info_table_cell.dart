import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class InfoTableCell extends StatelessWidget {
  const InfoTableCell({
    Key? key,
    required this.text,
  }) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 12, bottom: 12),
          child: TableCell(
            child: Text(
              text,
              style: const TextStyle(
                  color: AppColors.blackColor,
                  fontSize: AppSize.size12,
                  fontWeight: FontWeight.w400),
            ),
          ),
        ),
      ],
    );
  }
}
