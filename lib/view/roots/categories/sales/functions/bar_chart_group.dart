import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

BarChartGroupData barChartGroup(
    {required int x, required double topY, required Color color}) {
  return BarChartGroupData(x: x, barRods: [
    BarChartRodData(
        borderRadius: BorderRadius.circular(2),
        toY: topY,
        fromY: 0,
        width: 15,
        color: color)
  ]);
}
