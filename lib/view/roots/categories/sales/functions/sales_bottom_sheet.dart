import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/sales_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/bottomsheet_widgets/date_text_bottomsheet.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/bottomsheet_widgets/main_text_bottomsheet.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/bottomsheet_widgets/page_view_bottomsheet.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/services_products_container.dart';

Future<dynamic> salesBottomSheet() {
  final controller = Get.put(SalesControllerImp());
  return Get.bottomSheet(
      clipBehavior: Clip.hardEdge,
      elevation: 10,
      SingleChildScrollView(
        child: Container(
            decoration: BoxDecoration(
                color: AppColors.lightgreyColor,
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(34),
                    topRight: Radius.circular(34))),
            child: Padding(
                padding: const EdgeInsets.all(18.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const MainTextBottomSheet(),
                      const DateTextBottomSheet(),
                      const ServicesOrProductsContainer(),
                      const PageViewBottomSheet(),
                      GetBuilder<SalesControllerImp>(
                        builder: (_) => Indicator(
                          index: controller.currentIndex,
                        ),
                      ),
                    ]))),
      ));
}
