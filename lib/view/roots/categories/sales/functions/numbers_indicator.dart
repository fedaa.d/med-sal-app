import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

Container numbersIndicator(int number, Color borderColor) {
  return Container(
    margin: const EdgeInsets.only(top: 3, bottom: 3, left: 1, right: 1),
    width: 16,
    decoration:
        BoxDecoration(border: Border.all(width: .5, color: borderColor)),
    child: Center(
      child: Text(
        number.toString(),
        style: const TextStyle(
            fontSize: AppSize.size10,
            color: AppColors.blackColor,
            fontWeight: FontWeight.w300),
      ),
    ),
  );
}
