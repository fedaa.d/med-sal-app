import 'package:fl_chart/fl_chart.dart';
import 'package:mad_sal/view/roots/categories/sales/model/pie_data_model.dart';

PieChartData pieChartData(
    {required double centerRadius, required double radius}) {
  return PieChartData(
    startDegreeOffset: 20,
    sections: getSections(radiusCircle: radius),
    borderData: FlBorderData(show: false),
    sectionsSpace: 0,
    centerSpaceRadius: centerRadius,
  );
}
