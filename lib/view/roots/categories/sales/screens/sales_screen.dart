import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/sales/functions/sales_bottom_sheet.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/sales_search.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/statistics_section.dart';
import 'package:mad_sal/view/roots/categories/widgets/custom_appbar.dart';

class SalesScreen extends StatelessWidget {
  const SalesScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: customAppBar(title: 'Sales Report'),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: [
            GestureDetector(
              onTap: () => salesBottomSheet(),
              child: const SalesSearchTextFiled(
                hintText: 'Enter Service Provider',
              ),
            ),
            GestureDetector(
              onTap: () => salesBottomSheet(),
              child: const SalesSearchTextFiled(
                hintText: 'Enter Date',
              ),
            ),
            const StatisticsSection()
          ],
        ),
      )),
    );
  }
}
