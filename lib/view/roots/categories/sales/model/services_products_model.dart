class ServicesOrProductsModel {
  final String text;
  ServicesOrProductsModel({
    required this.text,
  });
}

List<ServicesOrProductsModel> servicesOrProductsList = [
  ServicesOrProductsModel(text: 'Services'),
  ServicesOrProductsModel(text: 'Products'),
];
