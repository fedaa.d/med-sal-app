class IndicatorModel {
  final int numer;
  final int index;
  IndicatorModel({
    required this.numer,
    required this.index,
  });
}

List<IndicatorModel> indicatorModelList = [
  IndicatorModel(
    numer: 1,
    index: 0,
  ),
  IndicatorModel(
    numer: 2,
    index: 1,
  ),
  IndicatorModel(
    numer: 3,
    index: 2,
  ),
  IndicatorModel(
    numer: 4,
    index: 3,
  ),
  IndicatorModel(
    numer: 5,
    index: 4,
  ),
  IndicatorModel(
    numer: 6,
    index: 5,
  ),
  IndicatorModel(
    numer: 7,
    index: 6,
  ),
  IndicatorModel(
    numer: 8,
    index: 7,
  ),
  IndicatorModel(
    numer: 9,
    index: 8,
  ),
];
