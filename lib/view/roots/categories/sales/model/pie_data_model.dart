import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

int touchedIndex = 0;

class Data {
  final double? percent;
  final Color? color;

  Data({this.percent, this.color});
}

class PieData {
  static List<Data> data = [
    Data(percent: 95, color: AppColors.secondaryColor),
    Data(percent: 15, color: AppColors.glowingGreenColor),
  ];
}

List<PieChartSectionData> getSections({required double radiusCircle}) =>
    PieData.data
        .asMap()
        .map<int, PieChartSectionData>((index, data) {
          final isTouched = index == touchedIndex;
          final double radius = isTouched ? radiusCircle : radiusCircle;

          final value = PieChartSectionData(
            color: data.color,
            value: data.percent,
            title: '',
            radius: radius,
          );

          return MapEntry(index, value);
        })
        .values
        .toList();
