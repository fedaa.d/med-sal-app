import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class SeeAllText extends StatelessWidget {
  const SeeAllText({super.key});

  @override
  Widget build(BuildContext context) {
    return TextButton(
      onPressed: (){},
      child: Row(
        children: [
          Text(
            'See All',
            style: TextStyle(
              fontSize: AppSize.size16,
              fontWeight: FontWeight.w300,
              color: AppColors.secondaryColor,
            ),
          ),
          const SizedBox(
            width: 5,
          ),
          Icon(
            Icons.arrow_forward,
            color: AppColors.secondaryColor,
          ),
        ],
      ),
    );
  }
}
