import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_images.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class ProfileSection extends StatelessWidget {
  const ProfileSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 19,
        top: 5,
      ),
      child: Row(
        children: [
          Container(
            width: 120,
            height: 120,
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage(
                  AppImages.personImage,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 9),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Dr Lana Ahmad',
                  style: TextStyle(
                    fontSize: AppSize.size20,
                    fontWeight: FontWeight.w600,
                    color: AppColors.secondaryColor,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      'Dentist ',
                      style: TextStyle(
                        fontSize: AppSize.size18,
                        fontWeight: FontWeight.w500,
                        color: AppColors.lightBlackColor,
                      ),
                    ),
                    Image.asset(
                      AppImages.dentistryGrey,
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
