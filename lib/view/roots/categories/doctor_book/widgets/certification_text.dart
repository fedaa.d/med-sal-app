import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class CertificationText extends StatelessWidget {
  const CertificationText({super.key});

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Certification',
      style: TextStyle(
        fontSize: AppSize.size18,
        fontWeight: FontWeight.w600,
        color: AppColors.blackColor,
      ),
    );
  }
}
