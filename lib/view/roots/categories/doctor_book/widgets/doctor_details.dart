import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/doctor_book/widgets/about_text.dart';
import 'package:mad_sal/view/roots/categories/doctor_book/widgets/certification_text.dart';
import 'package:mad_sal/view/roots/categories/doctor_book/widgets/details_about_text.dart';
import 'package:mad_sal/view/roots/categories/doctor_book/widgets/details_certification_text.dart';
import 'package:mad_sal/view/roots/categories/doctor_book/widgets/gallery_text.dart';
import 'package:mad_sal/view/roots/categories/doctor_book/widgets/listgallery.dart';
import 'package:mad_sal/view/roots/categories/doctor_book/widgets/see_all_text.dart';

class DoctorDetails extends StatelessWidget {
  const DoctorDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 354,
      height: 480,
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
          side: BorderSide(
            color: AppColors.secondaryColor,
            width: 1.0,
          ),
        ),
      ),
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 10,
              ),
              const CertificationText(),
              const SizedBox(
                height: 8,
              ),
              const DetailsCertificationText(),
              const SizedBox(
                height: 22,
              ),
              const AboutText(),
              const SizedBox(
                height: 8,
              ),
              const DetailsAboutText(),
              const SizedBox(
                height: 16,
              ),
              const Row(
                children: [
                  GalleryText(),
                  SizedBox(
                    width: 165,
                  ),
                  SeeAllText(),
                ],
              ),
              Container(
                  padding: const EdgeInsets.symmetric(horizontal: 5),
                  width: double.infinity,
                  height: 150,
                  child: const ListGalleryDoctors()),
            ],
          ),
        ),
      ),
    );
  }
}
