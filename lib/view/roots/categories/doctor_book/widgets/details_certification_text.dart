import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class DetailsCertificationText extends StatelessWidget {
  const DetailsCertificationText({super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      'Lorem ipsum dolor sit amet consectetur. Enim ipsum dolor aliquam non.',
      style: TextStyle(
        fontSize: AppSize.size18,
        fontWeight: FontWeight.w400,
        color: AppColors.subColor,
      ),
    );
  }
}
