import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class DetailsAboutText extends StatelessWidget {
  const DetailsAboutText({super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      'Lorem ipsum dolor sit amet consectetur. Viverra ut posuere dolor faucibus duis. Viverra proin malesuada quam placerat. Sed nisl et diam sit diam.',
      style: TextStyle(
        fontSize: AppSize.size18,
        fontWeight: FontWeight.w400,
        color: AppColors.subColor,
      ),
    );
  }
}
