import 'package:flutter/cupertino.dart';
import 'package:mad_sal/view/roots/categories/doctor_book/model/gallery_model.dart';

class ListGalleryDoctors extends StatelessWidget {
  const ListGalleryDoctors({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        scrollDirection: Axis.horizontal,
        shrinkWrap: true,
        itemBuilder: (context, index) => Row(
              children: [
                Container(
                  width: 104,
                  height: 98,
                  child: Image.asset(
                    galleryDoctorsModelList[index].image1,
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                Container(
                  width: 104,
                  height: 98,
                  child: Image.asset(
                    galleryDoctorsModelList[index].image2,
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                Container(
                  width: 104,
                  height: 98,
                  child: Image.asset(
                    galleryDoctorsModelList[index].image3,
                  ),
                ),
              ],
            ),
        separatorBuilder: (context, index) => const SizedBox(
              width: 5,
            ),
        itemCount: galleryDoctorsModelList.length);
  }
}
