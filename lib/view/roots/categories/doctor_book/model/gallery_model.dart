import 'package:mad_sal/core/constants/app_images.dart';

class GalleryDoctorsModel {
  final String image1;
  final String image2;
  final String image3;

  GalleryDoctorsModel({
    required this.image1,
    required this.image2,
    required this.image3,
  });
}

List<GalleryDoctorsModel> galleryDoctorsModelList = [
  GalleryDoctorsModel(
    image1: '${AppImages.root}Rectangle 138.png',
    image2: '${AppImages.root}Rectangle 139.png',
    image3: '${AppImages.root}Rectangle 141.png',
  ),
];
