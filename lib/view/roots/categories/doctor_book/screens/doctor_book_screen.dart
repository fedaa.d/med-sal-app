import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/shared/custom_button.dart';
import 'package:mad_sal/view/roots/categories/doctor_book/widgets/doctor_details.dart';
import 'package:mad_sal/view/roots/categories/doctor_book/widgets/profile_section.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class DoctorBookScreen extends StatelessWidget {
  const DoctorBookScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: publicAppBar(title: 'Doctor Book'),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const ProfileSection(),
            const SizedBox(
              height: 14,
            ),
            const DoctorDetails(),
            const SizedBox(
              height: 14,
            ),
            CustomButton(
                text: 'Take appiontment',
                onPressed: () =>
                    Get.toNamed(AppNameRoutes.doctorAppointmentScreen),
                width: double.infinity,
                height: 80)
          ],
        ),
      ),
    );
  }
}
