import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class TextWithLines extends StatelessWidget {
  const TextWithLines({
    Key? key,
    required this.title,
    required this.secondryLine,
  }) : super(key: key);
  final String title;
  final String secondryLine;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 18.0, right: 18.0),
      child: Row(
        children: [
          Text('___ ', style: TextStyle(color: AppColors.lightWhiteColor)),
          Padding(
            padding: const EdgeInsets.only(top: 14.0),
            child: Text(title,
                style: TextStyle(
                    color: AppColors.lightBlackColor,
                    fontSize: AppSize.size18)),
          ),
          Text(secondryLine,
              style: TextStyle(
                color: AppColors.lightWhiteColor,
              )),
        ],
      ),
    );
  }
}
