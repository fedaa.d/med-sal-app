import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/registration_details_controller.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class UploadFilePdf extends GetView<RegistrationDetailsControllerImp> {
  const UploadFilePdf({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(RegistrationDetailsControllerImp());
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        MaterialButton(
          onPressed: () => controller.pickFile(),
          child: Image.asset(AppImages.pdfImage),
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
