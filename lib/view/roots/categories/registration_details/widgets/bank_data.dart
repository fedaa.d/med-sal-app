import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class BankData extends StatelessWidget {
  const BankData({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: ListView.separated(
          shrinkWrap: true,
          itemBuilder: (BuildContext context, int index) => Container(
                alignment: Alignment.centerLeft,
                width: double.infinity,
                height: 53,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: AppColors.lightWhiteColor)),
                child: Padding(
                  padding: const EdgeInsets.only(left: 18.0),
                  child: Text(
                    bankDataText[index],
                    style: const TextStyle(
                        color: AppColors.blackColor, fontSize: AppSize.size16),
                  ),
                ),
              ),
          separatorBuilder: (BuildContext context, int index) => const SizedBox(
                height: 10,
              ),
          itemCount: bankDataText.length),
    );
  }
}

List<String> bankDataText = [
  'Mr: ALI AHMAN',
  'AL47 2121 1009 0000 0002',
  'METWAU4B',
];
