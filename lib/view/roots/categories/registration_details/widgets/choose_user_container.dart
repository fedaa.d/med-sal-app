import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/registration_details/widgets/accept_or_refuse.dart';

class ChooseUserContainer extends StatelessWidget {
  const ChooseUserContainer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(bottom: 18.0),
      child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
        AcceptOrRefuse(
          text: 'Accept',
        ),
        AcceptOrRefuse(
          text: 'Refuse',
        ),
      ]),
    );
  }
}
