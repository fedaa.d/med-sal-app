import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class AcceptOrRefuse extends StatelessWidget {
  const AcceptOrRefuse({
    Key? key,
    required this.text,
  }) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 42,
      child: Container(
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: AppColors.secondaryColor,
            border: Border.all(color: AppColors.secondaryColor),
            borderRadius: BorderRadius.circular(15)),
        child: Padding(
          padding: const EdgeInsets.only(left: 33.0, right: 33.0),
          child: Text(
            text,
            style: const TextStyle(
                color: AppColors.whiteColor,
                fontSize: AppSize.size18,
                fontWeight: FontWeight.w600),
          ),
        ),
      ),
    );
  }
}
