import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/registration_details_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/shared/custom_text_field.dart';
import 'package:mad_sal/view/roots/categories/registration_details/widgets/bank_data.dart';
import 'package:mad_sal/view/roots/categories/registration_details/widgets/choose_user_container.dart';
import 'package:mad_sal/view/roots/categories/registration_details/widgets/text_with_lines.dart';
import 'package:mad_sal/view/roots/categories/registration_details/widgets/upload_file_pdf.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class RegistrationDetailsScreen
    extends GetView<RegistrationDetailsControllerImp> {
  const RegistrationDetailsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(RegistrationDetailsControllerImp());
    return Scaffold(
        backgroundColor: AppColors.whiteColor,
        appBar: publicAppBar(title: 'Registration Request Details'),
        body: SafeArea(
            child: SingleChildScrollView(
          child: GetBuilder<RegistrationDetailsControllerImp>(
            builder: (_) => Form(
              key: controller.formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const SizedBox(
                    height: 30,
                  ),
                  CustomTextFelid(
                    label: 'Email',
                    controller: controller.emailController,
                    hint: 'yourname@mail.com',
                    keyboardType: TextInputType.emailAddress,
                    icon: IconButton(
                      icon: SvgPicture.asset(
                        AppIcons.mailIcon,
                        colorFilter: ColorFilter.mode(
                            AppColors.borderColor, BlendMode.srcIn),
                      ),
                      onPressed: () {},
                    ),
                    obscure: false,
                  ),
                  CustomTextFelid(
                    label: 'Password',
                    controller: controller.passwordController,
                    hint: '**************',
                    keyboardType: TextInputType.visiblePassword,
                    icon: IconButton(
                      onPressed: () => controller.visibilityPassword(),
                      icon: Icon(
                        controller.isPassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                    ),
                    obscure: controller.isPassword,
                  ),
                  CustomTextFelid(
                    label: 'Confirm Password',
                    controller: controller.confirmPasswordController,
                    hint: '**************',
                    keyboardType: TextInputType.visiblePassword,
                    icon: IconButton(
                      onPressed: () => controller.visibilityConfirmPassword(),
                      icon: Icon(
                        controller.isConfirmPassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                    ),
                    obscure: controller.isConfirmPassword,
                  ),
                  CustomTextFelid(
                      label: 'Contact number',
                      controller: controller.numberController,
                      hint: '+963 123456789',
                      keyboardType: TextInputType.visiblePassword,
                      icon: IconButton(
                        onPressed: () {
                          controller.visibilityConfirmPassword();
                        },
                        icon: SvgPicture.asset(
                          AppIcons.callIcon,
                          colorFilter: ColorFilter.mode(
                              AppColors.borderColor, BlendMode.srcIn),
                        ),
                      ),
                      obscure: true),
                  CustomTextFelid(
                      label: 'Busines name',
                      controller: controller.businesController,
                      hint: 'yourname @mail.com',
                      keyboardType: TextInputType.visiblePassword,
                      icon: IconButton(
                        onPressed: () {},
                        icon: SvgPicture.asset(
                          AppIcons.businesIcon,
                          colorFilter: ColorFilter.mode(
                              AppColors.borderColor, BlendMode.srcIn),
                        ),
                      ),
                      obscure: true),
                  CustomTextFelid(
                      label: 'Service Type',
                      controller: controller.servicesController,
                      hint: 'Cleaning Teeth',
                      keyboardType: TextInputType.visiblePassword,
                      icon: IconButton(
                        onPressed: () {},
                        icon: SvgPicture.asset(
                          AppIcons.editIcon,
                          colorFilter: ColorFilter.mode(
                              AppColors.borderColor, BlendMode.srcIn),
                        ),
                      ),
                      obscure: true),
                  const TextWithLines(
                    title: '  Bank data  ',
                    secondryLine: '  ____________________________',
                  ),
                  const BankData(),
                  const TextWithLines(
                    title: '  Information PDF  ',
                    secondryLine: ' ______________________',
                  ),
                  const UploadFilePdf(),
                  const ChooseUserContainer()
                ],
              ),
            ),
          ),
        )));
  }
}
