import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

AppBar publicAppBar({required String title}) {
  return AppBar(
    leading: GestureDetector(
      onTap: () => Get.back(),
      child: const Icon(
        Icons.arrow_back_ios,
        color: AppColors.blackColor,
        size: 20,
      ),
    ),
    centerTitle: true,
    title: Text(
      title,
      style: const TextStyle(
          fontSize: AppSize.size25,
          color: AppColors.blackColor,
          fontWeight: FontWeight.w500),
    ),
    toolbarHeight: 121,
    elevation: 0,
    backgroundColor: AppColors.lightgreyColor,
  );
}
