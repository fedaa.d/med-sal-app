import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class TitleSection extends StatelessWidget {
  const TitleSection({
    Key? key,
    required this.title,
  }) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 55,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            alignment: Alignment.center,
            width: 186,
            height: 60,
            decoration: BoxDecoration(
                border: Border.all(color: AppColors.secondaryColor, width: 2),
                borderRadius: const BorderRadius.only(
                    bottomLeft: Radius.circular(75),
                    topRight: Radius.circular(75)),
                color: AppColors.lightbackGroundColor),
            child:
                Text(title, style: AppTheme.themeArabic.textTheme.titleMedium),
          ),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
