import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/functions/categories_navigate.dart';
import 'package:mad_sal/view/roots/categories/model/main_category_model.dart';

class CategoriesList extends StatelessWidget {
  const CategoriesList({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) => GestureDetector(
              onTap: () => categoriesNavigatePages(index),
              child: Container(
                height: 93,
                width: double.infinity,
                decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 0.5,
                      offset: const Offset(0, -1),
                    ),
                  ],
                  color: AppColors.cardColor,
                  borderRadius: BorderRadius.circular(25),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Image.asset(
                      mainCategoryModelList[index].image,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 30.0),
                      child: Text(
                        mainCategoryModelList[index].title,
                        style: const TextStyle(
                            fontSize: AppSize.size25,
                            color: AppColors.blackColor),
                      ),
                    ),
                    const Spacer(),
                    IconButton(
                      padding: const EdgeInsets.only(right: 30),
                      onPressed: () {},
                      icon: SvgPicture.asset(
                        AppIcons.editIcon,
                        colorFilter: ColorFilter.mode(
                            AppColors.darkColor, BlendMode.srcIn),
                      ),
                    ),
                  ],
                ),
              ),
            ),
        separatorBuilder: ((BuildContext context, int index) => const SizedBox(
              height: 20,
            )),
        itemCount: mainCategoryModelList.length);
  }
}
