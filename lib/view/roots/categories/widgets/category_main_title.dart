import 'package:flutter/material.dart';

import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class CategoryMainTitle extends StatelessWidget {
  const CategoryMainTitle({
    Key? key,
    required this.title,
  }) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 50,
        ),
        Container(
          alignment: Alignment.center,
          height: 80,
          width: 266,
          decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: AppColors.secondaryColor,
                  spreadRadius: 2,
                  blurRadius: 0.5,
                  offset: const Offset(-6, -3),
                ),
                const BoxShadow(
                  color: AppColors.gryColor,
                  spreadRadius: 1,
                  blurRadius: 2,
                  offset: Offset(0, 2),
                ),
              ],
              color: AppColors.lightMainColor,
              borderRadius: BorderRadius.circular(25)),
          child: Text(title, style: AppTheme.themeArabic.textTheme.titleLarge),
        ),
        const SizedBox(
          height: 15,
        ),
      ],
    );
  }
}
