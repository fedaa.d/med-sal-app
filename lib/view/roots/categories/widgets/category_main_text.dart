import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class CategoryMainText extends StatelessWidget {
  const CategoryMainText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
        'Lorem ipsum dolor sit amet consectetur. Lectus lacus phasellus enim vitae id integer tincidunt. Tincidunt suspendisse aliquam pretium ut dapibus mus ut et.',
        style: TextStyle(color: AppColors.mainColor, fontSize: AppSize.size16));
  }
}
