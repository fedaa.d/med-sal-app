import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class GridViewServicesOrProduct extends StatelessWidget {
  const GridViewServicesOrProduct({
    Key? key,
    required this.image,
  }) : super(key: key);
  final String image;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GridView.builder(
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          itemCount: 8,
          gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
              mainAxisSpacing: 30,
              crossAxisSpacing: 20,
              maxCrossAxisExtent: 200),
          itemBuilder: (BuildContext context, int index) => Expanded(
                child: Container(
                  width: 167,
                  height: 200,
                  decoration: BoxDecoration(
                      boxShadow: const [
                        BoxShadow(
                          color: AppColors.gryColor,
                          spreadRadius: 1,
                          blurRadius: 2,
                          offset: Offset(0, 3),
                        ),
                      ],
                      borderRadius: BorderRadius.circular(25),
                      color: AppColors.whiteColor),
                  child: Padding(
                    padding: const EdgeInsets.only(
                        top: 20.0, left: 6.0, right: 6.0, bottom: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Image.asset(
                            image,
                            width: double.infinity,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Text(
                          'Service 1',
                          style: AppTheme.themeArabic.textTheme.labelMedium,
                        ),
                        Text(
                          'Cost 1',
                          style: AppTheme.themeArabic.textTheme.labelSmall,
                        ),
                      ],
                    ),
                  ),
                ),
              )),
    );
  }
}
