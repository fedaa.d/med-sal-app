import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

AppBar customAppBar({required String title}) {
  return AppBar(
    leading: const Padding(
      padding: EdgeInsets.only(left: 70.0),
      child: Icon(
        Icons.arrow_back_ios,
        color: AppColors.blackColor,
        size: 20,
      ),
    ),
    centerTitle: true,
    title: Text(
      title,
      style: const TextStyle(
          fontSize: AppSize.size20,
          color: AppColors.blackColor,
          fontWeight: FontWeight.w500),
    ),
    toolbarHeight: 121,
    elevation: 0,
    backgroundColor: AppColors.lightgreyColor,
  );
}
