import 'package:flutter/material.dart';

import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class ConfirmCancelButton extends StatelessWidget {
  const ConfirmCancelButton({
    Key? key,
    required this.height,
    required this.text,
    required this.buttonColor,
    required this.onPressed,
  }) : super(key: key);
  final double height;
  final String text;
  final Color buttonColor;
  final Function() onPressed;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 30,
        ),
        Container(
          height: height,
          width: double.infinity,
          decoration: ShapeDecoration(
            color: buttonColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          child: MaterialButton(
            onPressed: onPressed,
            child: Text(
              text,
              style: const TextStyle(
                color: AppColors.whiteColor,
                fontSize: AppSize.size18,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
