import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/admins_list_controller.dart';
import 'package:mad_sal/view/roots/categories/admins_list/widgets/list_admin_model.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';
import 'package:mad_sal/view/roots/categories/widgets/secondary_button.dart';

class AdminsList extends GetView<AdminsListControllerImp> {
  const AdminsList({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(AdminsListControllerImp());
    return Scaffold(
      appBar: publicAppBar(title: 'Admins List'),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.symmetric(vertical: 20),
              child: ListAdmin(),
            ),
            const SizedBox(
              height: 24,
            ),
            GetBuilder<AdminsListControllerImp>(
              builder: (_) => Indicator(
                index: controller.currentIndex,
              ),
            ),
            const SizedBox(
              height: 17,
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 18),
              child: SecondaryButton(
                text: 'Giving Permission',
                height: 50,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
