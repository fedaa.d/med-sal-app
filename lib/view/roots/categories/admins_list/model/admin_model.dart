class AdminModel {
  final String adminNum;

  AdminModel({
    required this.adminNum,
});
}

List<AdminModel> adminModel =[
  AdminModel(adminNum: 'Admin 1'),
  AdminModel(adminNum: 'Admin 2'),
  AdminModel(adminNum: 'Admin 3'),
  AdminModel(adminNum: 'Admin 4'),
  AdminModel(adminNum: 'Admin 5'),
  AdminModel(adminNum: 'Admin 6'),
  AdminModel(adminNum: 'Admin 7'),
];