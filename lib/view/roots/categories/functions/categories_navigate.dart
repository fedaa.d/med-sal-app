import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';

categoriesNavigatePages(int index) {
  switch (index) {
    case 0:
      Get.toNamed(AppNameRoutes.categoriesDoctorScreen);
      break;
    case 1:
      Get.toNamed(AppNameRoutes.pharmaciesScreen);
      break;

    case 2:
      Get.toNamed(AppNameRoutes.hospitalScreen);
      break;

    case 3:
      Get.toNamed(AppNameRoutes.labsScreen);
      break;

    case 4:
      Get.toNamed(AppNameRoutes.clinicScreen);
      break;
  }
  return index;
}
