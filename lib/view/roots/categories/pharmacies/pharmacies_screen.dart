import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/pharmacies_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/pharmacies/widgets/pharmacies_page_view.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';
import 'package:mad_sal/view/roots/categories/widgets/category_main_text.dart';
import 'package:mad_sal/view/roots/categories/widgets/category_main_title.dart';

class PharmaciesScreen extends GetView<PharmaciesControllerImp> {
  const PharmaciesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(PharmaciesControllerImp());
    return Material(
      child: Container(
        color: AppColors.lightbackGroundColor,
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            children: [
              const CategoryMainTitle(
                title: 'Pharmacies',
              ),
              const CategoryMainText(),
              const PharmaciesPageView(),
              const SizedBox(
                height: 20,
              ),
              GetBuilder<PharmaciesControllerImp>(
                builder: (_) => Indicator(
                  index: controller.currentIndex,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
