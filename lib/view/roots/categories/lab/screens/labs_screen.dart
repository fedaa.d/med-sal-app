import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/labs_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/lab/widgets/lab_page_view.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';
import 'package:mad_sal/view/roots/categories/widgets/category_main_text.dart';
import 'package:mad_sal/view/roots/categories/widgets/category_main_title.dart';

class LabsScreen extends GetView<LabsControllerImp> {
  const LabsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(LabsControllerImp());
    return Material(
      child: Container(
        color: AppColors.lightbackGroundColor,
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            children: [
              const CategoryMainTitle(
                title: 'Labs',
              ),
              const CategoryMainText(),
              const LabsPageView(),
              const SizedBox(
                height: 20,
              ),
              GetBuilder<LabsControllerImp>(
                builder: (_) => Indicator(
                  index: controller.currentIndex,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
