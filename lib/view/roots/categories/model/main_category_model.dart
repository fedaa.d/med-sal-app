import 'package:mad_sal/core/constants/app_images.dart';

class MainCategoryModel {
  final String image;
  final String title;
  MainCategoryModel({
    required this.image,
    required this.title,
  });
}

List<MainCategoryModel> mainCategoryModelList = [
  MainCategoryModel(
    image: AppImages.stethoscopeImage,
    title: 'Doctor',
  ),
  MainCategoryModel(
    image: AppImages.pharmacyImage,
    title: 'Pharmacy',
  ),
  MainCategoryModel(
    image: AppImages.hospitalImage,
    title: 'Hospital',
  ),
  MainCategoryModel(
    image: AppImages.labImage,
    title: 'Lab',
  ),
  MainCategoryModel(
    image: AppImages.clinicImage,
    title: 'Clinic',
  ),
];
