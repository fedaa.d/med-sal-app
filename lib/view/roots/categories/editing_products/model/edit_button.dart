import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class EditButton extends StatelessWidget {
    final String textbt;
  final Function() pressed;
    EditButton(
  {
    required this.textbt,
    required this.pressed,
}
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 95,
      height: 42,
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        color: AppColors.mainColor,
      ),
      child: MaterialButton(
        onPressed: pressed,
        child: Text(
          textbt,
          style: const TextStyle(
            fontSize: AppSize.size16,
            fontWeight: FontWeight.w400,
            color: AppColors.whiteColor,
          ),
        ),
      ),
    );
  }
}




