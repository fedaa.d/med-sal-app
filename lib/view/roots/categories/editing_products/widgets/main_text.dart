import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class MainText extends StatelessWidget {
  const MainText({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 130),
      child: Column(
        children: [
          Text(
            'Products',
            style: TextStyle(
              fontSize: AppSize.size25,
              fontWeight: FontWeight.w600,
              color: AppColors.secondaryColor,
            ),
          ),
          Container(
            width: 120,
            height: 6,
            decoration: ShapeDecoration(
              shape: const RoundedRectangleBorder(),
              color: AppColors.secondaryColor,
            ),
          ),
        ],
      ),
    );
  }
}
