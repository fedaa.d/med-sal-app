import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/editing_products_controller.dart';
import 'package:mad_sal/view/roots/categories/editing_products/widgets/list_edit_products.dart';
import 'package:mad_sal/view/roots/categories/editing_products/widgets/main_text.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';
import 'package:mad_sal/view/roots/categories/widgets/secondary_button.dart';

class EditingProductsScreen extends GetView<EditingProductsController> {
  const EditingProductsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(EditingProductsController());
    return Scaffold(
      appBar: publicAppBar(
        title: 'Editing Products',
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 15,
            ),
            const MainText(),
            const SizedBox(
              height: 43,
            ),
            const ListEditProducts(),
            const SizedBox(
              height: 19,
            ),
            GetBuilder<EditingProductsController>(
              builder: (_) => Indicator(
                index: controller.currentIndex,
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 18, vertical: 22),
              child: SecondaryButton(height: 55, text: 'ADD'),
            ),
          ],
        ),
      ),
    );
  }
}
