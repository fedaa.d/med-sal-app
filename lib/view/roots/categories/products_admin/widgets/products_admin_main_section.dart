import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/products_admin_controller.dart';
import 'package:mad_sal/view/roots/categories/products_admin/widgets/products_admin_pageview.dart';
import 'package:mad_sal/view/roots/categories/products_admin/widgets/products_admin_titletable.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';

class MainProductsAdmin extends GetView<ProductsAdminControllerImp> {
  const MainProductsAdmin({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(ProductsAdminControllerImp());
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        children: [
          const ProductsAdminTitleTableList(),
          const ProductsAdminPageView(),
          const SizedBox(
            height: 25,
          ),
          GetBuilder<ProductsAdminControllerImp>(
              builder: (_) => Indicator(
                    index: controller.currentIndex,
                  ))
        ],
      ),
    );
  }
}
