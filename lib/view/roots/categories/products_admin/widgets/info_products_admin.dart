import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/products_admin/model/products_admin_model.dart';
import 'package:mad_sal/view/roots/categories/services_admin/widgets/info_text.dart';

class InfoProductsAdmin extends StatelessWidget {
  const InfoProductsAdmin({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        physics: const ScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) => Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InfoText(
                  text: productsAdminModelList[index].id,
                ),
                InfoText(
                  text: productsAdminModelList[index].name,
                ),
                InfoText(
                  text: productsAdminModelList[index].providerName,
                ),
                InfoText(
                  text: productsAdminModelList[index].cost,
                ),
                InfoText(
                  text: productsAdminModelList[index].date,
                ),
              ],
            ),
        separatorBuilder: (context, index) => const Divider(
              thickness: 2,
            ),
        itemCount: 10);
  }
}
