import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/products_admin_controller.dart';
import 'package:mad_sal/view/roots/categories/products_admin/widgets/info_products_admin.dart';

class ProductsAdminPageView extends GetView<ProductsAdminControllerImp> {
  const ProductsAdminPageView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(ProductsAdminControllerImp());
    return SizedBox(
      height: 500,
      child: GetBuilder<ProductsAdminControllerImp>(
        builder: (_) => PageView(
          controller: PageController(initialPage: 0),
          onPageChanged: (int val) => controller.change(val),
          children: [
            ...List.generate(
              9,
              (index) => const InfoProductsAdmin(),
            )
          ],
        ),
      ),
    );
  }
}
