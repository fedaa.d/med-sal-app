import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/products_admin/widgets/products_admin_table_text.dart';

class ProductsAdminTitleTableList extends StatelessWidget {
  const ProductsAdminTitleTableList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 55,
          width: double.infinity,
          decoration: BoxDecoration(
              color: AppColors.secondaryColor,
              borderRadius: BorderRadius.circular(10)),
          child: const Padding(
            padding: EdgeInsets.only(left: 20.0, right: 27.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ProductsAdminTableTextLabel(
                  title: 'ID',
                ),
                ProductsAdminTableTextLabel(
                  title: 'Name',
                ),
                ProductsAdminTableTextLabel(
                  title: 'Provider \n Name',
                ),
                ProductsAdminTableTextLabel(
                  title: 'Cost',
                ),
                ProductsAdminTableTextLabel(
                  title: 'Date',
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }
}
