import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/widgets/delivery_searh_file.dart';
import 'package:mad_sal/view/roots/categories/products_admin/widgets/products_admin_main_section.dart';
import 'package:mad_sal/view/roots/categories/widgets/custom_appbar.dart';

class ProductsAdminScreen extends StatelessWidget {
  const ProductsAdminScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: customAppBar(title: 'Products Management'),
      body: const SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: [DeliverySearchTextFiled(), MainProductsAdmin()],
        ),
      )),
    );
  }
}
