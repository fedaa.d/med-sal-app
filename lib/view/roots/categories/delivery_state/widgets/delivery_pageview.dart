import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/delivery_state_controller.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/widgets/info_services_provider.dart';

class DeliveryPageView extends GetView<DeliveryStateControllerImp> {
  const DeliveryPageView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(DeliveryStateControllerImp());
    return GetBuilder<DeliveryStateControllerImp>(
      builder: (_) => SizedBox(
        height: 400,
        child: PageView(
          controller: PageController(initialPage: 0),
          onPageChanged: (int val) => controller.change(val),
          children: [
            ...List.generate(
              9,
              (index) => const InfoServicesProvider(),
            )
          ],
        ),
      ),
    );
  }
}
