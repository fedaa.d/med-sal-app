import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/widgets/search_text_filed.dart';

class DeliverySearchTextFiled extends StatelessWidget {
  const DeliverySearchTextFiled({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(18.0),
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  blurRadius: 3,
                  offset: const Offset(0, 2),
                ),
              ],
            ),
            child: SearchTextFiled(
              height: 60,
              hintText: 'Enter Service Provider',
              suffixIcon: Padding(
                padding: const EdgeInsets.only(right: 35.0),
                child: SvgPicture.asset(AppIcons.searchIcon),
              ),
            ),
          ),
        ),
      ],
    );
  }
}
