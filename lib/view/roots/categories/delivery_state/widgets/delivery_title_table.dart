import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/widgets/table_text_lable.dart';

class TitleTableList extends StatelessWidget {
  const TitleTableList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            TableTextLabel(
              title: 'Num',
            ),
            TableTextLabel(
              title: 'Product Name',
            ),
            TableTextLabel(
              title: 'State',
            ),
            TableTextLabel(
              title: 'Service Name',
            ),
          ],
        ),
        SizedBox(
          height: 20,
        )
      ],
    );
  }
}
