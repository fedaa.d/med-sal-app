import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

class DeliveryEmptyContainer extends StatelessWidget {
  const DeliveryEmptyContainer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 53,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: AppColors.secondaryColor,
      ),
    );
  }
}
