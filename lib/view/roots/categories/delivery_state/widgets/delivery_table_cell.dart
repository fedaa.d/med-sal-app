import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class DeliveryTableCell extends StatelessWidget {
  const DeliveryTableCell({super.key, required this.title});
  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 4.0, top: 28, bottom: 10.0),
      child: TableCell(
          child: Text(
        title,
        style: const TextStyle(
            color: AppColors.blackColor,
            fontSize: AppSize.size13,
            fontWeight: FontWeight.w500),
        textAlign: TextAlign.center,
      )),
    );
  }
}
