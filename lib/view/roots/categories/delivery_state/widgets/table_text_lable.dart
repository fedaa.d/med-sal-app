import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class TableTextLabel extends StatelessWidget {
  const TableTextLabel({super.key, required this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Text(
      title,
      style: AppTheme.themeArabic.textTheme.labelMedium,
    );
  }
}
