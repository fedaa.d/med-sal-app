import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class ServicesProviderText extends StatelessWidget {
  const ServicesProviderText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Text(
          'Service Provider Name',
          style: AppTheme.themeArabic.textTheme.bodyMedium,
        ),
        const SizedBox(
          height: 25,
        ),
      ],
    );
  }
}
