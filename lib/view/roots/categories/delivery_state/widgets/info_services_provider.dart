import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/functions/select_colors.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/model/delivery_state_model.dart';

class InfoServicesProvider extends StatelessWidget {
  const InfoServicesProvider({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        physics: const ScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) => Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(delivereyStateModelList[index].number),
                  Text(
                    delivereyStateModelList[index].productName,
                  ),
                  Text(
                    delivereyStateModelList[index].state,
                    style: TextStyle(color: selectColor(index)),
                  ),
                  Text(delivereyStateModelList[index].serviceName),
                ],
              ),
            ),
        separatorBuilder: (context, index) => const SizedBox(
              height: 20,
            ),
        itemCount: 10);
  }
}
