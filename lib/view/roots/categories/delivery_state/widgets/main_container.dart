import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/delivery_state_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/widgets/delivery_pageview.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/widgets/delivery_title_table.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/widgets/empty_container.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/widgets/services_provider_text.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';

class DeliveryMainContainer extends GetView<DeliveryStateControllerImp> {
  const DeliveryMainContainer({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(DeliveryStateControllerImp());
    return Container(
      height: 650,
      width: double.infinity,
      decoration: BoxDecoration(
        color: AppColors.lightgreyColor,
        border: Border.all(color: AppColors.darkBorderColor),
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(45),
          topRight: Radius.circular(45),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(18.0),
        child: Column(
          children: [
            const ServicesProviderText(),
            Column(
              children: [
                const TitleTableList(),
                const DeliveryPageView(),
                const DeliveryEmptyContainer(),
                const SizedBox(height: 25),
                GetBuilder<DeliveryStateControllerImp>(
                  builder: (_) => Indicator(
                    index: controller.currentIndex,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
