import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

Color selectColor(int index) {
  if (index == 0 || index == 1 || index == 2 || index == 3) {
    return AppColors.mainColor;
  } else if (index == 4 || index == 5 || index == 6) {
    return AppColors.orangeColor;
  } else if (index == 7 || index == 8 || index == 9) {
    return AppColors.lightRedColor;
  }

  return AppColors.blackColor;
}
