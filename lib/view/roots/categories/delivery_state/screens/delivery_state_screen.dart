import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/widgets/delivery_searh_file.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/widgets/main_container.dart';
import 'package:mad_sal/view/roots/categories/widgets/custom_appbar.dart';

class DeliveryStateScreen extends StatelessWidget {
  const DeliveryStateScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: customAppBar(title: 'Delivery Report'),
      body: const SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: [
            DeliverySearchTextFiled(),
            DeliveryMainContainer(),
          ],
        ),
      )),
    );
  }
}
