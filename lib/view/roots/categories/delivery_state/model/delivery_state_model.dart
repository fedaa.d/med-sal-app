class DelivereyStateModel {
  final String number;
  final String productName;
  final String state;
  final String serviceName;

  DelivereyStateModel(
      {required this.number,
      required this.productName,
      required this.state,
      required this.serviceName});
}

List<DelivereyStateModel> delivereyStateModelList = [
  DelivereyStateModel(
    number: '1',
    productName: 'Product 1',
    state: 'Delivered',
    serviceName: 'Service 1',
  ),
  DelivereyStateModel(
    number: '2',
    productName: 'Product 2',
    state: 'Delivered',
    serviceName: 'Service 2',
  ),
  DelivereyStateModel(
    number: '3',
    productName: 'Product 3',
    state: 'Delivered',
    serviceName: 'Service 4',
  ),
  DelivereyStateModel(
    number: '4',
    productName: 'Product 4',
    state: 'Delivered',
    serviceName: 'Service 4',
  ),
  DelivereyStateModel(
    number: '5',
    productName: 'Product 5',
    state: 'In Progress',
    serviceName: 'Service 5',
  ),
  DelivereyStateModel(
    number: '6',
    productName: 'Product 6',
    state: 'In Progress',
    serviceName: 'Service 6',
  ),
  DelivereyStateModel(
    number: '7',
    productName: 'Product 7',
    state: 'In Progress',
    serviceName: 'Service 7',
  ),
  DelivereyStateModel(
    number: '8',
    productName: 'Product 8',
    state: 'Canceled',
    serviceName: 'Service 8',
  ),
  DelivereyStateModel(
    number: '9',
    productName: 'Product 9',
    state: 'Canceled',
    serviceName: 'Service 9',
  ),
  DelivereyStateModel(
    number: '10',
    productName: 'Product 10',
    state: 'Canceled',
    serviceName: 'Service 10',
  ),
];
