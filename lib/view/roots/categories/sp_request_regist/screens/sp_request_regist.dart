import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/sp_regist_request_controller.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';
import 'package:mad_sal/view/roots/categories/sp_request_regist/widgets/list_regist_request.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class SPRequestRigist extends GetView<SPRegistrationRequestControllerImp> {
  const SPRequestRigist({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(SPRegistrationRequestControllerImp());
    return Scaffold(
      appBar: publicAppBar(
        title: 'Registration Request \n     Service Provide',
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 32),
          child: Column(
            children: [
              const ListRegistRequest(),
              const SizedBox(
                height: 36,
              ),
              GetBuilder<SPRegistrationRequestControllerImp>(
                  builder: (_) => Indicator(
                        index: controller.currentIndex,
                      )),
            ],
          ),
        ),
      ),
    );
  }
}
