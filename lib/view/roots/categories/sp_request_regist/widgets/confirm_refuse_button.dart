import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class ConfirmRefuseButton extends StatelessWidget {
  final String text;
  final Color colorbt;
  final Color colortxt;
  final Function() pressed;

  const ConfirmRefuseButton({
    super.key,
    required this.text,
    required this.colorbt,
    required this.colortxt,
    required this.pressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:const EdgeInsets.symmetric(vertical: 10),
      width: 75,
      height: 35,
      decoration: ShapeDecoration(
        color: colorbt,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      child: MaterialButton(
        onPressed: pressed,
        child: Text(
          text,
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: AppSize.size12,
            fontWeight: FontWeight.w400,
            color: colortxt,
          ),
        ),
      ),
    );
  }
}
