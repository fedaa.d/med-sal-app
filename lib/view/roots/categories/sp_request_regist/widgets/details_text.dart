import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class DetailsText extends StatelessWidget {
  const DetailsText({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextButton(
        child: Text(
          'Details',
          style: TextStyle(
            fontSize: AppSize.size12,
            fontWeight: FontWeight.w400,
            color: AppColors.secondaryColor,
          ),
        ),
        onPressed: () => Get.toNamed(AppNameRoutes.datailsRegestrationScreen),
      ),
    );
  }
}
