import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/sp_request_regist/model/regist_request.dart';
import 'package:mad_sal/view/roots/categories/sp_request_regist/widgets/confirm_refuse_button.dart';
import 'package:mad_sal/view/roots/categories/sp_request_regist/widgets/details_text.dart';

class ListRegistRequest extends StatelessWidget {
  const ListRegistRequest({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        itemBuilder: (context, index) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18),
              child: Container(
                width: 355,
                height: 110,
                decoration: ShapeDecoration(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15),
                    side: BorderSide(
                      color: AppColors.lightColor,
                    ),
                  ),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(9.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        registRequest[index].requestNum,
                        style: const TextStyle(
                          fontSize: AppSize.size16,
                          fontWeight: FontWeight.w400,
                          color: AppColors.blackColor,
                        ),
                      ),
                      const SizedBox(
                        height: 3,
                      ),
                      Row(
                        children: [
                          ConfirmRefuseButton(
                            pressed: () {},
                            text: 'Confirm',
                            colorbt: AppColors.secondaryColor,
                            colortxt: AppColors.whiteColor,
                          ),
                          const SizedBox(
                            width: 26,
                          ),
                          ConfirmRefuseButton(
                            pressed: () {},
                            text: 'Refuse',
                            colorbt: AppColors.redHotColor,
                            colortxt: AppColors.blackColor,
                          ),
                          const SizedBox(
                            width: 100,
                          ),
                          const DetailsText(),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
        separatorBuilder: (context, index) => const SizedBox(
              height: 16,
            ),
        itemCount: 6);
  }
}
