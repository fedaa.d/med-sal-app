class RegistRequest {
  final String requestNum;

  RegistRequest(
  {
    required this.requestNum,
}
      );
}

List<RegistRequest> registRequest = [
  RegistRequest(requestNum: 'Registration Request 1'),
  RegistRequest(requestNum: 'Registration Request 2'),
  RegistRequest(requestNum: 'Registration Request 3'),
  RegistRequest(requestNum: 'Registration Request 4'),
  RegistRequest(requestNum: 'Registration Request 5'),
  RegistRequest(requestNum: 'Registration Request 6'),
];
