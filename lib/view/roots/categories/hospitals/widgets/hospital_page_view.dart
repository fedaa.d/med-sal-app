import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/hospitals_controller.dart';
import 'package:mad_sal/core/constants/app_images.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/view/roots/categories/widgets/category_title_section.dart';
import 'package:mad_sal/view/roots/categories/widgets/gridview_category.dart';

class HospitalPageView extends GetView<HospitalsControllerImp> {
  const HospitalPageView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(HospitalsControllerImp());
    return GetBuilder<HospitalsControllerImp>(
      builder: (_) => Expanded(
        child: PageView(
          controller: PageController(initialPage: 0),
          onPageChanged: (int val) => controller.change(val),
          children: [
            ...List.generate(
                9,
                (index) => SingleChildScrollView(
                      child: Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Column(
                          children: [
                            const TitleSection(
                              title: 'Services',
                            ),
                            GestureDetector(
                              onTap: () => Get.toNamed(
                                AppNameRoutes.servicesBookingScreen,
                              ),
                              child: const GridViewServicesOrProduct(
                                image: AppImages.buildingImage,
                              ),
                            ),
                            const TitleSection(
                              title: 'Products',
                            ),
                            GestureDetector(
                              onTap: () => Get.toNamed(
                                AppNameRoutes.productBookingScreen,
                              ),
                              child: const GridViewServicesOrProduct(
                                image: AppImages.buildingImage,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ))
          ],
        ),
      ),
    );
  }
}
