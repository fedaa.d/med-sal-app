import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/hospitals_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/hospitals/widgets/hospital_page_view.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';
import 'package:mad_sal/view/roots/categories/widgets/category_main_text.dart';
import 'package:mad_sal/view/roots/categories/widgets/category_main_title.dart';

class HospitalScreen extends GetView<HospitalsControllerImp> {
  const HospitalScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(HospitalsControllerImp());
    return Material(
      child: Container(
        color: AppColors.lightbackGroundColor,
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Column(
            children: [
              const CategoryMainTitle(
                title: 'Hospitals',
              ),
              const CategoryMainText(),
              const HospitalPageView(),
              const SizedBox(
                height: 20,
              ),
              GetBuilder<HospitalsControllerImp>(
                builder: (_) => Indicator(
                  index: controller.currentIndex,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
