import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/payment_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/schedule/screens/my_schedule_screen.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/model/payment_selection.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/widgets/line_container.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/widgets/list_discount_details.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/widgets/list_dr_name.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/widgets/list_cost_details.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/widgets/list_total_price.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/widgets/listcontainer_appointment.dart';
import 'package:mad_sal/view/roots/categories/widgets/confirm_cancel_button.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class ServiceAppointment extends GetView<PaymentController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: publicAppBar(
        title: '  Service 1 \n Appointment',
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 17, top: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ListDrName(),
              const SizedBox(
                height: 5,
              ),
              const ListCostDetails(),
              const SizedBox(
                height: 5,
              ),
              const ListDiscountDetails(),
              const SizedBox(
                height: 16,
              ),
              const LineSpace(),
              const SizedBox(
                height: 24,
              ),
              ListTotalPrice(),
              const SizedBox(
                height: 16,
              ),
              PaymentSelect(),
              const SizedBox(
                height: 55,
              ),
              ListContainerAppointment(),
              const SizedBox(
                height: 20,
              ),
              ConfirmCancelButton(
                  height: 70,
                  text: 'Confirm',
                  buttonColor: AppColors.secondaryColor,
                  onPressed: () {
                    Get.to(MyScheduleScreen());
                    ScaffoldMessenger.of(context)
                        .showSnackBar(PaymentController.orderConfirmed);
                  }),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5),
                child: ConfirmCancelButton(
                    height: 70,
                    text: 'Cancel',
                    buttonColor: AppColors.redHotColor,
                    onPressed: () {
                      ScaffoldMessenger.of(context)
                          .showSnackBar(PaymentController.orderCanceled);
                    }),
              ),
              ConfirmCancelButton(
                  height: 70,
                  text: 'Show my schedule',
                  buttonColor: AppColors.lightBlackColor,
                  onPressed: () {
                    Get.toNamed(AppNameRoutes.schedulScreen);
                    ScaffoldMessenger.of(context)
                        .showSnackBar(PaymentController.orderConfirmed);
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
