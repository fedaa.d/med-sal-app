import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mad_sal/controller/payment_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/model/period_appointment.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/widgets/time_appointment.dart';

class ListContainerAppointment extends GetView<PaymentController> {
  PaymentController paymentController = Get.put(PaymentController());
  List<bool> _isSelected = [true,false,false];
  int currentIndex =0;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 250,
      child: ListView.separated(
        shrinkWrap: true,
          scrollDirection: Axis.horizontal,
          itemBuilder: (context, index) => Container(
            width: 307,
            height: 100,
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
                side: BorderSide(
                  color: AppColors.lightColor,
                  width: 1,
                ),
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children:
              [
                const SizedBox(
                  height: 19,
                ),
                Row(
                  children: [
                    const SizedBox(
                      width: 115,
                    ),
                    Text(
                      periodAppointment[index].period,
                      style: TextStyle(
                        fontSize: AppSize.size20,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryColor,
                      ),
                    ),
                    const SizedBox(
                      width: 60,
                    ),
                    IconButton(
                        onPressed: (){},
                        icon: Icon(
                          Icons.arrow_forward,
                          color: AppColors.secondaryColor,
                          size: 24,
                        ),
                    ),
                  ],
                ),
                Container(
                  width: 85,
                  height: 3,
                  decoration: ShapeDecoration(
                      shape: RoundedRectangleBorder(
                        side: BorderSide(
                          color: AppColors.secondaryColor,
                          strokeAlign: BorderSide.strokeAlignCenter,
                          width: 3,
                        ),
                      ),
                  ),
                ),
                SizedBox(
                  height: 16,
                ),
                   GetBuilder<PaymentController>(
                     builder: (_) =>
                        Column(
                         children: [
                           Row(
                             children: [
                               TimeAppointment(
                                   text: '9:00',
                                 onTap: (){
                                     controller.setValue(0);
                                 },
                                 color: controller.value == 0 ? AppColors.secondaryColor : AppColors.whiteColor,
                               ),
                               const SizedBox(
                                 width: 10,
                               ),
                               TimeAppointment(
                                   text: '9:30',
                                 onTap: (){
                                     controller.setValue(1);
                                 },
                                 color: controller.value == 1 ? AppColors.secondaryColor : AppColors.whiteColor,
                               ),
                               const SizedBox(
                                 width: 10,
                               ),
                               TimeAppointment(
                                   text: '10:00',
                                 onTap: (){
                                     controller.setValue(2);
                                 },
                                 color: controller.value == 2 ? AppColors.secondaryColor : AppColors.whiteColor,
                               ),
                             ],
                           ),
                           const SizedBox(
                             height: 18,
                           ),
                          Row(
                            children: [
                              TimeAppointment(
                                text: '10:30',
                                onTap: (){
                                  controller.setValue(4);
                                },
                                color: controller.value == 4 ? AppColors.secondaryColor : AppColors.whiteColor,
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              TimeAppointment(
                                text: '11:30',
                                onTap: (){
                                  controller.setValue(5);
                                },
                                color: controller.value == 5 ? AppColors.secondaryColor : AppColors.whiteColor,
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              TimeAppointment(
                                text: '12:30',
                                onTap: (){
                                  controller.setValue(6);
                                },
                                color: controller.value == 6 ? AppColors.secondaryColor : AppColors.whiteColor,
                              ),
                            ],
                          ),

                         ],
                       ),

                   ),


              ],
            ),
          ),
          separatorBuilder: (context, index) => const SizedBox(
            width: 10,
          ),
          itemCount: 3),
    );
  }
}
