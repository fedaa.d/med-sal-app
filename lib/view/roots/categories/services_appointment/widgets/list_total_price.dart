import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/model/service_details.dart';

class ListTotalPrice extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
        itemBuilder: (context, index) => Row(
          children:
          [
            Text(
              totalPrice[index].titleInfo,
              style: TextStyle(
                fontSize: AppSize.size20,
                fontWeight: FontWeight.w500,
                color: AppColors.secondaryColor,
              ),
            ),
            SizedBox(
              width: 50,
            ),
            Text(
              totalPrice[index].info,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: AppSize.size20,
                color: AppColors.blackColor,
              ),
            ),
          ],
        ),
        separatorBuilder: (context,index) => SizedBox(
          width: 10,
        ),
        itemCount: 1,
    );
  }
}

