import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/model/service_details.dart';

class ListDiscountDetails extends StatelessWidget {
  const ListDiscountDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        itemBuilder: (context, index) => Column(
          children:
          [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  discountDetails[index].titleInfo,
                  style: TextStyle(
                    fontSize: AppSize.size20,
                    fontWeight: FontWeight.w500,
                    color: AppColors.secondaryColor,
                  ),
                ),
                SizedBox(
                  width: 13,
                ),
                Text(
                  discountDetails[index].info,
                  style: TextStyle(
                    fontSize: AppSize.size20,
                    fontWeight: FontWeight.w500,
                    color: AppColors.blackColor,
                  ),
                ),
              ],
            ),
          ],
        ),
        separatorBuilder: (context, index) => SizedBox(
          height: 5,
        ),
        itemCount: 1);
  }
}
