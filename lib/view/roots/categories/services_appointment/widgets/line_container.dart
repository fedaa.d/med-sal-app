import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

class LineSpace extends StatelessWidget {
  const LineSpace({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 350,
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          side: BorderSide(
            strokeAlign: BorderSide.strokeAlignCenter,
            width: 1.0,
            color: AppColors.lightWhiteColor,
          ),
        ),
      ),
    );
  }
}
