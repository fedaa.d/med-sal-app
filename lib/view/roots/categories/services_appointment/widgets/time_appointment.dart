import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class TimeAppointment extends StatelessWidget {
  final String text;
  final Color color;
  final Function() onTap;
  const TimeAppointment({super.key, required this.text,required this.color,required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
             onTap: onTap,
             child: Container(
               padding: const EdgeInsets.symmetric(horizontal: 23, vertical: 18),
               width: 90,
               height: 55,
               decoration: ShapeDecoration(
                 color: color,
                 shape: RoundedRectangleBorder(
                   borderRadius: BorderRadius.circular(20),
                   side: BorderSide(
                     color: AppColors.secondaryColor,
                     width: 2,
                   ),
                 ),
               ),
               child: Text(
                 text,
                 textAlign: TextAlign.center,
                 style: const TextStyle(
                   fontSize: AppSize.size16,
                   fontWeight: FontWeight.w400,
                   color: AppColors.blackColor,
                 ),
               ),
             ),
        );
  }
}
