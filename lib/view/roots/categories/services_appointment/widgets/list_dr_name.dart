import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/services_appointment/model/service_details.dart';

class ListDrName extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
      itemBuilder: (context, index) => Row(
        children:
        [
          Text(
            drName[index].titleInfo,
            style: TextStyle(
              fontSize: AppSize.size20,
              fontWeight: FontWeight.w500,
              color: AppColors.secondaryColor,
            ),
          ),
          Text(
            drName[index].info,
            style: TextStyle(
              fontSize: AppSize.size20,
              fontWeight: FontWeight.w500,
              color: AppColors.secondaryColor,
            ),
          ),
        ],
      ),
      separatorBuilder: (context,index) => SizedBox(
        width: 10,
      ),
      itemCount: 1,
    );
  }
}

