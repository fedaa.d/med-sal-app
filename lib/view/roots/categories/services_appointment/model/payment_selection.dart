import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/payment_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class PaymentSelect extends GetView<PaymentController> {
  PaymentController paymentController = Get.put(PaymentController());

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
          children:
          [
            Text(
              'Payment:',
              style: TextStyle(
                fontSize: AppSize.size20,
                fontWeight: FontWeight.w500,
                color: AppColors.secondaryColor,
              ),
            ),
            const SizedBox(
              width: 20,
            ),
            Row(
              children: [
                Obx( () =>
                     Radio(
                          value: 'Cash',
                          groupValue: paymentController.selectedPay.value,
                          onChanged: (value) {
                            paymentController.onClickedRadioButton(value);}
                      ),
                ),
                const Text(
                  'Cash',
                  style: TextStyle(
                    fontSize: AppSize.size20,
                    fontWeight: FontWeight.w400,
                    color: AppColors.blackColor,
                  ),
                ),
              ],
            ),
            Row(
              children: [
                Obx( () =>
                     Radio(
                          value: 'Card',
                          groupValue: paymentController.selectedPay.value,
                          onChanged: (value) {
                            paymentController.onClickedRadioButton(value);}
                      ),
                ),
                const Text(
                  'Card',
                  style: TextStyle(
                    fontSize: AppSize.size20,
                    fontWeight: FontWeight.w400,
                    color: AppColors.blackColor,
                  ),
                ),
              ],
            ),
          ],

    );
  }

}