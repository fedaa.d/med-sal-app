class ServiceDetails {
  final String titleInfo;
  final String info;
  ServiceDetails({
    required this.titleInfo,
    required this.info,
});
}


List<ServiceDetails> drName = [
  ServiceDetails(titleInfo: 'Dr:', info: 'Name'),

];

List<ServiceDetails> costDetails = [
  ServiceDetails(titleInfo: 'Cost:', info: '50\$'),

];

List<ServiceDetails> discountDetails = [
  ServiceDetails(titleInfo: 'Discount:', info: '12\$'),

];

List<ServiceDetails> totalPrice = [
  ServiceDetails(titleInfo: 'Total:', info: '44\$'),

];
