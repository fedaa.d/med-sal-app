import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_images.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class AdminPhotoSection extends StatelessWidget {
  const AdminPhotoSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 22.0),
              child: GestureDetector(
                onTap: () => Get.toNamed(AppNameRoutes.optionScreen),
                child: SvgPicture.asset(
                  AppIcons.listIcon,
                ),
              ),
            ),
            const SizedBox(
              width: 120,
            ),
            const Text(
              'Profile',
              style: TextStyle(
                  fontSize: AppSize.size22,
                  fontWeight: FontWeight.w600,
                  color: AppColors.blackColor),
            ),
          ],
        ),
        const SizedBox(
          height: 20,
        ),
        Image.asset(AppImages.profileImage)
      ],
    );
  }
}
