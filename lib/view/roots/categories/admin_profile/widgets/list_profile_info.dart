import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/admin_profile/model/profile_info.dart';

class ListProfileInfo extends StatelessWidget {
  const ListProfileInfo({required this.onTap, super.key});
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18),
          child: Container(
            child: ListTile(
              minLeadingWidth: 7.0,
              onTap: onTap,
              title: Padding(
                padding: const EdgeInsets.only(top: 11.0),
                child: Text(
                  profileInfo[index].title,
                  style: TextStyle(
                      fontSize: AppSize.size20,
                      fontWeight: FontWeight.w400,
                      color: AppColors.lightBlackColor
                  ),
                ),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 11.0),
                child: Text(
                  profileInfo[index].subTitle,
                  style: const TextStyle(
                      fontSize: AppSize.size20,
                      fontWeight: FontWeight.w400,
                      color: AppColors.blackColor),
                ),
              ),
              leading: SvgPicture.asset(
                profileInfo[index].icon,
                colorFilter: ColorFilter.mode(
                    AppColors.lightBlackColor, BlendMode.srcIn),
                width: 24,
                height: 24,
              ),
            ),
          ),
        ),
        separatorBuilder: ((context, index) => const Padding(
          padding: EdgeInsets.all(18.0),
          child: Divider(
            thickness: 2,
          ),
        )),
        itemCount: profileInfo.length);
  }
}
