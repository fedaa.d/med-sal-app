import 'package:mad_sal/core/constants/app_icons.dart';

class ProfileInfo {
  final String icon;
  final String title;
  final String subTitle;

  ProfileInfo(
  {
    required this.icon,
    required this.title,
    required this.subTitle,
}
      );
}


List<ProfileInfo> profileInfo = [
  ProfileInfo(
      icon: '${AppIcons.root}mail.svg',
      title: 'Email',
      subTitle: 'yourname @mail.com'),
  ProfileInfo(
      icon: '${AppIcons.root}call.svg',
      title: 'Phone',
      subTitle: '+963 987654321'),
  ProfileInfo(
      icon: '${AppIcons.root}calendar_clock.svg',
      title: 'Date of joining',
      subTitle: '25 / 10 / 2023'),
];