import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/admin_profile/widgets/admin_photo_section.dart';
import 'package:mad_sal/view/roots/categories/admin_profile/widgets/list_profile_info.dart';

class AdminProfile extends StatelessWidget {
  const AdminProfile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                  width: double.infinity,
                  height: 339,
                  decoration: BoxDecoration(
                    color: AppColors.mainColor,
                    border: Border.all(color: AppColors.borderColor),
                    borderRadius: const BorderRadius.only(
                        bottomLeft: Radius.circular(45),
                        bottomRight: Radius.circular(45)),
                  ),
                  child: const AdminPhotoSection()),
              const SizedBox(
                height: 23,
              ),
              ListProfileInfo(onTap: () {  },),
            ],
          ),
        ),
      ),
    );
  }
}
