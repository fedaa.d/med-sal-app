class EditService {
  final String service;
  EditService(
  {
    required this.service,
}
      );
}
List<EditService> editService = [
  EditService(service: 'Service 1'),
  EditService(service: 'Service 2'),
  EditService(service: 'Service 3'),
  EditService(service: 'Service 4'),
  EditService(service: 'Service 5'),
];