import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/editing_services_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/editing_services/model/edit_button.dart';
import 'package:mad_sal/view/roots/categories/editing_services/model/edit_service.dart';

class ListEditService extends GetView<EditingServicesController> {
  const ListEditService({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(EditingServicesController());
    return ListView.separated(
      shrinkWrap: true,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 13),
          child: Container(
            padding: const EdgeInsets.all(8.0),
            width: 357,
            height: 115,
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
                side: BorderSide(
                  color: AppColors.lightgreyColor,
                  width: 3,
                ),
              ),
            ),
            child: Column(
              children:
              [
                Row(
                  children: [
                    Text(
                      editService[index].service,
                      style: const TextStyle(
                        fontSize: AppSize.size20,
                        fontWeight: FontWeight.w400,
                        color: AppColors.blackColor,
                      ),
                    ),
                    const SizedBox(
                      width: 220,
                    ),
                    PopupMenuButton(itemBuilder: (context) => [
                        const PopupMenuItem(child: Text(
                            'Cancel',
                        ),
                        ),
                    ],
                    ),
                  ],
                ),
                Row(
                  children: [
                    const SizedBox(
                      width: 71,
                    ),
                     GetBuilder<EditingServicesController>(builder: (_) =>
                         EditButton(textbt: 'Delete', pressed: ()
                     {
                       controller.removeItem(index);
                     }),
                     ),


                    const SizedBox(
                      width: 29,
                    ),
                    EditButton(textbt: 'Modify', pressed: (){}),
                  ],
                ),
              ],
            ),
          ),
        ),
        separatorBuilder: (context, index) => const SizedBox(
          height: 18,
        ),
        itemCount: 5);
  }
}
