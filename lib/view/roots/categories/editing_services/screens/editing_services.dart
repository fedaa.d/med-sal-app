import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/editing_services_controller.dart';
import 'package:mad_sal/view/roots/categories/editing_services/widgets/list_edit_service.dart';
import 'package:mad_sal/view/roots/categories/editing_services/widgets/main_text.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';
import 'package:mad_sal/view/roots/categories/widgets/secondary_button.dart';

class EditingServicesScreen extends GetView<EditingServicesController> {
  const EditingServicesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(EditingServicesController());
    return Scaffold(
      appBar: publicAppBar(
        title: 'Editing Services',
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SizedBox(
              height: 15,
            ),
            const MainText(),
            const SizedBox(
              height: 43,
            ),
            const ListEditService(),
            const SizedBox(
              height: 19,
            ),
            GetBuilder<EditingServicesController>(
              builder: (_) => Indicator(
                index: controller.currentIndex,
              ),
            ),
            const Padding(
              padding: EdgeInsets.symmetric(horizontal: 18, vertical: 22),
              child: SecondaryButton(height: 55, text: 'ADD'),
            ),
          ],
        ),
      ),
    );
  }
}
