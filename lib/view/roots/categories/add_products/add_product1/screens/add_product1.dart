import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/add_products/add_product1/widgets/confirm_button.dart';
import 'package:mad_sal/view/roots/categories/add_products/add_product1/widgets/discount_text.dart';
import 'package:mad_sal/view/roots/categories/add_products/add_product1/widgets/discount_total_textfeild.dart';
import 'package:mad_sal/view/roots/categories/add_products/add_product1/widgets/name_of_service_textfeild.dart';
import 'package:mad_sal/view/roots/categories/add_products/add_product1/widgets/upload_photo_section.dart';
import 'package:mad_sal/view/roots/categories/add_products/add_product1/widgets/yes_or_no_discount.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class AddProduct1 extends StatelessWidget {
  const AddProduct1({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: publicAppBar(
        title: 'ADD Product',
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const UploadPhotosSection(),
            NameAndcostTextFeilds(),
            const DiscountText(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
              child: YesOrNoDiscount(),
            ),
            DiscountTextFeilds(),
            ConfirmButton(),
          ],
        ),
      ),
    );
  }
}
