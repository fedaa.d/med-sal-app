import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/add_product_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class YesOrNoDiscount extends GetView<AddProductControllerImp> {
  @override
  Widget build(BuildContext context) {
    Get.put(AddProductControllerImp());
    return GetBuilder<AddProductControllerImp>(
      builder: (_) => Row(
        children: [
          GestureDetector(
            onTap: () {
              controller.setValue1(0);
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              width: 65,
              height: 42,
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                color: controller.value1 == 0
                    ? AppColors.secondaryColor
                    : AppColors.lightWhiteColor,
              ),
              child: const Text(
                'Yes',
                style: TextStyle(
                  fontSize: AppSize.size14,
                  fontWeight: FontWeight.w400,
                  color: AppColors.whiteColor,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
          const SizedBox(
            width: 10,
          ),
          GestureDetector(
            onTap: () {
              controller.setValue1(1);
            },
            child: Container(
              padding: const EdgeInsets.symmetric(vertical: 10),
              width: 65,
              height: 42,
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
                color: controller.value1 == 1
                    ? AppColors.secondaryColor
                    : AppColors.lightWhiteColor,
              ),
              child: const Text(
                'No',
                style: TextStyle(
                  fontSize: AppSize.size14,
                  fontWeight: FontWeight.w400,
                  color: AppColors.blackColor,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
