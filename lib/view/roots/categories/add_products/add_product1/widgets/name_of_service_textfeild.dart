import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/shared/custom_text_field.dart';

class NameAndcostTextFeilds extends StatelessWidget {
  var serviceNameController = TextEditingController();
  var serviceCostController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 4),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomTextFelid(
            label: 'Enter Name of service',
            hint: 'Please Enter Name of Service',
            icon: IconButton(
              onPressed: () {},
              icon: SvgPicture.asset(AppIcons.editIcon),
            ),
            obscure: false,
            controller: serviceNameController,
          ),
          const SizedBox(
            height: 5,
          ),
          CustomTextFelid(
            label: 'Enter Cost',
            hint: 'Please Enter Cost',
            icon: IconButton(
              onPressed: () {},
              icon: SvgPicture.asset(AppIcons.editIcon),
            ),
            obscure: false,
            controller: serviceCostController,
          ),
        ],
      ),
    );
  }
}
