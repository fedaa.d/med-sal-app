import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/add_products/add_product1/widgets/list_upload_photo.dart';
import 'package:mad_sal/view/roots/categories/add_products/add_product1/widgets/yes_no_button.dart';

class UploadPhotosSection extends StatelessWidget {
  const UploadPhotosSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 18),
      child: Container(
        width: 400,
        height: 260,
        child: InputDecorator(
          decoration: InputDecoration(
            labelText: 'Upload photos of product',
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15),
              borderSide: BorderSide(
                color: AppColors.lightgreyColor,
                width: 1,
              ),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(width: 345, height: 132, child: ListUploadPhotos()),
                const SizedBox(
                  height: 5,
                ),
                const Text(
                  'Are you sure?',
                  style: TextStyle(
                    fontSize: AppSize.size14,
                    fontWeight: FontWeight.w400,
                    color: AppColors.blackColor,
                  ),
                ),
                const SizedBox(
                  height: 9,
                ),
                YesOrNoButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
