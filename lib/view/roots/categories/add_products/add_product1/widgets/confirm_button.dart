import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mad_sal/core/shared/custom_button.dart';

class ConfirmButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 5),
      child: Container(
        child: CustomButton(
          width: 375,
          height: 55,
          text: 'Confirm',
          onPressed: () {},
        ),
      ),
    );
  }
}
