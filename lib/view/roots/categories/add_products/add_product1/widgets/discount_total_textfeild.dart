import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/shared/custom_text_field.dart';

class DiscountTextFeilds extends StatelessWidget {
  var discountController = TextEditingController();
  var totalCostController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CustomTextFelid(
          label: 'Enter Discount',
          hint: 'Please Enter Discount %',
          icon: IconButton(
            onPressed: () {},
            icon: SvgPicture.asset(AppIcons.editIcon),
          ),
          obscure: false,
          controller: discountController,
        ),
      ],
    );
  }
}
