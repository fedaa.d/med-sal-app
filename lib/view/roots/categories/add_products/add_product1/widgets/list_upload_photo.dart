import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/auth/register_provider_controller.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class ListUploadPhotos extends GetView<RegisterProviderControllerImp> {
  const ListUploadPhotos({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(RegisterProviderControllerImp());
    return ListView.separated(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) =>
            GetBuilder<RegisterProviderControllerImp>(
              builder: (_) => Container(
                width: 148,
                height: 125,
                decoration: ShapeDecoration(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12),
                  ),
                ),
                child: InkWell(
                  onTap: controller.pickFile,
                  child: Image.asset(
                    AppImages.uploadImage,
                  ),
                ),
              ),
            ),
        separatorBuilder: (context, index) => const SizedBox(
              width: 41,
            ),
        itemCount: 2);
  }
}
