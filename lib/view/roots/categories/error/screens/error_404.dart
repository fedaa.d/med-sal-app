import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/error/widgets/home_button.dart';
import 'package:mad_sal/view/roots/categories/error/widgets/photo_section.dart';

class Error404 extends StatelessWidget {
  const Error404({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.whiteColor,
      child: Column(
        children: [
          const PhotoSection(),
          GoHomeButton(),
        ],
      ),
    );
  }
}
