import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class PhotoSection extends StatelessWidget {
  const PhotoSection({super.key});

  @override
  Widget build(BuildContext context) {
    return const Image(
      image: AssetImage(
        AppImages.errorImage,
      ),
      width: 390,
      height: 613,
    );
  }
}
