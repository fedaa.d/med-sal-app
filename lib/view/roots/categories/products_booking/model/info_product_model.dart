import 'package:mad_sal/core/constants/app_images.dart';

class ProductModel {
  final String titleInfo;
  final String info;
  String? imageAvatar;
  ProductModel({
    required this.titleInfo,
    required this.info,
    this.imageAvatar,
  });
}

List<ProductModel> productModelList = [
  ProductModel(
      titleInfo: 'Company: ',
      info: 'Name 1',
      imageAvatar: AppImages.bookingImage),
  ProductModel(
      titleInfo: 'Cost: ', info: 'Cost 1', imageAvatar: AppImages.bookingImage),
  ProductModel(
      titleInfo: 'Quantity ', info: '2', imageAvatar: AppImages.bookingImage),
  ProductModel(titleInfo: 'Description: ', info: 'Easy to use'),
  ProductModel(titleInfo: 'Date of request: ', info: 'MM / DD'),
];
