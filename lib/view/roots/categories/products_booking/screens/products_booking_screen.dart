import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/product_booking_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/products_booking/widgets/products_pageview.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class ProducstBookingScreen extends GetView<ProductBookingControllerImp> {
  const ProducstBookingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(ProductBookingControllerImp());
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: publicAppBar(title: 'Products booking'),
      body: SafeArea(
          child: Column(
        children: [
          Expanded(
            child: PageView(
              controller: PageController(initialPage: 0),
              onPageChanged: (int val) => controller.change(val),
              children: [
                ...List.generate(9, (index) => const ProductsInfoPageView())
              ],
            ),
          ),
          GetBuilder<ProductBookingControllerImp>(
            builder: (_) => Indicator(
              index: controller.currentIndex,
            ),
          ),
          const SizedBox(
            height: 10,
          )
        ],
      )),
    );
  }
}
