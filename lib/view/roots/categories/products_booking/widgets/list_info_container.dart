import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/view/roots/categories/products_booking/model/info_product_model.dart';
import 'package:mad_sal/view/roots/categories/products_booking/widgets/list_info_product.dart';

class ListInfoContainer extends StatelessWidget {
  const ListInfoContainer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      physics: const ScrollPhysics(),
      separatorBuilder: (BuildContext context, int index) => const SizedBox(),
      itemCount: 3,
      shrinkWrap: true,
      itemBuilder: (BuildContext context, int index) => Stack(
        children: [
          GestureDetector(
            onTap: () => Get.toNamed(AppNameRoutes.servicesAdminScreen),
            child: Container(
              margin: const EdgeInsets.all(18.0),
              child: Container(
                margin: const EdgeInsets.only(top: 18.0),
                height: 240,
                width: double.infinity,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 0.5,
                        offset: const Offset(0, 1),
                      ),
                    ],
                    color: AppColors.cardColor,
                    border: Border.all(
                      color: AppColors.secondaryColor,
                    ),
                    borderRadius: BorderRadius.circular(25)),
                child: const Padding(
                  padding: EdgeInsets.all(18.0),
                  child: ListInfoProduct(),
                ),
              ),
            ),
          ),
          Positioned(
              top: 2,
              right: 1,
              child: CircleAvatar(
                radius: 50,
                backgroundColor: Colors.transparent,
                backgroundImage:
                    AssetImage('${productModelList[index].imageAvatar}'),
              ))
        ],
      ),
    );
  }
}
