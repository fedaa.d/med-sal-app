import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/products_booking/model/info_product_model.dart';

class ListInfoProduct extends StatelessWidget {
  const ListInfoProduct({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Text(
          'Products 1',
          style:
              TextStyle(fontSize: AppSize.size22, color: AppColors.blackColor),
        ),
        const SizedBox(
          height: 10,
        ),
        ListView.separated(
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          itemCount: productModelList.length,
          separatorBuilder: (BuildContext context, int index) => const SizedBox(
            height: 10,
          ),
          itemBuilder: (BuildContext context, int index) => Row(
            children: [
              Text(
                productModelList[index].titleInfo,
                style: TextStyle(
                  color: AppColors.mainColor,
                  fontSize: AppSize.size18,
                ),
              ),
              Text(
                productModelList[index].info,
                style: const TextStyle(
                    fontSize: AppSize.size18, color: AppColors.blackColor),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
