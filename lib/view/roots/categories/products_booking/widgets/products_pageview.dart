import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/products_booking/widgets/list_info_container.dart';

class ProductsInfoPageView extends StatelessWidget {
  const ProductsInfoPageView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.only(bottom: 8.0),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(
                left: 18.0,
                right: 18.0,
                bottom: 18.0,
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  ListInfoContainer()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
