import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/products_booking/details/widgets/buy_button.dart';
import 'package:mad_sal/view/roots/categories/products_booking/details/widgets/description_service.dart';
import 'package:mad_sal/view/roots/categories/products_booking/details/widgets/image_details.dart';
import 'package:mad_sal/view/roots/categories/products_booking/details/widgets/list_service_name.dart';
import 'package:mad_sal/view/roots/categories/products_booking/details/widgets/text_services_name.dart';

class DetailsServicesScreen extends StatelessWidget {
  const DetailsServicesScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.whiteColor,
        leading: Icon(
          Icons.arrow_back_ios,
          color: AppColors.darkColor,
          size: 24,
        ),
        title: const Text(
          'Details',
          style: TextStyle(
              color: AppColors.blackColor,
              fontSize: AppSize.size20,
              fontWeight: FontWeight.w600),
        ),
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Stack(
          children: [
            const ImageDetails(),
            Center(
              child: Container(
                margin: const EdgeInsets.only(top: 270, left: 18, right: 18),
                height: 430,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: AppColors.whiteColor,
                    border:
                        Border.all(color: AppColors.secondaryColor, width: 2),
                    borderRadius: BorderRadius.circular(25)),
                child: const Padding(
                  padding: EdgeInsets.only(left: 18.0, right: 18.0, top: 18.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      TextServicesName(),
                      SizedBox(
                        height: 20,
                      ),
                      ListServiceName(),
                      SizedBox(
                        height: 10,
                      ),
                      DescriptionService(),
                      BuyButton()
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
