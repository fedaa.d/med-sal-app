class InfoServicesModel {
  final String titleInfo;
  final String info;

  InfoServicesModel({
    required this.titleInfo,
    required this.info,
  });
}

List<InfoServicesModel> infoServicesModelList = [
  InfoServicesModel(
    titleInfo: 'Time requested: ',
    info: '30min',
  ),
  InfoServicesModel(
    titleInfo: 'Cost: ',
    info: 'Cost 1',
  ),
  InfoServicesModel(
    titleInfo: 'Discount: ',
    info: '10%',
  ),
];
