import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class ImageDetails extends StatelessWidget {
  const ImageDetails({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      AppImages.hospitalRoomImage,
      fit: BoxFit.fill,
      width: double.infinity,
    );
  }
}
