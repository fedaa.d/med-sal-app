import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/products_booking/details/model/info_services.dart';

class ListServiceName extends StatelessWidget {
  const ListServiceName({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      itemBuilder: (BuildContext context, int index) => Row(
        children: [
          Text(
            infoServicesModelList[index].titleInfo,
            style: TextStyle(
              color: AppColors.mainColor,
              fontSize: AppSize.size18,
            ),
          ),
          Text(
            infoServicesModelList[index].info,
            style: const TextStyle(
                fontSize: AppSize.size18, color: AppColors.blackColor),
            maxLines: 6,
            overflow: TextOverflow.ellipsis,
          ),
        ],
      ),
      separatorBuilder: (BuildContext context, int index) => const SizedBox(
        height: 10,
      ),
      itemCount: 3,
      shrinkWrap: true,
    );
  }
}
