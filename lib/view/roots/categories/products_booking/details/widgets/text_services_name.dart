import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class TextServicesName extends StatelessWidget {
  const TextServicesName({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      'Service Name',
      style: TextStyle(
          color: AppColors.secondaryColor,
          fontSize: AppSize.size20,
          fontWeight: FontWeight.w600),
    );
  }
}
