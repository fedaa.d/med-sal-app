import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class BuyButton extends StatelessWidget {
  const BuyButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        width: 230,
        height: 40,
        decoration: ShapeDecoration(
          color: AppColors.secondaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15.0),
          ),
        ),
        child: MaterialButton(
          child: const Text(
            'Buy Now ',
            style: TextStyle(
              color: AppColors.whiteColor,
              fontSize: AppSize.size18,
            ),
          ),
          onPressed: () => Get.toNamed(AppNameRoutes.orderScreen),
        ),
      ),
    );
  }
}
