import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class DescriptionService extends StatelessWidget {
  const DescriptionService({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Description: ',
          style: TextStyle(
            color: AppColors.mainColor,
            fontSize: AppSize.size18,
          ),
        ),
        const Text(
          'Lorem ipsum dolor sit amet consectetur. Eget quis faucibus erat semper tincidunt posuere lorem tellus integer. Sapien in neque metus felis luctus. Lorem purus nibh pharetra nulla amet risus sed at nisi.',
          style:
              TextStyle(fontSize: AppSize.size18, color: AppColors.blackColor),
          maxLines: 6,
          overflow: TextOverflow.ellipsis,
        ),
        const SizedBox(
          height: 25,
        ),
      ],
    );
  }
}
