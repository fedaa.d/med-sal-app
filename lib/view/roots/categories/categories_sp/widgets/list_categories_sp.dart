
import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/categories_sp/model/categories_section_model.dart';

class ListCategoriesSP extends StatelessWidget {
  const ListCategoriesSP({required this.onTap, super.key});

  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
      shrinkWrap: true,
        itemBuilder: (context,index) => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: GestureDetector(
            onTap: onTap,
            child: Container(
                width: 320,
                height: 93,
                decoration: BoxDecoration(
                  border: Border.all(
                    color: AppColors.lightgreyColor,

                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 0.5,
                      offset: const Offset(0, -1),
                    ),
                  ],
                  color: AppColors.cardColor,
                  borderRadius: BorderRadius.circular(25),
                ),
              child: Row(
                children: [
                  Image.asset(
                    categoriesSectionModelList[index].image,
                  ),
                  const SizedBox(
                    width: 35,
                  ),
                  Text(
                    categoriesSectionModelList[index].text,
                    style: const TextStyle(
                      fontSize: AppSize.size25,
                      fontWeight: FontWeight.w400,
                      color: AppColors.blackColor,
                    ),
                  ),
                  const Spacer(),
                  categoriesSectionModelList[index].icon,
                ],
              ),
            ),
          ),
        ),
        separatorBuilder: (context, index) => const SizedBox(
          height: 16,
        ),
        itemCount: categoriesSectionModelList.length);







  }
}





