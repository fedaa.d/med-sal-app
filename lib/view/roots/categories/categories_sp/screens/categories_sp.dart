import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/categories_sp/widgets/list_categories_sp.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class CategoriesSP extends StatelessWidget {
  const CategoriesSP({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: publicAppBar(title: 'Categories',),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(
              height: 32,
            ),
            ListCategoriesSP(onTap: () {
            },),
          ],
        ),
      ),
    );
  }
}

