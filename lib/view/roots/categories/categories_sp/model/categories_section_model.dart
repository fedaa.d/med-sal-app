import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class CategoriesSectionModel {
  final String image;
  final String text;
  final SvgPicture icon;
  CategoriesSectionModel({
    required this.image,
    required this.text,
    required this.icon,
  });
}

List<CategoriesSectionModel> categoriesSectionModelList = [
  CategoriesSectionModel(
    image: AppImages.stethoscopeImage,
    text: 'Doctor',
    icon: SvgPicture.asset(
      AppIcons.editIcon,
      colorFilter:
          const ColorFilter.mode(AppColors.blackColor, BlendMode.srcIn),
    ),
  ),
  CategoriesSectionModel(
    image: AppImages.pharmacyImage,
    text: 'Pharmacy',
    icon: SvgPicture.asset(AppIcons.editIcon,
        colorFilter:
            const ColorFilter.mode(AppColors.blackColor, BlendMode.srcIn)),
  ),
  CategoriesSectionModel(
    image: AppImages.hospitalImage,
    text: 'Hospital',
    icon: SvgPicture.asset(AppIcons.editIcon,
        colorFilter:
            const ColorFilter.mode(AppColors.blackColor, BlendMode.srcIn)),
  ),
  CategoriesSectionModel(
    image: AppImages.labImage,
    text: 'Lab',
    icon: SvgPicture.asset(AppIcons.editIcon,
        colorFilter:
            const ColorFilter.mode(AppColors.blackColor, BlendMode.srcIn)),
  ),
  CategoriesSectionModel(
    image: AppImages.clinicImage,
    text: 'Clinic',
    icon: SvgPicture.asset(AppIcons.editIcon,
        colorFilter:
            const ColorFilter.mode(AppColors.blackColor, BlendMode.srcIn)),
  ),
];
