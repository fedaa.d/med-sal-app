class PermissionsModel {
  final String permissionNum;


  PermissionsModel({
    required this.permissionNum,
});

}


List<PermissionsModel> permissionsModel = [
  PermissionsModel(permissionNum: 'Permissions 1'),
  PermissionsModel(permissionNum: 'Permissions 2'),
  PermissionsModel(permissionNum: 'Permissions 3'),
  PermissionsModel(permissionNum: 'Permissions 4'),
  PermissionsModel(permissionNum: 'Permissions 5'),
  PermissionsModel(permissionNum: 'Permissions 6'),
  PermissionsModel(permissionNum: 'Permissions 7'),
  PermissionsModel(permissionNum: 'Permissions 8'),
  PermissionsModel(permissionNum: 'Permissions 9'),
  PermissionsModel(permissionNum: 'Permissions 10'),
  PermissionsModel(permissionNum: 'Permissions 11'),
  PermissionsModel(permissionNum: 'Permissions 12'),
  PermissionsModel(permissionNum: 'Permissions 13'),
  PermissionsModel(permissionNum: 'Permissions 14'),
  PermissionsModel(permissionNum: 'Permissions 15'),
];