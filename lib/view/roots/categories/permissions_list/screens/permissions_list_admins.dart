import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/permissions_list/widgets/admin_provider_button.dart';
import 'package:mad_sal/view/roots/categories/permissions_list/widgets/list_permissions.dart';
import 'package:mad_sal/view/roots/categories/permissions_list/widgets/main_text.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';
import 'package:mad_sal/view/roots/categories/widgets/secondary_button.dart';

class PermissionsListAdmins extends StatelessWidget {
  const PermissionsListAdmins({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: publicAppBar(title: 'Permissions List'),
      body: const SingleChildScrollView(
        child: Column(
          children: [
            AdminProviderButton(),
            SizedBox(
              height: 27,
            ),
            MainText(text: 'Admins Permissions'),
            SizedBox(
              height: 18,
            ),
            ListPermissions(),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 18, vertical: 60),
              child: SecondaryButton(height: 50, text: 'Save'),
            ),
          ],
        ),
      ),
    );
  }
}
