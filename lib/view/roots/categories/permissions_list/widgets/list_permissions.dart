import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/permissions_list/model/permissions_model.dart';
import 'package:mad_sal/view/roots/categories/permissions_list/widgets/allow_deny_button.dart';

class ListPermissions extends StatelessWidget {
  const ListPermissions({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        itemBuilder: (context, index) => Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 18),
                  child: Row(
                    children: [
                      SvgPicture.asset(
                        AppIcons.blackCircle,
                        width: 7,
                        height: 7,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      Text(
                        permissionsModel[index].permissionNum,
                        style: const TextStyle(
                          fontSize: AppSize.size14,
                          fontWeight: FontWeight.w400,
                          color: AppColors.blackColor,
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Row(
                    children: [
                      AllowDenyButton(
                          textButton: 'Allow',
                          onPressed: () {},
                          colorButton: AppColors.secondaryColor),
                      const SizedBox(
                        width: 34,
                      ),
                      AllowDenyButton(
                        textButton: 'Deny',
                        onPressed: () {},
                        colorButton: AppColors.redHotColor,
                      ),
                    ],
                  ),
                ),
              ],
            ),
        separatorBuilder: (context, index) => const SizedBox(
              height: 16,
            ),
        itemCount: 5);
  }
}
