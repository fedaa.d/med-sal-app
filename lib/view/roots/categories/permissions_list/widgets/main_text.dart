import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class MainText extends StatelessWidget {
  final String text;
  const MainText({super.key, required this.text});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      child: Column(
        children: [
          Text(
            text,
            style: const TextStyle(
              fontSize: AppSize.size18,
              fontWeight: FontWeight.w400,
              color: AppColors.blackColor,
            ),
          ),
          const SizedBox(
            height: 12,
          ),
          Container(
            width: 352,
            height: 2,
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                side: BorderSide(
                  width: 2,
                  color: AppColors.lightBlackColor,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
