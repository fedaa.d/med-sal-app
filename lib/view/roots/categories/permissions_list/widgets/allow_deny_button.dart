import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class AllowDenyButton extends StatelessWidget {
  final String textButton;
  final Function() onPressed;
  final Color colorButton;
  const AllowDenyButton({super.key,
    required this.textButton,
    required this.onPressed,
    required this.colorButton,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        color: colorButton,
      ),
      width: 75,
      height: 35,
      child: MaterialButton(
        onPressed: onPressed,
        child: Text(
            textButton,
          style: const TextStyle(
            fontSize: AppSize.size12,
            fontWeight: FontWeight.w400,
            color: AppColors.whiteColor,
          ),
        ),
      ),
    );
  }
}
