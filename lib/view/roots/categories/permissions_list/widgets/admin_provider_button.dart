import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/permission_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/permissions_list/screens/permissions_list_admins.dart';
import 'package:mad_sal/view/roots/categories/permissions_list/screens/permissions_list_providers.dart';

class AdminProviderButton extends GetView<PermissionControllerImp> {
  const AdminProviderButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(PermissionControllerImp());
    return GetBuilder<PermissionControllerImp>(
      builder: (_) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 61, vertical: 18),
        child: Row(
          children: [
            Container(
              width: 105,
              height: 40,
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
                color: controller.currentIndex == 1
                    ? AppColors.secondaryColor
                    : AppColors.gryColor,
              ),
              child: MaterialButton(
                onPressed: () {
                  controller.change(1);
                  Get.to(PermissionsListAdmins());
                },
                child: const Text(
                  'Admins',
                  style: TextStyle(
                    fontSize: AppSize.size14,
                    fontWeight: FontWeight.w600,
                    color: AppColors.blackColor,
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 58,
            ),
            Container(
              width: 105,
              height: 40,
              decoration: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
                color: controller.currentIndex == 0
                    ? AppColors.secondaryColor
                    : AppColors.gryColor,
              ),
              child: MaterialButton(
                onPressed: () {
                  controller.change(0);
                  Get.to(PermissionsListProviders());
                },
                child: const Text(
                  'Providers',
                  style: TextStyle(
                    fontSize: AppSize.size14,
                    fontWeight: FontWeight.w600,
                    color: AppColors.whiteColor,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
