import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/core/constants/app_theme.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/schedule/model/schedule_model.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/schedule/widgets/title_text.dart';

class ListAppointments extends StatelessWidget {
  const ListAppointments({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TitleText(
                  title: scheduleModelList[index].title,
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(color: AppColors.lightColor),
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            SvgPicture.asset(AppIcons.calendarIcon),
                            const SizedBox(
                              width: 10,
                            ),
                            Text(
                              scheduleModelList[index].appointmentName,
                              style: const TextStyle(
                                  fontSize: AppSize.size16,
                                  fontWeight: FontWeight.w500,
                                  color: AppColors.blackColor),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 4,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 27.0),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                scheduleModelList[index].appointment,
                                style:
                                    AppTheme.themeArabic.textTheme.labelSmall,
                              ),
                              const SizedBox(
                                height: 18,
                              ),
                              Text(
                                scheduleModelList[index].textView,
                                style: TextStyle(
                                    fontSize: AppSize.size14,
                                    color: AppColors.secondaryColor),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
        separatorBuilder: (BuildContext context, int index) => const SizedBox(
              height: 15,
            ),
        itemCount: scheduleModelList.length);
  }
}
