import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class OldAppointment extends StatelessWidget {
  const OldAppointment({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.center,
          width: 160,
          height: 37,
          decoration: BoxDecoration(
              border: Border.all(color: AppColors.lightColor),
              borderRadius: BorderRadius.circular(10),
              color: AppColors.lightgreyColor),
          child: Text(
            'Old Appiontment',
            style: TextStyle(
                fontSize: AppSize.size14, color: AppColors.secondaryColor),
          ),
        ),
        const SizedBox(
          height: 6,
        ),
      ],
    );
  }
}
