import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class TodayAppointmentContainer extends StatelessWidget {
  const TodayAppointmentContainer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: AppColors.lightColor),
          borderRadius: BorderRadius.circular(15),
          color: AppColors.lightHerbalColor),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                SvgPicture.asset(AppIcons.calendarIcon),
                const SizedBox(
                  width: 10,
                ),
                const Text(
                  'Up coming appiontment',
                  style: TextStyle(
                      fontSize: AppSize.size16,
                      fontWeight: FontWeight.w500,
                      color: AppColors.blackColor),
                ),
              ],
            ),
            const SizedBox(
              height: 4,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 27.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Doctor appointment at 9:00 Am Today',
                    style: AppTheme.themeArabic.textTheme.labelSmall,
                  ),
                  const SizedBox(
                    height: 18,
                  ),
                  Text(
                    'View',
                    style: TextStyle(
                        fontSize: AppSize.size14,
                        color: AppColors.secondaryColor),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
