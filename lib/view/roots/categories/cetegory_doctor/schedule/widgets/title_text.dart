import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class TitleText extends StatelessWidget {
  const TitleText({
    Key? key,
    required this.title,
  }) : super(key: key);
  final String title;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 10,
        ),
        Text(
          title,
          style: AppTheme.themeArabic.textTheme.labelSmall,
        ),
        const SizedBox(
          height: 7,
        ),
      ],
    );
  }
}
