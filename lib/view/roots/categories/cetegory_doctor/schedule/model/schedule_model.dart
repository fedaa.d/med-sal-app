class ScheduleModel {
  final String title;
  final String appointmentName;
  final String appointment;
  final String textView;
  ScheduleModel({
    required this.title,
    required this.appointmentName,
    required this.appointment,
    required this.textView,
  });
}

List<ScheduleModel> scheduleModelList = [
  ScheduleModel(
      title: 'Tomorow',
      appointmentName: 'Up coming appiontment',
      appointment: 'Doctor appointment at 9:00 Am Today',
      textView: 'View'),
  ScheduleModel(
      title: 'This month',
      appointmentName: 'Up coming appiontment',
      appointment: 'Doctor appointment at 9:00 Am Today',
      textView: 'View'),
  ScheduleModel(
      title: 'Next appointment',
      appointmentName: 'Up coming appiontment',
      appointment: 'Doctor appointment at 9:00 Am Today',
      textView: 'View'),
];
