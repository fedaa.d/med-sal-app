import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/schedule/widgets/list_appointments.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/schedule/widgets/old_appointment.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/schedule/widgets/title_text.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/schedule/widgets/today_container.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class MyScheduleScreen extends StatelessWidget {
  const MyScheduleScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: publicAppBar(title: 'My Schedule'),
      body: const SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.all(18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                OldAppointment(),
                TitleText(
                  title: 'Today',
                ),
                TodayAppointmentContainer(),
                ListAppointments()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
