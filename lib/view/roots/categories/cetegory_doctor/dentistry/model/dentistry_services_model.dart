import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class DentistryServicesModel {
  final String image;
  final String title;
  final SvgPicture icon;
  DentistryServicesModel({
    required this.image,
    required this.title,
    required this.icon,
  });
}

List<DentistryServicesModel> dentistryServicesModelList = [
  DentistryServicesModel(
    image: AppImages.teethImage,
    title: 'Whitening Teeth',
    icon: SvgPicture.asset(AppIcons.arrowDownIcon),
  ),
  DentistryServicesModel(
    image: AppImages.teethImage,
    title: 'Whitening Teeth',
    icon: SvgPicture.asset(AppIcons.arrowDownIcon),
  ),
  DentistryServicesModel(
    image: AppImages.teethImage,
    title: 'Whitening Teeth',
    icon: SvgPicture.asset(AppIcons.arrowDownIcon),
  ),
];
