import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/dentistry_controller.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/widgets/dentistry_indicator.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/widgets/dentistry_page_view.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/widgets/main_text_dentistry.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/widgets/search_text_filed.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/widgets/text_services_dentistry.dart';

class DentistryMinBox extends GetView<DentistryControllerImp> {
  const DentistryMinBox({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(DentistryControllerImp());
    return Padding(
      padding: const EdgeInsets.only(
        left: 18.0,
        right: 18.0,
        top: 18.0,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SearchTextFiled(
            hintText: 'Search',
            height: 45,
          ),
          const MainTextDentistry(),
          const TextServicesDentistry(),
          const DentistryPageView(),
          DentistryIndicator(
            index: controller.currentIndex,
          ),
          const SizedBox(
            height: 10,
          ),
        ],
      ),
    );
  }
}
