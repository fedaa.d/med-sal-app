import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/dentistry_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

class DentistryIndicator extends GetView<DentistryControllerImp> {
  const DentistryIndicator({
    Key? key,
    required this.index,
  }) : super(key: key);
  final int index;
  @override
  Widget build(BuildContext context) {
    Get.put(DentistryControllerImp());
    return GetBuilder<DentistryControllerImp>(
      builder: (_) => Center(
        child: SizedBox(
          width: 60,
          height: 10,
          child: ListView.separated(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) => Container(
                  width: 10,
                  height: 10,
                  decoration: BoxDecoration(
                      color: controller.currentIndex == index
                          ? AppColors.secondaryColor
                          : AppColors.lightColor,
                      shape: BoxShape.circle)),
              separatorBuilder: (BuildContext context, int index) =>
                  const SizedBox(
                    width: 10,
                  ),
              itemCount: 3),
        ),
      ),
    );
  }
}
