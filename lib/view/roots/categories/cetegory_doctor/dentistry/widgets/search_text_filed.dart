import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class SearchTextFiled extends StatelessWidget {
  final double? height;
  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final bool? enabled;
  final String hintText;
  const SearchTextFiled(
      {Key? key,
      required this.hintText,
      this.prefixIcon,
      this.enabled,
      this.suffixIcon,
      this.height})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.whiteColor,
      child: Container(
        alignment: Alignment.center,
        width: double.infinity,
        height: height,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: AppColors.lightColor, width: 0)),
        child: TextField(
          enabled: enabled,
          style: const TextStyle(
              fontWeight: FontWeight.w400,
              color: AppColors.blackColor,
              fontSize: AppSize.size16),
          textAlignVertical: TextAlignVertical.bottom,
          decoration: InputDecoration(
            suffixIcon: suffixIcon,
            prefixIcon: prefixIcon,
            disabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: AppColors.transparentColor),
                borderRadius: BorderRadius.circular(10)),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: const BorderSide(color: AppColors.transparentColor),
            ),
            enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: AppColors.transparentColor),
                borderRadius: BorderRadius.circular(10)),
            hintText: hintText,
            hintStyle: TextStyle(
              fontSize: AppSize.size16,
              fontWeight: FontWeight.w400,
              color: AppColors.lightWhiteColor,
            ),
          ),
        ),
      ),
    );
  }
}
