import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class MainTextDentistry extends StatelessWidget {
  const MainTextDentistry({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 7,
        ),
        Text(
          'Lorem ipsum dolor sit amet consectetur. Eu enim lectus vitae urna.',
          style: AppTheme.themeArabic.textTheme.displaySmall,
        ),
        const SizedBox(
          height: 20,
        ),
      ],
    );
  }
}
