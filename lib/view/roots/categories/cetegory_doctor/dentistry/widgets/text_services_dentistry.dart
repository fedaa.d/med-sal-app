import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class TextServicesDentistry extends StatelessWidget {
  const TextServicesDentistry({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'Services',
          style: AppTheme.themeArabic.textTheme.displayMedium,
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
