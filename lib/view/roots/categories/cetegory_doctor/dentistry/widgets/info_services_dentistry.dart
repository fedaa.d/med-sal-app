import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_theme.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/model/dentistry_services_model.dart';

class InfoServicesDentistry extends StatelessWidget {
  const InfoServicesDentistry({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        physics: const ScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) => GestureDetector(
              onTap: () => Get.toNamed(AppNameRoutes.availableDoctorsScreen),
              child: Container(
                height: 88,
                decoration: BoxDecoration(
                    border: Border.all(color: AppColors.lightColor),
                    borderRadius: BorderRadius.circular(15)),
                child: Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.asset(dentistryServicesModelList[index].image),
                        Text(
                          dentistryServicesModelList[index].title,
                          style: AppTheme.themeArabic.textTheme.displayMedium,
                        ),
                        dentistryServicesModelList[index].icon
                      ]),
                ),
              ),
            ),
        separatorBuilder: (BuildContext context, int index) => const SizedBox(
              height: 20,
            ),
        itemCount: dentistryServicesModelList.length);
  }
}
