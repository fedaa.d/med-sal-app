import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/dentistry_controller.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/widgets/info_services_dentistry.dart';

class DentistryPageView extends GetView<DentistryControllerImp> {
  const DentistryPageView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(DentistryControllerImp());
    return Expanded(
      child: GetBuilder<DentistryControllerImp>(
        builder: (_) => PageView(
          controller: PageController(initialPage: 0),
          onPageChanged: (int val) => controller.change(val),
          children: [
            ...List.generate(
              3,
              (index) => const InfoServicesDentistry(),
            )
          ],
        ),
      ),
    );
  }
}
