import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class ImageDentistry extends StatelessWidget {
  const ImageDentistry({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      AppImages.dentistImage,
      fit: BoxFit.fill,
      width: double.infinity,
    );
  }
}
