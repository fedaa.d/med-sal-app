import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/widgets/dentistry_main_box.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/widgets/image_dentistry.dart';

class DentistryScreen extends StatelessWidget {
  const DentistryScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return Container(
        color: AppColors.whiteColor,
        child: Stack(
          children: [
            constraints.maxWidth > 700 ? Container() : const ImageDentistry(),
            Container(
              margin: EdgeInsets.only(
                top: constraints.maxWidth > 700 ? 18 : 267,
                right: constraints.maxWidth > 700 ? 18 : 0,
                left: constraints.maxWidth > 700 ? 18 : 0,
                bottom: constraints.maxWidth > 700 ? 18 : 0,
              ),
              width: double.infinity,
              decoration: BoxDecoration(
                  color: AppColors.whiteColor,
                  border: Border.all(color: AppColors.secondaryColor, width: 2),
                  borderRadius: BorderRadius.circular(25)),
              child: const DentistryMinBox(),
            ),
          ],
        ),
      );
    }));
  }
}
