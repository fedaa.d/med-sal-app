import 'package:mad_sal/core/constants/app_images.dart';

class CategoriesDoctorModel {
  final String image;
  final String title;
  CategoriesDoctorModel({
    required this.image,
    required this.title,
  });
}

List<CategoriesDoctorModel> categoryDoctorModelList = [
  CategoriesDoctorModel(image: AppImages.dentistryImage, title: 'Dentistry'),
  CategoriesDoctorModel(image: AppImages.eyeglassesImage, title: 'Optics'),
  CategoriesDoctorModel(image: AppImages.nutritionImage, title: 'Nutritionist'),
  CategoriesDoctorModel(image: AppImages.laboratoryImage, title: 'Radiologist'),
  CategoriesDoctorModel(image: AppImages.laboratoryImage, title: 'Home care'),
  CategoriesDoctorModel(image: AppImages.selfCareImage, title: 'Aesthetics'),
];
