import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class ImageAvailableDoctors extends StatelessWidget {
  const ImageAvailableDoctors({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      AppImages.whiteningTeethImage,
      fit: BoxFit.fill,
      width: double.infinity,
    );
  }
}
