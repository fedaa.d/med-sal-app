import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class TextAvailableDoctors extends StatelessWidget {
  const TextAvailableDoctors({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          'Available Doctors',
          style: AppTheme.themeArabic.textTheme.headlineMedium,
        ),
        Text(
          'See All',
          style: AppTheme.themeArabic.textTheme.labelSmall,
        ),
      ],
    );
  }
}
