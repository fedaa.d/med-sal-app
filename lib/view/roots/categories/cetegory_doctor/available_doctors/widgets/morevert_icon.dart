import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';

class MoreVertIcon extends StatelessWidget {
  const MoreVertIcon({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.whiteColor,
      child: IconButton(
        padding: const EdgeInsets.only(bottom: 18, left: 19),
        icon: SvgPicture.asset(AppIcons.moreVertIcon),
        onPressed: () {},
      ),
    );
  }
}
