import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class BookContainer extends StatelessWidget {
  const BookContainer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 22.0, left: 30),
      child: GestureDetector(
        onTap: () => Get.toNamed(AppNameRoutes.doctorBookScreen),
        child: Container(
          alignment: Alignment.center,
          height: 34,
          width: 80,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              color: AppColors.secondaryColor),
          child: Text(
            'book',
            style: AppTheme.themeArabic.textTheme.bodySmall,
          ),
        ),
      ),
    );
  }
}
