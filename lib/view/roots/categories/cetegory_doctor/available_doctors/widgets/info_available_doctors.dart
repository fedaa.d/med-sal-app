import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_theme.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/model/available_doctors_model.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/widgets/book_container.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/widgets/morevert_icon.dart';

class InfoAvailableDoctors extends StatelessWidget {
  const InfoAvailableDoctors({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        physics: const ScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) => Container(
              height: 80,
              decoration: BoxDecoration(
                  border: Border.all(color: AppColors.lightColor),
                  borderRadius: BorderRadius.circular(15)),
              child: Padding(
                padding: const EdgeInsets.only(
                    top: 8.0, bottom: 8.0, left: 15.0, right: 7),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Image.asset(
                      availablDoctorseModelList[index].image,
                    ),
                    const SizedBox(
                      width: 18,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          availablDoctorseModelList[index].title,
                          style: AppTheme.themeArabic.textTheme.headlineMedium,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Text(
                          availablDoctorseModelList[index].subTitle,
                          style: AppTheme.themeArabic.textTheme.headlineSmall,
                        ),
                      ],
                    ),
                    const BookContainer(),
                    const MoreVertIcon()
                  ],
                ),
              ),
            ),
        separatorBuilder: (BuildContext context, int index) => const SizedBox(
              height: 15,
            ),
        itemCount: availablDoctorseModelList.length);
  }
}
