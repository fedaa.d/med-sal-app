import 'package:mad_sal/core/constants/app_images.dart';

class AvailablDoctorseModel {
  final String image;
  final String title;
  final String subTitle;

  AvailablDoctorseModel({
    required this.image,
    required this.title,
    required this.subTitle,
  });
}

List<AvailablDoctorseModel> availablDoctorseModelList = [
  AvailablDoctorseModel(
      image: AppImages.personImage, title: 'Dr. Lana', subTitle: 'Dentist'),
  AvailablDoctorseModel(
      image: AppImages.personImage, title: 'Dr. Lana', subTitle: 'Dentist'),
  AvailablDoctorseModel(
      image: AppImages.personImage, title: 'Dr. Lana', subTitle: 'Dentist'),
  AvailablDoctorseModel(
      image: AppImages.personImage, title: 'Dr. Lana', subTitle: 'Dentist'),
  AvailablDoctorseModel(
      image: AppImages.personImage, title: 'Dr. Lana', subTitle: 'Dentist'),
];
