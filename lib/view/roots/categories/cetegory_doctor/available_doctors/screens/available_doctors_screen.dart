import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_theme.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/widgets/image_available_coctors.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/widgets/info_available_doctors.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/widgets/text_available_doctors.dart';

class AvailableDoctorsScreen extends StatelessWidget {
  const AvailableDoctorsScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(child: LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        return Container(
          color: AppColors.whiteColor,
          child: SingleChildScrollView(
              child: Stack(
            children: [
              constraints.maxWidth > 700
                  ? Container()
                  : const ImageAvailableDoctors(),
              Positioned(
                top: constraints.maxWidth > 700 ? 18 : 230,
                left: 20,
                child: Text(
                  'Whitening Teeth',
                  style: AppTheme.themeArabic.textTheme.titleMedium,
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: constraints.maxWidth > 700 ? 70 : 290,
                  right: constraints.maxWidth > 700 ? 18 : 18,
                  left: constraints.maxWidth > 700 ? 18 : 18,
                  bottom: constraints.maxWidth > 700 ? 18 : 0,
                ),
                width: double.infinity,
                decoration: BoxDecoration(
                    color: AppColors.whiteColor,
                    border:
                        Border.all(color: AppColors.secondaryColor, width: 2),
                    borderRadius: BorderRadius.circular(25)),
                child: const Padding(
                  padding: EdgeInsets.only(left: 18.0, right: 18.0, top: 18.0),
                  child: Column(
                    children: [
                      TextAvailableDoctors(),
                      SizedBox(
                        height: 18,
                      ),
                      InfoAvailableDoctors()
                    ],
                  ),
                ),
              ),
            ],
          )),
        );
      },
    ));
  }
}
