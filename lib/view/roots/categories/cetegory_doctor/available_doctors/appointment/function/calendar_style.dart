import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:table_calendar/table_calendar.dart';

CalendarStyle calendarStyle(TextStyle textStyle) {
  return CalendarStyle(
      selectedDecoration: BoxDecoration(
          color: AppColors.lightTorquoiseColor, shape: BoxShape.circle),
      todayDecoration: BoxDecoration(
          color: AppColors.lightTorquoiseColor.withOpacity(.2),
          shape: BoxShape.circle),
      defaultTextStyle: textStyle,
      rangeStartTextStyle: textStyle,
      rangeEndTextStyle: textStyle,
      weekNumberTextStyle: textStyle,
      outsideTextStyle: textStyle,
      disabledTextStyle: textStyle,
      withinRangeTextStyle: textStyle,
      weekendTextStyle: textStyle,
      selectedTextStyle: const TextStyle(
          fontWeight: FontWeight.w500, color: AppColors.whiteColor));
}
