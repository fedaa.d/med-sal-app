import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_icons.dart';

class AppointmentServicesModel {
  final String nameServices;
  final Widget trailing;
  AppointmentServicesModel({
    required this.nameServices,
    required this.trailing,
  });
}

List<AppointmentServicesModel> appointmentServicesModelList = [
  AppointmentServicesModel(
    nameServices: 'Service 1',
    trailing: SvgPicture.asset(AppIcons.arrowDownIcon),
  ),
  AppointmentServicesModel(
    nameServices: 'Service 1',
    trailing: SvgPicture.asset(AppIcons.arrowDownIcon),
  ),
  AppointmentServicesModel(
      nameServices: 'Service 1',
      trailing: SvgPicture.asset(AppIcons.arrowDownIcon)),
];
