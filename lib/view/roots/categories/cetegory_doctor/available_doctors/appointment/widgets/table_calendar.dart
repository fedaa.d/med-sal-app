import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/appointment_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_theme.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/appointment/function/calendar_style.dart';
import 'package:table_calendar/table_calendar.dart';

class ContainerTableCalendar extends GetView<AppointmentControllerImp> {
  const ContainerTableCalendar({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(AppointmentControllerImp());
    return GetBuilder<AppointmentControllerImp>(
      builder: (controller) => Container(
        height: 450,
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                blurRadius: 3,
                offset: const Offset(0, 2),
              ),
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 1,
                blurRadius: 3,
                offset: const Offset(0, 2),
              ),
            ],
            color: AppColors.cardColor,
            border: Border.all(
              color: AppColors.secondaryColor,
            ),
            borderRadius: const BorderRadius.only(
                bottomLeft: Radius.circular(45),
                bottomRight: Radius.circular(45))),
        child: TableCalendar(
          daysOfWeekHeight: 43,
          daysOfWeekStyle: DaysOfWeekStyle(
              weekendStyle: AppTheme.themeArabic.textTheme.headlineLarge ??
                  const TextStyle(),
              weekdayStyle: AppTheme.themeArabic.textTheme.headlineLarge ??
                  const TextStyle()),
          headerStyle: HeaderStyle(
            formatButtonVisible: false,
            titleTextStyle: AppTheme.themeArabic.textTheme.headlineLarge ??
                const TextStyle(),
          ),
          calendarStyle: calendarStyle(
            AppTheme.themeArabic.textTheme.headlineLarge ?? const TextStyle(),
          ),
          focusedDay: controller.focus,
          firstDay: DateTime(2000),
          lastDay: DateTime(2026),
          calendarFormat: controller.calendarFormat,
          onDaySelected: (selectedDay, focusedDay) {
            if (!isSameDay(controller.selected, selectedDay)) {
              controller.onDaySelected(selectedDay, focusedDay);
            }
          },
          selectedDayPredicate: (day) {
            return isSameDay(controller.selected, day);
          },
          onFormatChanged: (format) {
            if (controller.calendarFormat != format) {
              controller.onFormatChanged(format);
            }
          },
          onPageChanged: (focusedDay) => controller.focus = focusedDay,
        ),
      ),
    );
  }
}
