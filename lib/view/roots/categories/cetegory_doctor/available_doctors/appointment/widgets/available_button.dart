import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class AvailavleButton extends StatelessWidget {
  const AvailavleButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 20,
        ),
        Container(
          width: 174,
          height: 61,
          decoration: ShapeDecoration(
            color: AppColors.lightTorquoiseColor,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(45.0),
            ),
          ),
          child: MaterialButton(
            onPressed: () {},
            child: const Text(
              'Available Time',
              style: TextStyle(
                color: AppColors.whiteColor,
                fontSize: AppSize.size18,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 17,
        ),
      ],
    );
  }
}
