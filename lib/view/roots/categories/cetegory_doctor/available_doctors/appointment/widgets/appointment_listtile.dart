import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_theme.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/appointment/model/appointment_services_model.dart';

class AppointmentListTile extends StatelessWidget {
  const AppointmentListTile({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) => GestureDetector(
              onTap: () => Get.toNamed(AppNameRoutes.serviceAppointmentScreen),
              child: Container(
                height: 65,
                decoration: BoxDecoration(
                    border: Border.all(color: AppColors.secondaryColor),
                    borderRadius: BorderRadius.circular(10)),
                child: ListTile(
                  title: Text(
                    appointmentServicesModelList[index].nameServices,
                    style: AppTheme.themeArabic.textTheme.displayMedium,
                  ),
                  trailing: appointmentServicesModelList[index].trailing,
                ),
              ),
            ),
        separatorBuilder: (BuildContext context, int index) => const SizedBox(
              height: 17,
            ),
        itemCount: appointmentServicesModelList.length);
  }
}
