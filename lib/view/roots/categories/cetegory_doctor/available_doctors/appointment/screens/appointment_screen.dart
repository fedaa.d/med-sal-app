import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/appointment/widgets/table_calendar.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/appointment/widgets/appointment_listtile.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/available_doctors/appointment/widgets/available_button.dart';

import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class DoctorAppointmentScreen extends StatelessWidget {
  const DoctorAppointmentScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.whiteColor,
        appBar: publicAppBar(title: 'Appointment'),
        body: const SafeArea(
          child: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.all(15.0),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ContainerTableCalendar(),
                    AvailavleButton(),
                    AppointmentListTile()
                  ]),
            ),
          ),
        ));
  }
}
