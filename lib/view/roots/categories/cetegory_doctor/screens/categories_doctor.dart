import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/widgets/categories_list_doctor.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class CategoriesDoctorScreen extends StatelessWidget {
  const CategoriesDoctorScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: publicAppBar(title: 'Doctors'),
      body: const SafeArea(
          child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(18.0),
          child: Column(
            children: [
              SizedBox(
                height: 18,
              ),
              CategoriesListDoctor()
            ],
          ),
        ),
      )),
    );
  }
}
