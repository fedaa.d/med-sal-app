import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/model/category_doctor_model.dart';

class CategoriesListDoctor extends StatelessWidget {
  const CategoriesListDoctor({super.key});
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        physics: const ScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (context, index) => GestureDetector(
              onTap: () => Get.toNamed(AppNameRoutes.dentistryScreen),
              child: Container(
                height: 93,
                width: double.infinity,
                decoration: BoxDecoration(
                  border: Border.all(color: AppColors.secondaryColor),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 2,
                      blurRadius: 0.5,
                      offset: const Offset(0, 1),
                    ),
                  ],
                  color: AppColors.cardColor,
                  borderRadius: BorderRadius.circular(25),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 50.0),
                      child: Text(
                        categoryDoctorModelList[index].title,
                        style: const TextStyle(
                            fontSize: AppSize.size25,
                            color: AppColors.blackColor),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 50.0),
                      child: Image.asset(
                        categoryDoctorModelList[index].image,
                      ),
                    )
                  ],
                ),
              ),
            ),
        separatorBuilder: ((context, index) => const SizedBox(
              height: 20,
            )),
        itemCount: categoryDoctorModelList.length);
  }
}
