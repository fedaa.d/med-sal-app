import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/add_services_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/shared/custom_text_field.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/add_services/widgets/confirm_button.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/add_services/widgets/question_discount.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class AddServicesNoteScreen extends GetView<AddServicesControllerImp> {
  const AddServicesNoteScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(AddServicesControllerImp());
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: publicAppBar(title: 'ADD Service'),
      body: SingleChildScrollView(
        child: SafeArea(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomTextFelid(
                label: 'Enter Name of Service',
                hint: 'Pleas Enter Name of Service',
                icon: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: SvgPicture.asset(
                    AppIcons.editIcon,
                  ),
                ),
                obscure: false,
                controller: controller.nameServicesController),
            CustomTextFelid(
                label: 'Enter Name of Category',
                hint: 'Pleas Enter Name of Category',
                icon: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: SvgPicture.asset(
                    AppIcons.editIcon,
                  ),
                ),
                obscure: false,
                controller: controller.nameCategoryController),
            CustomTextFelid(
                label: 'Enter Cost',
                hint: 'Pleas Enter Cost',
                icon: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: SvgPicture.asset(
                    AppIcons.editIcon,
                  ),
                ),
                obscure: false,
                controller: controller.costController),
            const QuestionDiscount(),
            CustomTextFelid(
                label: 'Enter Discount',
                hint: 'Pleas Enter Discount %',
                icon: Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: SvgPicture.asset(
                    AppIcons.editIcon,
                  ),
                ),
                obscure: false,
                controller: controller.discountController),
            const ConfirmButton()
          ],
        )),
      ),
    );
  }
}
