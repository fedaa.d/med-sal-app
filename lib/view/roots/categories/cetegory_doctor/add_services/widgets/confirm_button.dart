import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/add_services/widgets/add_services_container.dart';

class ConfirmButton extends StatelessWidget {
  const ConfirmButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: AddServicesContainer(
        text: 'Confirm',
        color: AppColors.whiteColor,
        backgroundClr: AppColors.lightTorquoiseColor,
        width: double.infinity,
        height: 55,
        circular: 10,
      ),
    );
  }
}
