import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/add_services/widgets/add_services_container.dart';

class QuestionDiscount extends StatelessWidget {
  const QuestionDiscount({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            'Have a Discount?',
            style: TextStyle(
                fontSize: AppSize.size16, color: AppColors.blackColor),
          ),
          const SizedBox(
            height: 11,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 18.0),
            child: Row(
              children: [
                AddServicesContainer(
                  text: 'Yes',
                  color: AppColors.whiteColor,
                  backgroundClr: AppColors.lightTorquoiseColor,
                  circular: 50,
                  width: 60,
                  height: 60,
                ),
                const SizedBox(
                  width: 20,
                ),
                AddServicesContainer(
                  text: 'No',
                  color: AppColors.blackColor,
                  backgroundClr: AppColors.lightColor,
                  circular: 50,
                  width: 60,
                  height: 60,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
