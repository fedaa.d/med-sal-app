import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class AddServicesContainer extends StatelessWidget {
  const AddServicesContainer(
      {Key? key,
      required this.text,
      required this.color,
      required this.backgroundClr,
      this.width,
      this.height,
      this.circular})
      : super(key: key);
  final String text;
  final Color color;
  final Color backgroundClr;

  final double? width;
  final double? height;
  final double? circular;
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      decoration: BoxDecoration(
          color: backgroundClr, borderRadius: BorderRadius.circular(circular!)),
      child: MaterialButton(
        onPressed: () {},
        child: Text(
          text,
          style: TextStyle(
            color: color,
            fontSize: AppSize.size14,
          ),
        ),
      ),
    );
  }
}
