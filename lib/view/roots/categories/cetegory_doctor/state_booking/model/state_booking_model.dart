import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';

class StateBookingModel {
  final String title;
  final Function() onTap;
  StateBookingModel({
    required this.title,
    required this.onTap,
  });
}

List<StateBookingModel> stateBookingModelList = [
  StateBookingModel(
    title: 'Services',
    onTap: () => Get.toNamed(AppNameRoutes.editingServicesScreen),
  ),
  StateBookingModel(
    title: 'Products',
    onTap: () => Get.toNamed(AppNameRoutes.editingProductsScreen),
  ),
  StateBookingModel(
    title: 'Appointment',
    onTap: () => Get.toNamed(AppNameRoutes.doctorAppointmentScreen),
  )
];
