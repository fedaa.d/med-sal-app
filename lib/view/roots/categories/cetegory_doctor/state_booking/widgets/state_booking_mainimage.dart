import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class MainImage extends StatelessWidget {
  const MainImage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      AppImages.stateBookingImage,
      width: double.infinity,
      fit: BoxFit.fill,
    );
  }
}
