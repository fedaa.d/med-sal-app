import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/state_booking/model/state_booking_model.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/state_booking/widgets/state_booking_button.dart';

class StateBookingList extends StatelessWidget {
  const StateBookingList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        physics: const ScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) => StateBookingButton(
              onTap: () => stateBookingModelList[index].onTap(),
              text: stateBookingModelList[index].title,
            ),
        separatorBuilder: (BuildContext context, int index) => const SizedBox(
              height: 30,
            ),
        itemCount: stateBookingModelList.length);
  }
}
