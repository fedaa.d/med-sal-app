// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';

import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class StateBookingButton extends StatelessWidget {
  const StateBookingButton({
    Key? key,
    required this.text,
    required this.onTap,
  }) : super(key: key);
  final String text;
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 67,
      decoration: ShapeDecoration(
        color: AppColors.secondaryColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
      ),
      child: MaterialButton(
        onPressed: onTap,
        child: Text(
          text,
          style: const TextStyle(
            color: AppColors.whiteColor,
            fontSize: AppSize.size22,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }
}
