import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class StateBookingMainText extends StatelessWidget {
  const StateBookingMainText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Welcome',
          style: TextStyle(
              fontSize: AppSize.size24,
              color: AppColors.blackColor,
              fontWeight: FontWeight.w600),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          'Lorem ipsum dolor sit amet consectetur. Ac id iaculis tristique proin egestas magna id.',
          style: TextStyle(
            fontSize: AppSize.size16,
            color: AppColors.blackColor,
          ),
        ),
        SizedBox(
          height: 35,
        ),
      ],
    );
  }
}
