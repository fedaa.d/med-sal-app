import 'package:flutter/material.dart';

import 'package:mad_sal/view/roots/categories/cetegory_doctor/state_booking/widgets/state_booking_mainimage.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/state_booking/widgets/state_booking_maintext.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/state_booking/widgets/state_booking_list.dart';

class StateBookingScreen extends StatelessWidget {
  const StateBookingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Material(
        child: LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
          return Stack(
            children: [
              constraints.maxWidth > 700 ? Container() : const MainImage(),
              const Padding(
                padding: EdgeInsets.all(18.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [StateBookingMainText(), StateBookingList()],
                ),
              )
            ],
          );
        }),
      ),
    );
  }
}
