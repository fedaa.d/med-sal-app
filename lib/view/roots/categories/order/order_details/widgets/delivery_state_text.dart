import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class DeliveryStateText extends StatelessWidget {
  const DeliveryStateText({super.key});

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Delivery State',
      style: TextStyle(
        fontSize: AppSize.size16,
        fontWeight: FontWeight.w400,
        color: AppColors.gryColor,
      ),
    );
  }
}
