import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class DeliveryArriveDetails extends StatelessWidget {
  const DeliveryArriveDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding:  EdgeInsets.symmetric(horizontal: 59),
      child: Text(
        'Products will arrive after two hours',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: AppSize.size16,
          color: AppColors.blackColor,
        ),
      ),
    );
  }
}
