import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/model/order_price.dart';

class ListOrderPrice extends StatelessWidget {
  const ListOrderPrice({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        itemBuilder: (context, index) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18),
              child: Row(
                children: [
                  Text(
                    orderPrice[index].title,
                    style: const TextStyle(
                      fontSize: AppSize.size16,
                      fontWeight: FontWeight.w400,
                      color: AppColors.gryColor,
                    ),
                  ),
                  Text(
                    orderPrice[index].subtitle,
                    style: const TextStyle(
                      fontSize: AppSize.size16,
                      fontWeight: FontWeight.w400,
                      color: AppColors.blackColor,
                    ),
                  ),
                ],
              ),
            ),
        separatorBuilder: (context, index) => const SizedBox(
              height: 10,
            ),
        itemCount: 3);
  }
}
