import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_images.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class PaymentMethod extends StatelessWidget {
  const PaymentMethod({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 32,
          ),
          const Text(
            'Payment Method',
            style: TextStyle(
              fontSize: AppSize.size16,
              fontWeight: FontWeight.w400,
              color: AppColors.gryColor,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            width: 352,
            height: 93,
            decoration: ShapeDecoration(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
                side: BorderSide(
                  color: AppColors.lightgreyColor,
                  width: 1,
                ),
              ),
            ),
            child: const Image(
              image: AssetImage(
                AppImages.paymentImage,
              ),
              fit: BoxFit.fill,
            ),
          ),
        ],
      ),
    );
  }
}
