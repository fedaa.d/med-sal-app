import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class AddressNameText extends StatelessWidget {
  const AddressNameText({super.key});

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Address Name',
      style: TextStyle(
        fontSize: AppSize.size16,
        fontWeight: FontWeight.w600,
        color: AppColors.blackColor,
      ),
    );
  }
}
