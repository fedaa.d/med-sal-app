import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_images.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/address_details_text.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/address_name_text.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/shipping_address_text.dart';

class AddressSection extends StatelessWidget {
  const AddressSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 16,
          ),
          const ShippingAddressText(),
          const SizedBox(
            height: 8,
          ),
          Stack(
            alignment: Alignment.topLeft,
            children: [
              Image.asset(
                AppImages.addressImage,
              ),
              const Padding(
                padding: EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 13,
                    ),
                    AddressNameText(),
                    SizedBox(
                      height: 10,
                    ),
                    AddressDetailsText(),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
