import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class DeliveryLinearIndicator extends StatelessWidget {
  const DeliveryLinearIndicator({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        LinearPercentIndicator(
          width: 353,
          progressColor: AppColors.secondaryColor,
          percent: 0.7,
          lineHeight: 21.0,
          backgroundColor: AppColors.redColor,
        ),
        const Row(
          children: [
            Text(
              '15:00',
              style: TextStyle(
                fontSize: AppSize.size16,
                color: AppColors.blackColor,
              ),
            ),
            SizedBox(
              width: 275,
            ),
            Text(
              '13:00',
              style: TextStyle(
                fontSize: AppSize.size16,
                color: AppColors.blackColor,
              ),
            ),
          ],
        ),
      ],
    );
  }
}
