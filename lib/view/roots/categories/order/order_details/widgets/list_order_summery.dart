import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/model/order_summery.dart';

class ListOrderSummery extends StatelessWidget {
  const ListOrderSummery({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Image.asset(
                  orderSummery[index].image,
                  width: 170,
                  height: 100,
                ),
                const SizedBox(
                  height: 5,
                ),
                Text(
                  orderSummery[index].product,
                  style: const TextStyle(
                    fontSize: AppSize.size14,
                    fontWeight: FontWeight.w400,
                    color: AppColors.blackColor,
                  ),
                ),
                Text(
                  orderSummery[index].cost,
                  style: const TextStyle(
                    fontSize: AppSize.size14,
                    fontWeight: FontWeight.w400,
                    color: AppColors.blackColor,
                  ),
                ),
              ],
            ),
        separatorBuilder: (context, index) => const SizedBox(
              width: 16,
            ),
        itemCount: 2);
  }
}
