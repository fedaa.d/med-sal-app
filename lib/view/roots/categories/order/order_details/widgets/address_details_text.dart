import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class AddressDetailsText extends StatelessWidget {
  const AddressDetailsText({super.key});

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Lorem ipsum dolor sit amet consectetur.',
      style: TextStyle(
        fontSize: AppSize.size16,
        fontWeight: FontWeight.w400,
        color: AppColors.blackColor,
      ),
    );
  }
}
