import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/list_order_summery.dart';

class OrderSummerySection extends StatelessWidget {
  const OrderSummerySection({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 19),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 32,
          ),
          const Text(
            'Order Summery',
            style: TextStyle(
              fontSize: AppSize.size16,
              fontWeight: FontWeight.w400,
              color: AppColors.gryColor,
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
              width: double.infinity,
              height: 150,
              child: const ListOrderSummery()),
        ],
      ),
    );
  }
}
