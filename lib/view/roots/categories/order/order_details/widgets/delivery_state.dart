import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/shared/custom_button.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/delivery_arrive_details_text.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/delivery_details_text.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/delivery_linear_indicator.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/delivery_state_text.dart';
import 'package:mad_sal/view/roots/categories/order/success/screens/order_success_screen.dart';

class DeliveryState extends StatelessWidget {
  const DeliveryState({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(
            height: 32,
          ),
          const DeliveryStateText(),
          const SizedBox(
            height: 10,
          ),
          const DeliveryDetailsText(),
          const SizedBox(
            height: 10,
          ),
          const DeliveryLinearIndicator(),
          const SizedBox(
            height: 32,
          ),
          const DeliveryArriveDetails(),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 39),
            child: CustomButton(
                text: 'Confirm',
                onPressed: () {
                  Get.to(const OrderSuccessScreen());
                },
                width: 353,
                height: 50),
          ),
        ],
      ),
    );
  }
}
