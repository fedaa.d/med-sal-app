import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class DeliveryDetailsText extends StatelessWidget {
  const DeliveryDetailsText({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10),
      width: 353,
      height: 50,
      decoration: ShapeDecoration(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12),
          side:const BorderSide(
            width: 1,
            color: AppColors.gryColor,
          ),
        ),
      ),
      child: const Text(
        'Products will arrive at 15:00 PM by DHL',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: AppSize.size16,
          color: AppColors.blackColor,
        ),
      ),
    );
  }
}
