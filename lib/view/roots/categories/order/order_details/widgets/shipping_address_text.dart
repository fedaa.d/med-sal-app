import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class ShippingAddressText extends StatelessWidget {
  const ShippingAddressText({super.key});

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Shipping Address',
      style: TextStyle(
        fontSize: AppSize.size16,
        fontWeight: FontWeight.w400,
        color: AppColors.gryColor,
      ),
    );
  }
}
