import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/address_section.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/delivery_state.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/list_order_price.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/order_summery_section.dart';
import 'package:mad_sal/view/roots/categories/order/order_details/widgets/payment_method.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class OrderDetails extends StatelessWidget {
  const OrderDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: publicAppBar(
        title: 'Order Details',
      ),
      body: const SingleChildScrollView(
        child: Column(
          children: [
            AddressSection(),
            OrderSummerySection(),
            SizedBox(
              height: 16,
            ),
            ListOrderPrice(),
            PaymentMethod(),
            DeliveryState(),
          ],
        ),
      ),
    );
  }
}
