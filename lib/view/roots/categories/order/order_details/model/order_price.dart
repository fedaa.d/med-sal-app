class OrderPrice {
  final String title;
  final String subtitle;

  OrderPrice({
    required this.title,
    required this.subtitle,
});
}

List<OrderPrice> orderPrice = [
 OrderPrice(title: 'Price:', subtitle: '59\$'),
 OrderPrice(title: 'Discount:', subtitle: '12%'),
 OrderPrice(title: 'Total Price:', subtitle: '39\$'),
];