import 'package:mad_sal/core/constants/app_images.dart';

class OrderSummery {
  final String image;
  final String product;
  final String cost;

  OrderSummery({
    required this.image,
    required this.product,
    required this.cost,
});
}


List<OrderSummery> orderSummery = [
  OrderSummery(image: '${AppImages.root}Rectangle 186.png', product: 'Product 1', cost: '12\$'),
  OrderSummery(image: '${AppImages.root}Rectangle 187.png', product: 'Product 2', cost: '47\$'),
];