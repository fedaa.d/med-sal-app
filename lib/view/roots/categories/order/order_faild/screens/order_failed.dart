import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_images.dart';
import 'package:mad_sal/view/roots/categories/order/order_faild/widgets/text_section.dart';
import 'package:mad_sal/view/roots/categories/order/order_faild/widgets/view_details_txtbt.dart';
import 'package:mad_sal/view/roots/categories/widgets/secondary_button.dart';

class OrderFailedScreen extends StatelessWidget {
  const OrderFailedScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const TextSection(),
          const SizedBox(
            height: 38,
          ),
          Image.asset(
            AppImages.groupFailedImage,
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 18),
            child: SecondaryButton(height: 65, text: 'Try Again',
            ),
          ),
          const SizedBox(
            height: 50,
          ),
          const ViewDetails(),


        ],
      ),
    );
  }
}
