import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class ViewDetails extends StatelessWidget {
  const ViewDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextButton(
        onPressed: () => Get.toNamed(AppNameRoutes.orderDetailsScreen),
        child: Text(
          'View Details',
          style: TextStyle(
            fontSize: AppSize.size20,
            fontWeight: FontWeight.w400,
            color: AppColors.mainColor,
          ),
        ),
      ),
    );
  }
}
