import 'package:flutter/cupertino.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class TextSection extends StatelessWidget {
  const TextSection({super.key});

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 93),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              height: 86,
            ),
            Text(
              'Oooooops',
              style: TextStyle(
                fontSize: AppSize.size30,
                fontWeight: FontWeight.w500,
                color: AppColors.blackColor,
              ),
            ),
            SizedBox(
              height: 11,
            ),
            Text(
              'Sorry, Your order has refused',
              style: TextStyle(
                fontSize: AppSize.size16,
                fontWeight: FontWeight.w400,
                color: AppColors.blackColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
