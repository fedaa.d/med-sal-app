import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_images.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/view/roots/categories/order/success/widgets/secondary_text.dart';
import 'package:mad_sal/view/roots/categories/order/success/widgets/success_text.dart';
import 'package:mad_sal/view/roots/categories/order/success/widgets/view_details_text.dart';
import 'package:mad_sal/view/roots/categories/widgets/secondary_button.dart';

class OrderSuccessScreen extends StatelessWidget {
  const OrderSuccessScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Material(
        color: AppColors.whiteColor,
        child: SafeArea(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const SuccessText(),
            const SecondaryText(),
            Image.asset(
              AppImages.successImage,
            ),
            Padding(
              padding: const EdgeInsets.all(18.0),
              child: GestureDetector(
                onTap: () => Get.offAllNamed(AppNameRoutes.rootScreen),
                child: const SecondaryButton(
                  text: 'Back To Home',
                  height: 69,
                ),
              ),
            ),
            const ViewDetailsText(),
          ],
        )));
  }
}








// class SuccessScreen extends StatelessWidget {
//   const SuccessScreen({super.key});

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         backgroundColor: AppColors.whiteColor,
//         appBar: AppBar(
//           centerTitle: true,
//           title: const Text(
//             'Success',
//             style: TextStyle(
//                 fontSize: AppSize.size25,
//                 color: AppColors.blackColor,
//                 fontWeight: FontWeight.w400),
//           ),
//           toolbarHeight: 121,
//           elevation: 0,
//           backgroundColor: AppColors.lightgryColor,
//         ),
//         body: SafeArea(
//             child: Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               Image.asset(AppImages.groupSuccessImage),
//               const WelcomeText(),
//             ],
//           ),
//         )));
//   }
// }
