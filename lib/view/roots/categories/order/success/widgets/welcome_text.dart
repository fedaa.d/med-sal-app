import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_theme.dart';

class WelcomeText extends StatelessWidget {
  const WelcomeText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Text(
      'You are Welcome ito Med-sal',
      style: AppTheme.themeArabic.textTheme.displayMedium,
    );
  }
}
