import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class ViewDetailsText extends StatelessWidget {
  const ViewDetailsText({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.toNamed(AppNameRoutes.orderDetailsScreen),
      child: Text(
        'View Details',
        style: TextStyle(
            color: AppColors.mainColor,
            fontSize: AppSize.size20,
            fontWeight: FontWeight.w500),
      ),
    );
  }
}
