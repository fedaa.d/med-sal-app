import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class SuccessText extends StatelessWidget {
  const SuccessText({super.key});

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Success',
      style: TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size30,
          fontWeight: FontWeight.w500),
    );
  }
}
