import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class SecondaryText extends StatelessWidget {
  const SecondaryText({super.key});

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Yay, It’s Nice Order, It Will Arrive On Time',
      style: TextStyle(
          color: AppColors.blackColor,
          fontSize: AppSize.size16,
          fontWeight: FontWeight.w400),
    );
  }
}
