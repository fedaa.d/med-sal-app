import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class Maping extends StatelessWidget {
  const Maping({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
      return constraints.maxWidth > 700
          ? Container()
          : Column(
              children: [
                SizedBox(
                  child: Image.asset(
                    AppImages.mapImage,
                    fit: BoxFit.fill,
                    width: double.infinity,
                  ),
                ),
                const SizedBox(
                  height: 10,
                )
              ],
            );
    });
  }
}
