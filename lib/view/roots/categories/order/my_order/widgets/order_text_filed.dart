import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class OrderTextFiled extends StatelessWidget {
  const OrderTextFiled(
      {Key? key,
      required this.titleTextFiled,
      required this.hint,
      required this.controller,
      this.width,
      this.icon})
      : super(key: key);
  final String titleTextFiled;
  final String hint;
  final double? width;
  final Widget? icon;
  final TextEditingController controller;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          titleTextFiled,
          style: TextStyle(
              fontSize: AppSize.size14, color: AppColors.darkGryColor),
        ),
        Container(
          margin: const EdgeInsets.only(top: 5),
          height: 45,
          width: width,
          child: TextFormField(
            style: const TextStyle(
                fontWeight: FontWeight.w400,
                color: AppColors.blackColor,
                fontSize: AppSize.size16),
            controller: controller,
            decoration: InputDecoration(
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15.0),
                borderSide:
                    BorderSide(width: 1.0, color: AppColors.lightWhiteColor),
              ),
              enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: AppColors.lightWhiteColor),
                  borderRadius: BorderRadius.circular(15)),
              hintText: hint,
              hintStyle: TextStyle(
                fontSize: AppSize.size14,
                fontWeight: FontWeight.w400,
                color: AppColors.darkColor,
              ),
              suffixIcon: icon,
              suffixIconColor: AppColors.lightWhiteColor,
            ),
          ),
        ),
        const SizedBox(
          height: 15,
        ),
      ],
    );
  }
}
