import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class OrderText extends StatelessWidget {
  const OrderText({
    Key? key,
    required this.text,
  }) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          text,
          style: TextStyle(
              fontSize: AppSize.size14, color: AppColors.darkGryColor),
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}
