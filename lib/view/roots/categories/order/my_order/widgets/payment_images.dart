import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class PaymentImages extends StatelessWidget {
  const PaymentImages({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ...imagesPayList.map((image) => Image.asset(image)).toList()
          ],
        ),
        const SizedBox(
          height: 10,
        ),
      ],
    );
  }
}

List<String> imagesPayList = [
  AppImages.pay1Image,
  AppImages.pay2Image,
  AppImages.pay3Image,
];
