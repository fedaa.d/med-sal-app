import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/my_order_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/view/roots/categories/order/my_order/widgets/maping.dart';
import 'package:mad_sal/view/roots/categories/order/my_order/widgets/order_text.dart';
import 'package:mad_sal/view/roots/categories/order/my_order/widgets/order_text_filed.dart';
import 'package:mad_sal/view/roots/categories/order/my_order/widgets/payment_images.dart';
import 'package:mad_sal/view/roots/categories/widgets/confirm_cancel_button.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class MyOrderScreen extends GetView<MyOrderControllerImp> {
  const MyOrderScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(MyOrderControllerImp());
    return Scaffold(
      appBar: publicAppBar(title: 'My Order'),
      backgroundColor: AppColors.whiteColor,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(18.0),
            child: Form(
              key: controller.formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const OrderText(
                    text: 'Pleas Determine Place of Delivery',
                  ),
                  const Maping(),
                  const OrderText(
                    text: 'Pleas Determine Place of Delivery',
                  ),
                  const PaymentImages(),
                  const OrderText(
                    text: 'Enter your information',
                  ),
                  OrderTextFiled(
                    controller: controller.cardNumberController,
                    titleTextFiled: 'Card number',
                    hint: '975412863.015633',
                  ),
                  OrderTextFiled(
                    controller: controller.nameCardController,
                    titleTextFiled: 'Name on card',
                    hint: 'Ali Ahmad',
                  ),
                  LayoutBuilder(
                    builder:
                        (BuildContext context, BoxConstraints constraints) {
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          OrderTextFiled(
                            controller: controller.expDateController,
                            width: constraints.maxWidth > 700 ? 600 : 125,
                            titleTextFiled: 'Exp date',
                            hint: '12/11/2025',
                          ),
                          OrderTextFiled(
                            controller: controller.cvvController,
                            width: constraints.maxWidth > 700 ? 600 : 125,
                            titleTextFiled: 'CVV/ CVC',
                            hint: '7894856',
                          ),
                        ],
                      );
                    },
                  ),
                  ConfirmCancelButton(
                      height: 55,
                      text: 'Confirm',
                      buttonColor: AppColors.secondaryColor,
                      onPressed: () =>
                          Get.offNamed(AppNameRoutes.orderSccessScreen)),
                  ConfirmCancelButton(
                      height: 55,
                      text: 'Cancel',
                      buttonColor: AppColors.redHotColor,
                      onPressed: () =>
                          Get.offNamed(AppNameRoutes.orderFiledScreen)),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
