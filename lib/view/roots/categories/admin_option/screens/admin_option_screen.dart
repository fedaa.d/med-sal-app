import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/admin_option/widgets/account_text.dart';
import 'package:mad_sal/view/roots/categories/admin_option/widgets/list_options.dart';
import 'package:mad_sal/view/roots/categories/admin_option/widgets/logout_button.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';

class AdminOptionScreen extends StatelessWidget {
  const AdminOptionScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: publicAppBar(title: 'Options'),
      body: const SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 26,
            ),
            AccountText(),
            SizedBox(
              height: 24,
            ),
            ListOptions(),
            SizedBox(
              height: 24,
            ),
            LogoutButton(),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }
}
