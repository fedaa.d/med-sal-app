import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';

class Options {
  final String option;
  final Function() onTap;
  Options({
    required this.option,
    required this.onTap,
  });
}

List<Options> options = [
  Options(
      option: 'Users Management',
      onTap: () => Get.toNamed(AppNameRoutes.permissionListScreen)),
  Options(
      option: 'Registration Management',
      onTap: () => Get.toNamed(AppNameRoutes.spRequestRigistScreen)),
  Options(
      option: 'Services Management',
      onTap: () => Get.toNamed(AppNameRoutes.servicesAdminScreen)),
  Options(
      option: 'Products Management',
      onTap: () => Get.toNamed(AppNameRoutes.productsAdminScreen)),
  Options(
      option: 'Sales Report',
      onTap: () => Get.toNamed(AppNameRoutes.salesScreen)),
  Options(
      option: 'Appointments Management',
      onTap: () => Get.toNamed(AppNameRoutes.doctorAppointmentScreen)),
  Options(
      option: 'Delivery Report',
      onTap: () => Get.toNamed(AppNameRoutes.deliveryReportScreen)),
];
