import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/categories/admin_option/model/options.dart';

class ListOptions extends StatelessWidget {
  const ListOptions({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        itemBuilder: (context, index) => Padding(
              padding: const EdgeInsets.symmetric(horizontal: 18),
              child: GestureDetector(
                onTap: () => options[index].onTap(),
                child: Container(
                  padding: EdgeInsets.symmetric(vertical: 21, horizontal: 10),
                  width: 354,
                  height: 80,
                  decoration: ShapeDecoration(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(12),
                      side: BorderSide(
                        color: AppColors.lightWhiteColor,
                        width: 1,
                      ),
                    ),
                    color: AppColors.lightgreyColor,
                  ),
                  child: Text(
                    options[index].option,
                    style: TextStyle(
                      fontSize: AppSize.size20,
                      fontWeight: FontWeight.w400,
                      color: AppColors.blackColor,
                    ),
                  ),
                ),
              ),
            ),
        separatorBuilder: (context, index) => SizedBox(
              height: 16,
            ),
        itemCount: 7);
  }
}
