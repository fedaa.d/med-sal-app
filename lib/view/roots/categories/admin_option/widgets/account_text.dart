
import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class AccountText extends StatelessWidget {
  const AccountText({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Text(
        'Account',
        style: TextStyle(
          fontSize: AppSize.size24,
          fontWeight: FontWeight.w600,
          color: AppColors.mainColor,
        ),
      ),
    );
  }
}
