import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/auth/login/screens/login_screen.dart';

class LogoutButton extends StatelessWidget {
  const LogoutButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 18),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 101),
        width: 354,
        height: 68,
        decoration: ShapeDecoration(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          color: AppColors.mainColor,
        ),
        child: InkWell(
          onTap: (){
            Get.offAll(LoginScreen());
          },
          child: Row(
            children: [
             SvgPicture.asset(
               AppIcons.loghOutIcon,
             ),
              SizedBox(
                width: 26,
              ),
              Text(
                'Logout',
                style: TextStyle(
                  fontSize: AppSize.size22,
                  fontWeight: FontWeight.w500,
                  color: AppColors.whiteColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
