import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/services_admin/model/services_admin_model.dart';
import 'package:mad_sal/view/roots/categories/services_admin/widgets/info_text.dart';

class InfoServicesAdmin extends StatelessWidget {
  const InfoServicesAdmin({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        physics: const ScrollPhysics(),
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) => Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                InfoText(
                  text: servicesAdminModelList[index].id,
                ),
                InfoText(
                  text: servicesAdminModelList[index].name,
                ),
                InfoText(
                  text: servicesAdminModelList[index].providerName,
                ),
                InfoText(
                  text: servicesAdminModelList[index].cost,
                ),
                InfoText(
                  text: servicesAdminModelList[index].date,
                ),
              ],
            ),
        separatorBuilder: (context, index) => const Divider(
              thickness: 2,
            ),
        itemCount: 10);
  }
}
