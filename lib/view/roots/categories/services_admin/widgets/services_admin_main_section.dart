import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/services_admin_controller.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';
import 'package:mad_sal/view/roots/categories/services_admin/widgets/services_admin_pageview.dart';
import 'package:mad_sal/view/roots/categories/services_admin/widgets/services_admin_titletable.dart';

class MainServicesAdmin extends GetView<ServicesAdminControllerImp> {
  const MainServicesAdmin({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(ServicesAdminControllerImp());
    return Padding(
      padding: const EdgeInsets.all(18.0),
      child: Column(
        children: [
          const ServicesAdminTitleTableList(),
          const ServicesAdminPageView(),
          const SizedBox(
            height: 25,
          ),
          GetBuilder<ServicesAdminControllerImp>(
              builder: (_) => Indicator(
                    index: controller.currentIndex,
                  ))
        ],
      ),
    );
  }
}
