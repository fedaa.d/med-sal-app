import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/services_admin/widgets/services_admin_table_text.dart';

class ServicesAdminTitleTableList extends StatelessWidget {
  const ServicesAdminTitleTableList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          height: 55,
          width: double.infinity,
          decoration: BoxDecoration(
              color: AppColors.secondaryColor,
              borderRadius: BorderRadius.circular(10)),
          child: const Padding(
            padding: EdgeInsets.only(left: 20.0, right: 27.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ServicesAdminTableTextLabel(
                  title: 'ID',
                ),
                ServicesAdminTableTextLabel(
                  title: 'Name',
                ),
                ServicesAdminTableTextLabel(
                  title: 'Provider \n Name',
                ),
                ServicesAdminTableTextLabel(
                  title: 'Cost',
                ),
                ServicesAdminTableTextLabel(
                  title: 'Date',
                ),
              ],
            ),
          ),
        ),
        const SizedBox(
          height: 20,
        )
      ],
    );
  }
}
