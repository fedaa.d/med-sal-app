import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/services_admin_controller.dart';
import 'package:mad_sal/view/roots/categories/services_admin/widgets/info_services_admin.dart';

class ServicesAdminPageView extends GetView<ServicesAdminControllerImp> {
  const ServicesAdminPageView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(ServicesAdminControllerImp());
    return SizedBox(
      height: 500,
      child: GetBuilder<ServicesAdminControllerImp>(
        builder: (_) => PageView(
          controller: PageController(initialPage: 0),
          onPageChanged: (int val) => controller.change(val),
          children: [
            ...List.generate(
              9,
              (index) => const InfoServicesAdmin(),
            )
          ],
        ),
      ),
    );
  }
}
