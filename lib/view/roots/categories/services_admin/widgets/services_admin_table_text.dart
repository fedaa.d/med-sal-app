import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class ServicesAdminTableTextLabel extends StatelessWidget {
  const ServicesAdminTableTextLabel({super.key, required this.title});
  final String title;
  @override
  Widget build(BuildContext context) {
    return Text(title,
        style: const TextStyle(
            fontSize: AppSize.size12,
            fontWeight: FontWeight.w500,
            color: AppColors.blackColor));
  }
}
