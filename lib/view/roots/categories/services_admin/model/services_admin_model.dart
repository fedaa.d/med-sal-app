class ServicesAdminModel {
  final String id;
  final String name;
  final String providerName;
  final String cost;
  final String date;
  ServicesAdminModel({
    required this.id,
    required this.name,
    required this.providerName,
    required this.cost,
    required this.date,
  });
}

List<ServicesAdminModel> servicesAdminModelList = [
  ServicesAdminModel(
      id: '123',
      name: 'Cleaning Teeth',
      providerName: 'Provider 1',
      cost: r'12$',
      date: '15 /11/ 20213 \n 10:30 Am'),
  ServicesAdminModel(
      id: '123',
      name: 'Cleaning Teeth',
      providerName: 'Provider 2',
      cost: r'12$',
      date: '15 /11/ 20213 \n 10:30 Am'),
  ServicesAdminModel(
      id: '123',
      name: 'Cleaning Teeth',
      providerName: 'Provider 3',
      cost: r'12$',
      date: '15 /11/ 20213 \n 10:30 Am'),
  ServicesAdminModel(
      id: '123',
      name: 'Cleaning Teeth',
      providerName: 'Provider 4',
      cost: r'12$',
      date: '15 /11/ 20213 \n 10:30 Am'),
  ServicesAdminModel(
      id: '123',
      name: 'Cleaning Teeth',
      providerName: 'Provider 5',
      cost: r'12$',
      date: '15 /11/ 20213 \n 10:30 Am'),
  ServicesAdminModel(
      id: '123',
      name: 'Cleaning Teeth',
      providerName: 'Provider 6',
      cost: r'12$',
      date: '15 /11/ 20213 \n 10:30 Am'),
  ServicesAdminModel(
      id: '123',
      name: 'Cleaning Teeth',
      providerName: 'Provider 7',
      cost: r'12$',
      date: '15 /11/ 20213 \n 10:30 Am'),
  ServicesAdminModel(
      id: '123',
      name: 'Cleaning Teeth',
      providerName: 'Provider 8',
      cost: r'12$',
      date: '15 /11/ 20213 \n 10:30 Am'),
  ServicesAdminModel(
      id: '123',
      name: 'Cleaning Teeth',
      providerName: 'Provider 9',
      cost: r'12$',
      date: '15 /11/ 20213 \n 10:30 Am'),
  ServicesAdminModel(
      id: '123',
      name: 'Cleaning Teeth',
      providerName: 'Provider 10',
      cost: r'12$',
      date: '15 /11/ 20213 \n 10:30 Am'),
  ServicesAdminModel(
      id: '123',
      name: 'Cleaning Teeth',
      providerName: 'Provider 11',
      cost: r'12$',
      date: '15 /11/ 20213 \n 10:30 Am'),
];
