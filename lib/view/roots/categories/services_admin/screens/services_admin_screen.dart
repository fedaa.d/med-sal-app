import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/categories/delivery_state/widgets/delivery_searh_file.dart';
import 'package:mad_sal/view/roots/categories/services_admin/widgets/services_admin_main_section.dart';
import 'package:mad_sal/view/roots/categories/widgets/custom_appbar.dart';

class ServicesAdminScreen extends StatelessWidget {
  const ServicesAdminScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      appBar: customAppBar(title: 'Services Management'),
      body: const SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: [DeliverySearchTextFiled(), MainServicesAdmin()],
        ),
      )),
    );
  }
}
