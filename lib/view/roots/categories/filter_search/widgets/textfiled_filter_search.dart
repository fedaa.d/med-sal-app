import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/widgets/search_text_filed.dart';

class TextFiledFilterSearch extends StatelessWidget {
  const TextFiledFilterSearch({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 40,
        ),
        Padding(
          padding: const EdgeInsets.all(18.0),
          child: SearchTextFiled(
            hintText: 'Search',
            suffixIcon: Padding(
              padding: const EdgeInsets.only(right: 35.0),
              child: SvgPicture.asset(AppIcons.searchIcon),
            ),
          ),
        ),
      ],
    );
  }
}
