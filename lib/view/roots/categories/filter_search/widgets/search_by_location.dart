import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class SearchByLocation extends StatelessWidget {
  const SearchByLocation({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 18.0, right: 18.0),
      child: Container(
        alignment: Alignment.center,
        width: double.infinity,
        height: 58,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
            border: Border.all(color: AppColors.lightColor, width: 0)),
        child: TextField(
          style: const TextStyle(
              fontWeight: FontWeight.w400,
              color: AppColors.blackColor,
              fontSize: AppSize.size16),
          textAlignVertical: TextAlignVertical.center,
          decoration: InputDecoration(
            suffixIconConstraints:
                const BoxConstraints(maxHeight: 30, maxWidth: 95),
            suffixIcon: Padding(
              padding: const EdgeInsets.only(right: 18.0),
              child: Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    color: AppColors.lightColor,
                    border: Border.all(color: AppColors.gryTextColor),
                    borderRadius: BorderRadius.circular(15)),
                child: Text(
                  'Search',
                  style: TextStyle(
                      color: AppColors.darkGryColor, fontSize: AppSize.size12),
                ),
              ),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10.0),
              borderSide: const BorderSide(color: AppColors.transparentColor),
            ),
            enabledBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: AppColors.transparentColor),
                borderRadius: BorderRadius.circular(10)),
            hintText: 'Enter your Location',
            hintStyle: TextStyle(
              fontSize: AppSize.size16,
              fontWeight: FontWeight.w400,
              color: AppColors.lightWhiteColor,
            ),
          ),
        ),
      ),
    );
  }
}
