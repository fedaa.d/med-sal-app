import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/filter_search_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

List<String> doctorsNameList = [
  'Dentistry',
  'Optical',
  'Home care',
];

class FilterByDoctorsList extends GetView<FilterSearchControllerImp> {
  const FilterByDoctorsList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    Get.put(FilterSearchControllerImp());
    return Padding(
      padding: const EdgeInsets.only(left: 18.0, top: 6.0),
      child: GetBuilder<FilterSearchControllerImp>(
        builder: (_) => Container(
          height: 62,
          child: ListView.separated(
              scrollDirection: Axis.horizontal,
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) => GestureDetector(
                    onTap: () => controller.selectedDoctorsFilter(index),
                    child: Container(
                      alignment: Alignment.center,
                      height: 62,
                      decoration: BoxDecoration(
                          color: AppColors.lightGryColor,
                          border: Border.all(
                              color: controller.selectedIndex == index
                                  ? AppColors.secondaryColor
                                  : AppColors.lightWhiteColor),
                          borderRadius: BorderRadius.circular(10)),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 17.0, right: 17.0),
                        child: Text(
                          doctorsNameList[index],
                          style: const TextStyle(
                              fontSize: AppSize.size16,
                              color: AppColors.blackColor,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                  ),
              separatorBuilder: (BuildContext context, int index) =>
                  const SizedBox(
                    width: 15,
                  ),
              itemCount: doctorsNameList.length),
        ),
      ),
    );
  }
}
