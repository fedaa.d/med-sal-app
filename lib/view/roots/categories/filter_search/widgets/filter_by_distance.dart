import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class FilterByDistancse extends StatelessWidget {
  const FilterByDistancse({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          left: 18.0, right: 18.0, top: 10.0, bottom: 18.0),
      child: Image.asset(
        AppImages.mapingImage,
        fit: BoxFit.fill,
        width: double.infinity,
      ),
    );
  }
}
