import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class TextFilterBy extends StatelessWidget {
  const TextFilterBy({
    Key? key,
    required this.text,
  }) : super(key: key);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          top: 26.0, left: 18.0, right: 18.0, bottom: 10.0),
      child: Text(
        text,
        style:
            TextStyle(fontSize: AppSize.size16, color: AppColors.gryTextColor),
      ),
    );
  }
}
