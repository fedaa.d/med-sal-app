import 'package:flutter/material.dart';
import 'package:mad_sal/view/roots/categories/filter_search/widgets/filter_by_categories.dart';
import 'package:mad_sal/view/roots/categories/filter_search/widgets/filter_by_distance.dart';
import 'package:mad_sal/view/roots/categories/filter_search/widgets/filter_by_doctors_list.dart';
import 'package:mad_sal/view/roots/categories/filter_search/widgets/search_by_location.dart';
import 'package:mad_sal/view/roots/categories/filter_search/widgets/text_filter_by.dart';
import 'package:mad_sal/view/roots/categories/filter_search/widgets/textfiled_filter_search.dart';

class FilterSearchScreen extends StatelessWidget {
  const FilterSearchScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Material(
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const TextFiledFilterSearch(),
            const TextFilterBy(
              text: 'Filter by Category',
            ),
            const FilterByCategoriesList(),
            const TextFilterBy(
              text: 'Filter by Doctors',
            ),
            const FilterByDoctorsList(),
            const TextFilterBy(
              text: 'Filter by Location',
            ),
            const SearchByLocation(),
            const TextFilterBy(
              text: 'Filter by Distance',
            ),
            LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
              return constraints.maxWidth > 700
                  ? Container()
                  : const FilterByDistancse();
            })
          ],
        ),
      ),
    ));
  }
}
