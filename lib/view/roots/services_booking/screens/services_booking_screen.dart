import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/services_booking_controller.dart';
import 'package:mad_sal/view/roots/categories/sales/widgets/indicator_list.dart';
import 'package:mad_sal/view/roots/categories/widgets/public_appbar.dart';
import 'package:mad_sal/view/roots/services_booking/widgets/main_text.dart';
import 'package:mad_sal/view/roots/services_booking/widgets/services_booking_pageview.dart';
import 'package:mad_sal/view/roots/services_booking/widgets/under_line_text.dart';

class ServicesBookingScreen extends GetView<ServicesBookingControllerImp> {
  const ServicesBookingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(ServicesBookingControllerImp());
    return Scaffold(
      appBar: publicAppBar(
        title: 'Services booking',
      ),
      body: Padding(
        padding: const EdgeInsets.only(
            top: 15.0, left: 15.0, right: 15.0, bottom: 10.0),
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const MainTextServices(),
              const UnderLineMainText(),
              const ServicesBookingPageView(),
              const SizedBox(
                height: 20,
              ),
              GetBuilder<ServicesBookingControllerImp>(
                builder: (_) => Indicator(
                  index: controller.currentIndex,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
