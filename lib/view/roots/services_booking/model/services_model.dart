class ServicesModel {
  final String titleInfo;
  final String info;
  ServicesModel({
    required this.titleInfo,
    required this.info,
  });
}

List<ServicesModel> servicesModelList = [
  ServicesModel(
    titleInfo: 'Services name ',
    info: 'Teeth whitening',
  ),
  ServicesModel(
    titleInfo: 'Doctor name ',
    info: 'Name1',
  ),
  ServicesModel(
    titleInfo: 'Descreption ',
    info: 'Easy and fast',
  ),
  ServicesModel(titleInfo: 'Source ', info: 'Clinic1'),
  ServicesModel(titleInfo: 'Cost ', info: r'12$'),
  ServicesModel(titleInfo: 'Date & Time ', info: 'Wed,30Oct,10-10:3Am'),
];
