import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/view/roots/categories/services_booking/widgets/list_info_services.dart';

class ListServicesDetails extends StatelessWidget {
  const ListServicesDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        physics: const ScrollPhysics(),
        itemBuilder: (BuildContext context, int index) => Column(
              children: [
                GestureDetector(
                  onTap: () =>
                      Get.toNamed(AppNameRoutes.detailsServicesScreen),
                  child: Container(
                    margin: const EdgeInsets.only(
                        top: 18.0, left: 5, right: 5, bottom: 5),
                    height: 267,
                    width: double.infinity,
                    decoration: BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey.withOpacity(0.5),
                            spreadRadius: 2,
                            blurRadius: 0.5,
                            offset: const Offset(0, 1),
                          ),
                        ],
                        color: AppColors.cardColor,
                        border: Border.all(
                          color: AppColors.secondaryColor,
                        ),
                        borderRadius: BorderRadius.circular(25)),
                    child: const Padding(
                      padding: EdgeInsets.all(18.0),
                      child: ListInfoServices(),
                    ),
                  ),
                ),
              ],
            ),
        separatorBuilder: ((BuildContext context, int index) =>
            const SizedBox()),
        itemCount: 2);
  }
}
