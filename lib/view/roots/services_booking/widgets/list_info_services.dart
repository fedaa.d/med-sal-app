import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/services_booking/model/services_model.dart';

class ListInfoServices extends StatelessWidget {
  const ListInfoServices({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          'Service 1',
          style:
              TextStyle(fontSize: AppSize.size22, color: AppColors.torquoise),
        ),
        const SizedBox(
          height: 10,
        ),
        ListView.separated(
          shrinkWrap: true,
          itemCount: servicesModelList.length,
          separatorBuilder: (BuildContext context, int index) => const SizedBox(
            height: 10,
          ),
          itemBuilder: (BuildContext context, int index) => Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                servicesModelList[index].titleInfo,
                style: TextStyle(
                  color: AppColors.mainColor,
                  fontSize: AppSize.size18,
                ),
              ),
              Text(
                servicesModelList[index].info,
                style: const TextStyle(
                    fontSize: AppSize.size18, color: AppColors.blackColor),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
