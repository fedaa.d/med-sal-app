import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/services_booking_controller.dart';
import 'package:mad_sal/view/roots/services_booking/widgets/list_services_details.dart';

class ServicesBookingPageView extends GetView<ServicesBookingControllerImp> {
  const ServicesBookingPageView({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GetBuilder<ServicesBookingControllerImp>(
        builder: (_) => PageView(
          controller: PageController(initialPage: 0),
          onPageChanged: (int val) => controller.change(val),
          children: [
            ...List.generate(9, (index) => const ListServicesDetails())
          ],
        ),
      ),
    );
  }
}
