import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class MainTextServices extends StatelessWidget {
  const MainTextServices({super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      'Services',
      style: TextStyle(
        fontSize: AppSize.size30,
        fontWeight: FontWeight.w600,
        color: AppColors.secondaryColor,
      ),
    );
  }
}
