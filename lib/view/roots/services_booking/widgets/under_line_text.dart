import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

class UnderLineMainText extends StatelessWidget {
  const UnderLineMainText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 16,
        ),
        Container(
            width: 144,
            height: 6,
            decoration: BoxDecoration(
              color: AppColors.reqColor,
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 2,
                  blurRadius: 0.5,
                  offset: const Offset(0, 1),
                ),
              ],
            )),
        const SizedBox(
          height: 32,
        ),
      ],
    );
  }
}
