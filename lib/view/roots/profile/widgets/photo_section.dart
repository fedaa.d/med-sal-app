import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_images.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class PhotoSection extends StatelessWidget {
  const PhotoSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return Row(
              children: [
                Padding(
                    padding: const EdgeInsets.only(left: 22.0),
                    child: GestureDetector(
                        onTap: () {},
                        child: SvgPicture.asset(
                          AppIcons.settingsIcon,
                          width: 30,
                        ))),
                const SizedBox(
                  width: 100,
                ),
                constraints.maxWidth > 450
                    ? Container()
                    : const Text(
                        'My Profile',
                        style: TextStyle(
                            fontSize: AppSize.size22,
                            fontWeight: FontWeight.w600,
                            color: AppColors.blackColor),
                      )
              ],
            );
          },
        ),
        const SizedBox(
          height: 20,
        ),
        Image.asset(AppImages.profileImage)
      ],
    );
  }
}
