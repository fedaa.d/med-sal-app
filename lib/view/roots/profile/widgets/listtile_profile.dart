import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/profile/model/profile_model.dart';

class ListTileProfile extends StatelessWidget {
  const ListTileProfile({required this.onTap, super.key});
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        shrinkWrap: true,
        itemBuilder: (context, index) => ListTile(
              minLeadingWidth: 7,
              onTap: onTap,
              title: Padding(
                padding: const EdgeInsets.only(top: 11.0),
                child: Text(
                  infoProfileModelList[index].title,
                  style: TextStyle(
                      fontSize: AppSize.size16,
                      fontWeight: FontWeight.normal,
                      color: AppColors.lightBlackColor),
                ),
              ),
              subtitle: Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Text(
                  infoProfileModelList[index].subTitle,
                  style: const TextStyle(
                      fontSize: AppSize.size18, color: AppColors.blackColor),
                ),
              ),
              leading: SvgPicture.asset(
                infoProfileModelList[index].icon,
                colorFilter: ColorFilter.mode(
                    AppColors.lightBlackColor, BlendMode.srcIn),
                width: 18,
              ),
            ),
        separatorBuilder: ((context, index) => const Padding(
              padding: EdgeInsets.all(18.0),
              child: Divider(
                thickness: 2,
              ),
            )),
        itemCount: infoProfileModelList.length);
  }
}
