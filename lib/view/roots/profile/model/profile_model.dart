import 'package:mad_sal/core/constants/app_icons.dart';

class InfoProfileModel {
  final String icon;
  final String title;
  final String subTitle;

  InfoProfileModel({
    required this.icon,
    required this.title,
    required this.subTitle,
  });
}

List<InfoProfileModel> infoProfileModelList = [
  InfoProfileModel(
      icon: AppIcons.mailIcon, title: 'Email', subTitle: 'yourname @mail.com'),
  InfoProfileModel(
      icon: AppIcons.callIcon, title: 'Phone', subTitle: '+963 998765432'),
  InfoProfileModel(
      icon: AppIcons.calendarIcon,
      title: 'Date of joining',
      subTitle: 'YY/ MM/ DD'),
];
