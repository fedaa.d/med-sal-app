import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/profile/widgets/listtile_profile.dart';
import 'package:mad_sal/view/roots/profile/widgets/photo_section.dart';

class ProfileScreen extends StatelessWidget {
  const ProfileScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.whiteColor,
      body: SafeArea(
          child: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                width: double.infinity,
                height: 339,
                decoration: BoxDecoration(
                  color: AppColors.mainColor,
                  border: Border.all(color: AppColors.borderColor),
                  borderRadius: const BorderRadius.only(
                      bottomLeft: Radius.circular(45),
                      bottomRight: Radius.circular(45)),
                ),
                child: const PhotoSection()),
            const SizedBox(
              height: 30,
            ),
            ListTileProfile(onTap: () {}),
            const SizedBox(
              height: 10,
            )
          ],
        ),
      )),
    );
  }
}
