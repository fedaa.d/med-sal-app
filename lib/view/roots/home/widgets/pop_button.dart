import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/view/roots/home/widgets/pop_menu_pages.dart';

class PopButton extends StatelessWidget {
  const PopButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      icon: SvgPicture.asset(AppIcons.formatListIcon),
      elevation: 4,
      itemBuilder: (BuildContext context) => [
        PopupMenuItem(
          padding: const EdgeInsets.only(left: 8, right: 8),
          value: context,
          child: Column(
            children: [
              PopupMenuPages(
                title: 'Home',
                onTap: () => Get.toNamed(AppNameRoutes.rootScreen),
              ),
              PopupMenuPages(
                title: 'Categories',
                onTap: () => Get.toNamed(AppNameRoutes.categoriesScreen),
              ),
              PopupMenuPages(
                title: 'Setting',
                onTap: () => Get.toNamed(AppNameRoutes.settingScreen),
              ),
              PopupMenuPages(
                title: 'State Booking',
                onTap: () => Get.toNamed(AppNameRoutes.stateBookingScreen),
              ),
              PopupMenuPages(
                title: 'Log Out',
                onTap: () => Get.toNamed(AppNameRoutes.rootScreen),
              ),
            ],
          ),
        )
      ],
    );
  }
}
