import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class AdviceText extends StatelessWidget {
  const AdviceText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        Text(
          "Don't let illness or ill health sneak up on you. So, get our health services, and get your most up-to-date health information form in over 155,000 compatible and clinically verified medical journals",
          style: TextStyle(
            color: AppColors.blackColor,
            fontSize: AppSize.size16,
            fontWeight: FontWeight.w400,
          ),
        ),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
