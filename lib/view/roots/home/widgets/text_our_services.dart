import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class TextOurServices extends StatelessWidget {
  const TextOurServices({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Text(
      'Lorem ipsum dolor sit amet consectetur. Sed commodo faucibus accumsan faucibus nunc gravida.',
      style: TextStyle(
        color: AppColors.blackColor,
        fontSize: AppSize.size18,
        fontWeight: FontWeight.w400,
      ),
    );
  }
}
