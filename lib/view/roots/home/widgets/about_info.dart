import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/home/widgets/about_text.dart';

class AboutInfo extends StatelessWidget {
  const AboutInfo({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: 569,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            colors: [AppColors.lighGreenColor, AppColors.whiteColor],
          ),
          border: Border.all(
            color: AppColors.secondaryColor,
          ),
          borderRadius: const BorderRadius.only(
              topRight: Radius.circular(160),
              bottomLeft: Radius.circular(170),
              bottomRight: Radius.circular(170)),
        ),
        child: const AboutText());
  }
}
