import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class IntroText extends StatelessWidget {
  const IntroText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'Get The Best Health Care Services For a More Comfortable Life',
          style: TextStyle(
            color: AppColors.secondaryColor,
            fontSize: AppSize.size23,
            fontWeight: FontWeight.w500,
          ),
        ),
        const SizedBox(
          height: 38,
        ),
      ],
    );
  }
}
