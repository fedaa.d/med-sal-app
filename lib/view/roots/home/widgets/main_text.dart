import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:simple_gradient_text/simple_gradient_text.dart';

class MainText extends StatelessWidget {
  const MainText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Center(
          child: GradientText(
            'Med-Sal',
            style: const TextStyle(
                fontFamily: 'WhirlyBirdieRegular', fontSize: AppSize.size40),
            gradientType: GradientType.linear,
            radius: 1,
            colors: [
              AppColors.lightTealColor,
              AppColors.tealColor,
            ],
          ),
        ),
        const SizedBox(
          height: 15,
        ),
      ],
    );
  }
}
