import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class QuestionClient extends StatelessWidget {
  const QuestionClient({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        padding: const EdgeInsets.only(left: 30),
        alignment: Alignment.center,
        width: 136,
        height: 110,
        decoration: BoxDecoration(
          color: AppColors.whiteColor,
          border: Border.all(
            color: AppColors.lighGreenColor,
          ),
          borderRadius:
              const BorderRadius.only(bottomLeft: Radius.circular(160)),
        ),
        child: Text(
          'What Our Clients Say',
          style: TextStyle(
            fontSize: AppSize.size18,
            fontWeight: FontWeight.w600,
            color: AppColors.secondaryColor,
          ),
        ),
      ),
    );
  }
}
