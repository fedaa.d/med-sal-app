import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class DoctorsText extends StatelessWidget {
  const DoctorsText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Column(
      children: [
        SizedBox(
          height: 20,
        ),
        Center(
            child: Text(
          'Doctor',
          style: TextStyle(
            color: AppColors.blackColor,
            fontSize: AppSize.size20,
            fontWeight: FontWeight.w600,
          ),
        )),
        SizedBox(
          height: 30,
        ),
      ],
    );
  }
}
