// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/view/roots/home/model/choose_screen_model.dart';

class ChooseScreen extends StatelessWidget {
  const ChooseScreen({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 28,
        ),
        LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) => Center(
            child: SizedBox(
              height: 44,
              child: ListView.separated(
                separatorBuilder: (BuildContext context, int index) => SizedBox(
                  width: constraints.maxWidth > 700 ? 210 : 30,
                ),
                scrollDirection: Axis.horizontal,
                itemBuilder: (BuildContext context, int index) =>
                    GestureDetector(
                        onTap: chooseScreenModelList[index].onTap,
                        child: Container(
                          alignment: Alignment.center,
                          width: 105,
                          height: 44,
                          decoration: BoxDecoration(
                              borderRadius: const BorderRadius.only(
                                  bottomLeft: Radius.circular(25),
                                  topRight: Radius.circular(25)),
                              color: AppColors.secondaryColor),
                          child: Text(
                            chooseScreenModelList[index].title,
                            style: const TextStyle(
                                color: AppColors.whiteColor,
                                fontWeight: FontWeight.w500),
                          ),
                        )),
                shrinkWrap: true,
                itemCount: chooseScreenModelList.length,
              ),
            ),
          ),
        )
      ],
    );
  }
}
