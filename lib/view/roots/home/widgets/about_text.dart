import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class AboutText extends StatelessWidget {
  const AboutText({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 18.0, bottom: 18.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          ListView.separated(
              shrinkWrap: true,
              itemBuilder: (BuildContext context, int index) => const Text(
                    'Lorem ipsum dolor sit amet consectetur. Sed commodo faucibus accumsan faucibus nunc gravida.',
                    style: TextStyle(
                        fontSize: AppSize.size18,
                        fontWeight: FontWeight.w500,
                        color: AppColors.blackColor),
                  ),
              separatorBuilder: (BuildContext context, int index) =>
                  const SizedBox(
                    height: 60,
                  ),
              itemCount: 3)
        ],
      ),
    );
  }
}
