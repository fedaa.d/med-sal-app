import 'package:flutter/material.dart';

class IconAppbar extends StatelessWidget {
  const IconAppbar({
    Key? key,
    required this.icon,
    required this.size,
    this.onTap,
  }) : super(key: key);
  final Widget icon;
  final double size;
  final Function()? onTap;
  @override
  Widget build(BuildContext context) {
    return IconButton(
      iconSize: size,
      onPressed: onTap,
      icon: icon,
      color: Colors.black,
    );
  }
}
