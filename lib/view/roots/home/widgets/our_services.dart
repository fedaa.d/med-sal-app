import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class OurServices extends StatelessWidget {
  const OurServices({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 40,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Container(
            alignment: Alignment.center,
            width: 206,
            height: 57,
            decoration: BoxDecoration(
              color: AppColors.whiteColor,
              border: Border.all(color: AppColors.secondaryColor),
              borderRadius:
                  const BorderRadius.only(bottomRight: Radius.circular(160)),
            ),
            child: Text(
              'Our Services',
              style: TextStyle(
                fontSize: AppSize.size20,
                fontWeight: FontWeight.w600,
                color: AppColors.secondaryColor,
              ),
            ),
          ),
        ),
        const SizedBox(
          height: 40,
        ),
      ],
    );
  }
}
