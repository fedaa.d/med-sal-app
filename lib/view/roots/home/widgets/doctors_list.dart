import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_images.dart';

class DoctorsList extends StatelessWidget {
  const DoctorsList({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      width: double.infinity,
      child: ListView.separated(
          itemBuilder: (BuildContext context, int index) => Image.asset(
                imagesList[index],
              ),
          shrinkWrap: true,
          itemCount: imagesList.length,
          scrollDirection: Axis.horizontal,
          separatorBuilder: (BuildContext context, int index) => const SizedBox(
                width: 20,
              )),
    );
  }
}

List<String> imagesList = [
  AppImages.doctorsImage,
  AppImages.doctorsImage,
  AppImages.doctorsImage,
  AppImages.doctorsImage,
  AppImages.doctorsImage,
  AppImages.doctorsImage,
];
