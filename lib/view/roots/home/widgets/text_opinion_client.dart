import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class TextOpinionClient extends StatelessWidget {
  const TextOpinionClient({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: 8.0),
      child: Padding(
        padding: const EdgeInsets.only(
          left: 18.0,
          right: 18,
          top: 18,
        ),
        child: RichText(
          text: const TextSpan(
            style: TextStyle(
                fontSize: AppSize.size19,
                fontWeight: FontWeight.w400,
                color: AppColors.blackColor),
            children: [
              TextSpan(text: 'Lorem ipsum dolor sit amet '),
              TextSpan(text: 'consectetur. Lorem ipsum dolor sit'),
              TextSpan(text: 'amet consectetur. .'),
            ],
          ),
        ),
      ),
    );
  }
}
