import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_images.dart';
import 'package:mad_sal/view/roots/home/widgets/name_client.dart';
import 'package:mad_sal/view/roots/home/widgets/text_opinion_client.dart';

class ClientOpinion extends StatelessWidget {
  const ClientOpinion({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 11),
      width: double.infinity,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topRight,
          end: Alignment.bottomCenter,
          colors: [
            AppColors.whiteColor,
            AppColors.lighGreenColor,
          ],
        ),
        borderRadius: const BorderRadius.only(
          topRight: Radius.circular(180),
          bottomLeft: Radius.circular(300),
          bottomRight: Radius.circular(170),
        ),
      ),
      child: Column(
        children: [
          const TextOpinionClient(),
          Padding(
            padding: const EdgeInsets.only(right: 4.0),
            child: Align(
              alignment: Alignment.centerRight,
              child: Image.asset(
                AppImages.clientImage,
              ),
            ),
          ),
          const NameClient(),
        ],
      ),
    );
  }
}
