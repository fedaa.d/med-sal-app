import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class PopupMenuPages extends StatelessWidget {
  const PopupMenuPages({
    Key? key,
    required this.title,
    required this.onTap,
  }) : super(key: key);
  final String title;
  final Function() onTap;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
          margin: const EdgeInsets.only(top: 10.0),
          width: double.infinity,
          padding: const EdgeInsets.all(8),
          decoration:
              BoxDecoration(border: Border.all(color: AppColors.lightColor)),
          child: Text(
            title,
            style: const TextStyle(
                fontWeight: FontWeight.w500, fontSize: AppSize.size16),
          )),
    );
  }
}
