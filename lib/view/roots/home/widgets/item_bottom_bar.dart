import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

BottomNavigationBarItem bottomNavigationBarItem(
    {required String icon, required String activeIcon}) {
  return BottomNavigationBarItem(
      activeIcon: SvgPicture.asset(icon,
          colorFilter:
              ColorFilter.mode(AppColors.secondaryColor, BlendMode.srcIn)),
      label: '',
      icon: SvgPicture.asset(
        activeIcon,
        colorFilter:
            const ColorFilter.mode(AppColors.blackColor, BlendMode.srcIn),
      ));
}
