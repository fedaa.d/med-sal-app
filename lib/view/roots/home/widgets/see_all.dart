import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class SeeAll extends StatelessWidget {
  const SeeAll({
    Key? key,
    required this.mainAxisAlignment,
  }) : super(key: key);
  final MainAxisAlignment mainAxisAlignment;
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: mainAxisAlignment,
      children: [
        Text(
          'See All',
          style: TextStyle(
              color: AppColors.lightSecondaryColor,
              fontWeight: FontWeight.w400,
              fontSize: AppSize.size16),
        ),
        IconButton(onPressed: () {}, icon: SvgPicture.asset(AppIcons.arrowIcon))
      ],
    );
  }
}
