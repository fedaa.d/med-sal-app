import 'package:flutter/material.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_images.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';

class IntroSection extends StatelessWidget {
  const IntroSection({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const SizedBox(
          height: 40,
        ),
        LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
            return constraints.maxWidth > 400
                ? Container()
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Image.asset(AppImages.logoImage),
                      Image.asset(AppImages.doctorImage),
                    ],
                  );
          },
        ),
        const Text(
          'Welcome to',
          style: TextStyle(
            fontSize: AppSize.size18,
            fontWeight: FontWeight.w500,
            color: AppColors.blackColor,
          ),
        ),
        Text(
          'Welcome to',
          style: TextStyle(
            fontSize: AppSize.size24,
            fontWeight: FontWeight.w500,
            color: AppColors.secondaryColor,
          ),
        ),
        const SizedBox(
          height: 38,
        ),
      ],
    );
  }
}
