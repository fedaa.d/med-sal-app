import 'package:get/get.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';

class ChooseScreenModel {
  final String title;
  final Function() onTap;
  ChooseScreenModel({
    required this.title,
    required this.onTap,
  });
}

List<ChooseScreenModel> chooseScreenModelList = [
  ChooseScreenModel(
    title: 'Register',
    onTap: () => Get.offNamed(AppNameRoutes.registerPatientScreen),
  ),
  ChooseScreenModel(
    title: 'Contact Us',
    onTap: () => Get.offNamed(AppNameRoutes.registerPatientScreen),
  ),
  ChooseScreenModel(
    title: 'Log in',
    onTap: () => Get.offNamed(AppNameRoutes.login),
  ),
];
