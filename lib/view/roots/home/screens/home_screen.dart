import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/view/roots/categories/cetegory_doctor/dentistry/widgets/search_text_filed.dart';
import 'package:mad_sal/view/roots/home/widgets/about_info.dart';
import 'package:mad_sal/view/roots/home/widgets/about_us.dart';
import 'package:mad_sal/view/roots/home/widgets/advice_text.dart';
import 'package:mad_sal/view/roots/home/widgets/choose_screen.dart';
import 'package:mad_sal/view/roots/home/widgets/client_opinion.dart';
import 'package:mad_sal/view/roots/home/widgets/doctors_list.dart';
import 'package:mad_sal/view/roots/home/widgets/doctors_text.dart';
import 'package:mad_sal/view/roots/home/widgets/intro_section.dart';
import 'package:mad_sal/view/roots/home/widgets/intro_text.dart';
import 'package:mad_sal/view/roots/home/widgets/main_text.dart';
import 'package:mad_sal/view/roots/home/widgets/our_services.dart';
import 'package:mad_sal/view/roots/home/widgets/quistion_client.dart';
import 'package:mad_sal/view/roots/home/widgets/see_all.dart';
import 'package:mad_sal/view/roots/home/widgets/text_our_services.dart';
import 'package:mad_sal/view/roots/root/widgets/root_appbar.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: AppColors.cardColor,
        appBar: appBar(context),
        body: Padding(
          padding: const EdgeInsets.only(left: 18, right: 18, top: 18),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const MainText(),
                SearchTextFiled(
                  hintText: 'Search',
                  height: 58,
                  suffixIcon: Padding(
                    padding: const EdgeInsets.only(
                      right: 30.0,
                    ),
                    child: SvgPicture.asset(AppIcons.searchIcon),
                  ),
                ),
                const ChooseScreen(),
                const IntroSection(),
                const IntroText(),
                const AdviceText(),
                const AboutUs(),
                const AboutInfo(),
                const OurServices(),
                const TextOurServices(),
                const SeeAll(
                  mainAxisAlignment: MainAxisAlignment.end,
                ),
                const DoctorsList(),
                const DoctorsText(),
                const Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    SeeAll(
                      mainAxisAlignment: MainAxisAlignment.start,
                    ),
                    QuestionClient(),
                  ],
                ),
                const ClientOpinion()
              ],
            ),
          ),
        ),
      ),
    );
  }
}
