import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/main_controller.dart';
import 'package:mad_sal/view/roots/root/widgets/root_bottombar_widget.dart';

class RootScreen extends GetView<RootControllerImp> {
  const RootScreen({super.key});
  @override
  Widget build(BuildContext context) {
    Get.put(RootControllerImp());
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        bottomNavigationBar: const RootBottomBarWidget(),
        body: GetBuilder<RootControllerImp>(
          builder: (_) => IndexedStack(
            index: controller.currentIndex,
            children: controller.rootsScreensList,
          ),
        ),
      ),
    );
  }
}
