import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/main_controller.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/view/roots/home/widgets/item_bottom_bar.dart';

class RootBottomBarWidget extends GetView<RootControllerImp> {
  const RootBottomBarWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: Colors.grey.shade400,
            offset: const Offset(0, 5),
            blurRadius: 3,
            spreadRadius: 7.0,
          ),
        ],
      ),
      child: GetBuilder<RootControllerImp>(
        builder: (controller) => BottomNavigationBar(
          elevation: 1,
          onTap: (index) => controller.selectScreen(index),
          iconSize: 24,
          backgroundColor: Colors.white,
          currentIndex: controller.currentIndex,
          type: BottomNavigationBarType.fixed,
          items: [
            bottomNavigationBarItem(
              icon: AppIcons.homeIcon,
              activeIcon: AppIcons.homeIcon,
            ),
            bottomNavigationBarItem(
              icon: AppIcons.profileIcon,
              activeIcon: AppIcons.profileIcon,
            ),
            bottomNavigationBarItem(
              icon: AppIcons.bagIcon,
              activeIcon: AppIcons.bagIcon,
            ),
            bottomNavigationBarItem(
              icon: AppIcons.mailIcon,
              activeIcon: AppIcons.mailIcon,
            ),
            bottomNavigationBarItem(
              icon: AppIcons.helpIcon,
              activeIcon: AppIcons.helpIcon,
            )
          ],
        ),
      ),
    );
  }
}
