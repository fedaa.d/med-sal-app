import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/home_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';
import 'package:mad_sal/core/constants/app_icons.dart';
import 'package:mad_sal/core/constants/app_name_routes.dart';
import 'package:mad_sal/core/constants/app_sizes.dart';
import 'package:mad_sal/view/roots/home/widgets/icon_appbar.dart';
import 'package:mad_sal/view/roots/home/widgets/pop_button.dart';
import 'package:mad_sal/view/roots/root/widgets/choose_lang_dropbutton.dart';

AppBar appBar(BuildContext context) {
  Get.put(HomeControllerImp());

  return AppBar(
    actions: [
      const ChooseLangugeDropButton(),
      IconAppbar(
        icon: SvgPicture.asset(AppIcons.filterIcon),
        size: AppSize.size25,
        onTap: () => Get.toNamed(AppNameRoutes.filterSearchScreen),
      ),
      Theme(
        data: Theme.of(context).copyWith(
          iconTheme: const IconThemeData(color: AppColors.blackColor),
        ),
        child: const PopButton(),
      ),
    ],
    elevation: 0,
    shape: Border(bottom: BorderSide(color: AppColors.secondaryColor)),
    toolbarHeight: 84,
    leadingWidth: double.infinity,
    leading: Row(
      children: [
        IconAppbar(
          onTap: () => Get.toNamed(AppNameRoutes.profileScreen),
          icon: SvgPicture.asset(AppIcons.profileIcon),
          size: AppSize.size25,
        ),
        IconAppbar(
          icon: SvgPicture.asset(AppIcons.callIcon),
          size: AppSize.size24,
        ),
        IconAppbar(
          icon: SvgPicture.asset(AppIcons.alternateEmailIcon),
          size: AppSize.size24,
        ),
      ],
    ),
    backgroundColor: AppColors.lightGryColor,
  );
}
