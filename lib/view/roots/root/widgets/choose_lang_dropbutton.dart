import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mad_sal/controller/home_controller.dart';
import 'package:mad_sal/core/constants/app_colors.dart';

class ChooseLangugeDropButton extends StatelessWidget {
  const ChooseLangugeDropButton({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 90,
        height: 36,
        decoration: BoxDecoration(
            border: Border.all(color: AppColors.blackColor),
            color: AppColors.whiteColor),
        child: GetBuilder<HomeControllerImp>(
          builder: (controller) => DropdownButton(
              underline: const Text(''),
              elevation: 0,
              isExpanded: true,
              style: const TextStyle(fontSize: 14, color: Colors.black),
              padding: const EdgeInsets.symmetric(horizontal: 13),
              value: controller.selectedItem,
              items: lang.values
                  .map((e) => DropdownMenuItem(
                        value: e,
                        child: Text(e.name),
                      ))
                  .toList(),
              onChanged: (val) {
                if (val == null) {
                  return;
                }
                controller.onChoose(val);
              }),
        ),
      ),
    );
  }
}
