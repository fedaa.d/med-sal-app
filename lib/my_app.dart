import 'package:mad_sal/core/constants/app_packages.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    LocaleController controller = Get.put(LocaleController());
    return ScreenUtilInit(
      designSize: const Size(390, 812),
      minTextAdapt: true,
      splitScreenMode: true,
      builder: (BuildContext context, _) => GetMaterialApp(
        debugShowCheckedModeBanner: false,
        theme: AppTheme.themeArabic,
        translations: MyTranslation(),
        locale: controller.language,
        initialBinding: CrudBinding(),
        getPages: AppRoutes.routes,
        initialRoute: AppNameRoutes.login,
      ),
    );
  }
}
